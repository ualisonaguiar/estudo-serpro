from status_tarefa_enum import StatusTarefaEnum

class Tarefa:
	def __init__(self):
		self.__id: int = 0
		self.__nome: str = ''
		self.__status_tarefa: StatusTarefaEnum = StatusTarefaEnum.PARA_FAZER

	@property
	def id(self) -> int:
		return self.__id

	@id.setter
	def id(self, id: int) -> None:
		self.__id = id

	@property
	def nome(self) -> str:
		return self.__nome

	@nome.setter
	def nome(self, nome: str) -> None:
		self.__nome = nome

	@property
	def status_tarefa(self) -> None:
		return self.__status_tarefa

	@status_tarefa.setter
	def status_tarefa(self, status_tarefa: StatusTarefaEnum) -> None:
		self.__status_tarefa = status_tarefa
