import mysql.connector

class ConexaoDatabase:
	def __init__(self):
		self.__conexao = None

	def __enter__(self):
		config = {
			'user': 'root',
			'password': 'DCT@_bd&244',
			'host': '10.67.40.244',
			'database': 'ten_ualison'
		}
		try:
			connection = mysql.connector.connect(**config)
			if connection.is_connected():
				self.__conexao = connection
		except mysql.connector.Error as err:
			print(f"Erro ao conectar ao MySQL: {err}")

		return self.__conexao


	def __exit__(self, exc_type, exc_val, exc_tb):
		if self.__conexao is not None:
			self.__conexao.close()
