import unittest

from tarefa import Tarefa
from tarefa_service import TarefaService
from status_tarefa_enum import StatusTarefaEnum

class TestTarefaService(unittest.TestCase):

	def __init__(self, method_name =  'runtest') -> None:
		super().__init__(method_name)
		self.__service = TarefaService()


	def test_cadatrar_tarefa_status_para_fazer(self) -> None:
		tarefa = Tarefa()
		tarefa.nome = 'Definir identidade visual'
		self.__service.salvar(tarefa)
		self.assertTrue(tarefa.id != 0)

		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(tarefa.id, tarefa2.id)
		self.assertEqual(tarefa.nome, tarefa2.nome)
		self.assertEqual(StatusTarefaEnum.PARA_FAZER, tarefa2.status_tarefa)

	def test_atualizar_status_tarefa(self) -> None:
		tarefa = Tarefa()
		tarefa.nome = 'Definir SGDB'
		self.__service.salvar(tarefa)
		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.ESPERANDO)
		tarefa2 = self.__service.obter(tarefa.id)

		self.assertEqual(tarefa.id, tarefa2.id)
		self.assertEqual(StatusTarefaEnum.ESPERANDO, tarefa2.status_tarefa)

	def test_listagem_tarefa(self) -> None:
		tarefa = Tarefa()
		tarefa.nome = 'Corrigir erro em produção'
		self.__service.salvar(tarefa)
		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.TRABALHANDO)

		tarefas = self.__service.listar()
		tarefa_db = tarefas[::-1]

		self.assertEqual(tarefa.id, tarefa_db[0].id)
		self.assertEqual(tarefa.nome, tarefa_db[0].nome)
		self.assertEqual(tarefa.status_tarefa, StatusTarefaEnum.TRABALHANDO)

	def test_listar_categoria(self) -> None:
		for tarefa in self.__service.listar():
			self.__service.excluir(tarefa)
		self.assertEqual(0, len(self.__service.listar()))

		tarefa = Tarefa()
		tarefa.nome = 'Corrigir erro em produção'
		self.__service.salvar(tarefa)
		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.TRABALHANDO)

		tarefa = Tarefa()
		tarefa.nome = 'Implementar Login'
		self.__service.salvar(tarefa)
		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.TRABALHANDO)

		tarefa = Tarefa()
		tarefa.nome = 'Definir local para compartilhar código'
		self.__service.salvar(tarefa)
		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.FEITO)

		tarefa = Tarefa()
		tarefa.nome = 'Definir SGDB'
		self.__service.salvar(tarefa)

		tarefa = Tarefa()
		tarefa.nome = 'Definir identidade visual'
		self.__service.salvar(tarefa)

		self.assertEqual(2, len(self.__service.listar_por_categoria(StatusTarefaEnum.PARA_FAZER)))
		self.assertEqual(1, len(self.__service.listar_por_categoria(StatusTarefaEnum.FEITO)))
		self.assertEqual(2, len(self.__service.listar_por_categoria(StatusTarefaEnum.TRABALHANDO)))


	def test_progredir_regredir_tarefa(self):
		tarefa = Tarefa()
		tarefa.nome = 'Definir SGDB'
		self.__service.salvar(tarefa)

		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(StatusTarefaEnum.PARA_FAZER, tarefa2.status_tarefa)

		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.TRABALHANDO)
		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(StatusTarefaEnum.TRABALHANDO, tarefa2.status_tarefa)

		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.ESPERANDO)
		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(StatusTarefaEnum.ESPERANDO, tarefa2.status_tarefa)

		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.FEITO)
		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(StatusTarefaEnum.FEITO, tarefa2.status_tarefa)

		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.ESPERANDO)
		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(StatusTarefaEnum.ESPERANDO, tarefa2.status_tarefa)

		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.TRABALHANDO)
		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(StatusTarefaEnum.TRABALHANDO, tarefa2.status_tarefa)

		self.__service.atualizar_status_tarefa(tarefa, StatusTarefaEnum.PARA_FAZER)
		tarefa2 = self.__service.obter(tarefa.id)
		self.assertEqual(StatusTarefaEnum.PARA_FAZER, tarefa2.status_tarefa)		