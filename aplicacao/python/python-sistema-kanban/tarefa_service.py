from tarefa import Tarefa
from tarefa_dao import TarefaDAO
from status_tarefa_enum import StatusTarefaEnum

class TarefaService:
	def __init__(self):
		self.__repository = TarefaDAO()

	def salvar(self, tarefa: Tarefa) -> None:
		if tarefa.id == 0:
			tarefa.status_tarefa = StatusTarefaEnum.PARA_FAZER
			self.__repository.inserir(tarefa)
		else:
			self.__repository.atualizar(tarefa)


	def obter(self, id_tarefa: int) -> Tarefa:
		tarefa = self.__repository.obter(id_tarefa)
		if tarefa.id == 0:
			raise ValueError(f'ID: {id_tarefa} não encontrado')
		return tarefa

	def excluir(self, tarefa: Tarefa) -> None:
		self.obter(tarefa.id)
		self.__repository.excluir(tarefa.id)

	def atualizar_status_tarefa(self, tarefa: Tarefa, novo_status: StatusTarefaEnum) -> Tarefa:
		self.__repository.obter(tarefa.id)
		tarefa.status_tarefa = novo_status
		self.salvar(tarefa)
		return tarefa

	def listar(self) -> [Tarefa]:
		return self.__repository.listar()

	def listar_por_categoria(self, status: StatusTarefaEnum) -> [Tarefa]:
		return self.__repository.listar_por_categoria(status)		