from enum import Enum

class StatusTarefaEnum:
	PARA_FAZER = 1
	TRABALHANDO = 2
	ESPERANDO = 3
	FEITO = 4