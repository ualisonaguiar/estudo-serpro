from tarefa import Tarefa
from conexao_database import ConexaoDatabase
from status_tarefa_enum import StatusTarefaEnum

class TarefaDAO:

	def inserir(self, tarefa: Tarefa) -> Tarefa:
		with ConexaoDatabase() as conexao:
			cursor = conexao.cursor()
			values = (tarefa.nome, tarefa.status_tarefa,)
			cursor.execute("insert into tarefa (nome, status_tarefa) values (%s, %s)", values)
			conexao.commit()
			tarefa.id = int(cursor.lastrowid)
			cursor.close()
		return tarefa

	def atualizar(self, tarefa: Tarefa) -> Tarefa:
		with ConexaoDatabase() as conexao:
			cursor = conexao.cursor()
			values = (tarefa.nome, tarefa.status_tarefa, tarefa.id,)
			cursor.execute("update tarefa set nome = %s, status_tarefa = %s where id = %s", values)
			conexao.commit()
			cursor.close()
		return tarefa

	def obter(self, id_tarefa: int) -> Tarefa:
		with ConexaoDatabase() as conexao:
			cursor = conexao.cursor()
			cursor.execute("select * from tarefa where id = %s", (id_tarefa,))
			tarefa_data = cursor.fetchone()
			cursor.close()
			return self.__populate_entity(tarefa_data)

	def excluir(self, id_tarefa: int) -> None:
		with ConexaoDatabase() as conexao:
			cursor = conexao.cursor()
			cursor.execute("delete from tarefa where id = %s", (id_tarefa,))
			conexao.commit()
			cursor.close()

	def listar(self) -> [Tarefa]:
		with ConexaoDatabase() as conexao:
			cursor = conexao.cursor()
			cursor.execute("select * from tarefa order by id")
			tarefas_data = cursor.fetchall()
			cursor.close()
			return self.__criar_lista_entity(tarefas_data)

	def listar_por_categoria(self, status_tarefa: StatusTarefaEnum) -> [Tarefa]:
		with ConexaoDatabase() as conexao:
			cursor = conexao.cursor()
			values = (status_tarefa,)
			cursor.execute("select * from tarefa where status_tarefa = %s order by id asc", values)
			tarefas_data = cursor.fetchall()
			cursor.close()
			return self.__criar_lista_entity(tarefas_data)

	def __criar_lista_entity(self, tarefas_data) -> [Tarefa]: 
		tarefas: [Tarefa] = []
		for tarefa_data in tarefas_data:
			tarefas.append(self.__populate_entity(tarefa_data))
		return tarefas		

	def __populate_entity(self, result) -> Tarefa:
		tarefa = Tarefa()
		if result:
			id_tarefa, nome, status_tarefa = result
			tarefa.id = int(id_tarefa)
			tarefa.nome = nome
			tarefa.status_tarefa = int(status_tarefa)
		return tarefa
