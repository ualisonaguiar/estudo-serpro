import os

path_arquivo: str = 'linguagens.txt'


if not os.path.exists(path_arquivo):
	print(f'Arquivo: {path_arquivo} não localizado')
else:
	linguagens: [str] = []

	with open(path_arquivo, 'r') as arquivo:
		for linha in arquivo:
			linguagem_duplicada = False

			for linguagem in linguagens:
				if linguagem == linha:
					linguagem_duplicada = True

			if linguagem_duplicada == False:
				linguagens.append(linha)

	with open('linguagens_nao_duplicada.txt', 'w') as arquivo:
		linguagens = sorted(linguagens)
		for linguagem in linguagens:
			arquivo.write(linguagem)