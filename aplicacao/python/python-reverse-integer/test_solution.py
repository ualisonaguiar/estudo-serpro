import unittest
from solution import Solution

class TestSolution(unittest.TestCase):
	def __init__(self, method_name = 'runtest'):
		super().__init__(method_name)
		self.__service = Solution()

	def test_reverse_exemplo_01(self) :
		self.assertEqual(321, self.__service.reverse(123))

	def test_reverse_exemplo_02(self) :
		self.assertEqual(-321, self.__service.reverse(-123))

	def test_reverse_exemplo_03(self) :
		self.assertEqual(0, self.__service.reverse(1534236469))

	def test_reverse_exemplo_04(self) :
		self.assertEqual(21, self.__service.reverse(120))		