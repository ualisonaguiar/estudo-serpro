class Solution:
    def reverse(self, numero: int) -> int:
    	if self.__validar_numero_intervalor(numero):
    		return 0

    	numero_reverso: int = self.__reverso_numero(numero)

    	if self.__validar_numero_intervalor(numero_reverso):
    		numero_reverso = 0

    	return numero_reverso


    def __reverso_numero(self, numero):
    	numero_string: str = str(abs(numero))
    	numero_reverso = int(numero_string[::-1])

    	return numero_reverso if numero >= 0 else -numero_reverso

    def __validar_numero_intervalor(self, numero) -> bool:
    	limite_inferior = -2 ** 31
    	limite_superior = (2 ** 31) - 1
    	return numero < limite_inferior or numero > limite_superior
