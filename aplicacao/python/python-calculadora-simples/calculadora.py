# Calculadora Simples

def somar(a: float, b: float) -> float:
    return a + b

def subtrair(a: float, b: float) -> float:
    return a - b

def multiplicar(a: float, b: float) -> float:
    return a * b

def dividir(a: float, b: float) -> float:
    if b != 0:
        return a / b
    else:
        raise ValueError("Divisão por zero não é possível")

def main():
    print("Bem-vindo à Calculadora Simples!")
    operacao = input("Digite a operação desejada (+, -, *, /): ")

    if operacao not in ("+", "-", "*", "/"):
        print("Operação inválida! Apenas +, -, * e / são permitidos.")
        return

    num1 = float(input("Digite o primeiro número: "))
    num2 = float(input("Digite o segundo número: "))

    resultado = None

    if operacao == "+":
        resultado = somar(num1, num2)
    elif operacao == "-":
        resultado = subtrair(num1, num2)
    elif operacao == "*":
        resultado = multiplicar(num1, num2)
    elif operacao == "/":
        resultado = dividir(num1, num2)

    print("Resultado:", resultado)

if __name__ == "__main__":
    main()