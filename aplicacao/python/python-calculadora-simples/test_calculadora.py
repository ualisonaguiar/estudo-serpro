import unittest
from calculadora import somar, subtrair, multiplicar, dividir

class TestCalcualadora(unittest.TestCase):
    def test_soma(self):
        self.assertEqual(300, somar(100, 200))
        
    def test_subtrair(self):
        self.assertEqual(-100, subtrair(100, 200))
        
    def test_multiplicar(self):
        self.assertEqual(20000, multiplicar(100, 200))
        
    def test_dividir(self):
        self.assertEqual(20, dividir(200, 10))
        
    def test_dividir_zero(self):
        with self.assertRaises(ValueError):
            dividir(200, 0)