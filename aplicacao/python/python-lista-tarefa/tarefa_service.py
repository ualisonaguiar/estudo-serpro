import datetime
from typing import List
from tarefa import Tarefa

class TarefaService:
    def __init__(self):
        self.tarefas = []

    def listagem(self) -> List[Tarefa]:
        return self.tarefas

    def salvar(self, tarefa: Tarefa) -> Tarefa:
        if tarefa.id is None:
            tarefa.id = int(datetime.datetime.now().timestamp())
        self.tarefas.append(tarefa)

        return tarefa

    def excluir(self, tarefa: Tarefa) -> None:
        self.listagem().remove(tarefa)

    def get_item(self, id: int) -> Tarefa:
        indice_tarefa = -1
        tarefas = self.listagem()

        for indice, tarefa_list in enumerate(tarefas):
            if tarefa_list.id == id:
                indice_tarefa = indice
                break

        return tarefas[indice_tarefa]

    @classmethod
    def tarefa_pendente(cls) -> List[Tarefa]:
        return cls().listagem()
