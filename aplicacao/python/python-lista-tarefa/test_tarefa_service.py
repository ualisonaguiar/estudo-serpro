import unittest

from tarefa_service import TarefaService
from tarefa import Tarefa
from tipo_tarefa_enum import TipoTarefaEnum
from situacao_tarefa_enum import SituacaoTarefaEnum

class TestTarefaService(unittest.TestCase):

    def __init__(self, method_name: str = "runTest") -> None:
        super().__init__(method_name)
        self.service = TarefaService()

    def test_lista_vazia(self):
        self.assertTrue(len(self.service.listagem()) == 0)

    def test_adicionar(self):
        tarefa = Tarefa(None, "Entrega sistema", TipoTarefaEnum.TRABALHO, SituacaoTarefaEnum.NOVO)
        self.service.salvar(tarefa)

        tarefas = self.service.listagem()
        self.assertFalse(len(tarefas) == 0)
        self.assertEqual(tarefa, tarefas[0])

    def test_excluir(self):
        tarefa = Tarefa(None, "Levar o carro para arrumar", TipoTarefaEnum.PESSOAL, SituacaoTarefaEnum.PENDENTE)
        tarefa = self.service.salvar(tarefa)

        self.assertFalse(len(self.service.listagem()) == 0)
        self.service.excluir(tarefa)
        self.assertTrue(len(self.service.listagem()) == 0)

    def test_editar(self):
        tarefa = Tarefa(None, "Comprar remédio", TipoTarefaEnum.CASA, SituacaoTarefaEnum.CONCLUIDO)
        tarefa = self.service.salvar(tarefa)

        tarefa.situacao = SituacaoTarefaEnum.PENDENTE
        tarefa.tipo_tarefa = TipoTarefaEnum.CASA
        tarefa.descricao = "Fazer compras"

        tarefa = self.service.salvar(tarefa)

        self.assertEqual(tarefa, self.service.get_item(tarefa.id))

    def test_tarefas_pendente(self):
        self.assertTrue(len(TarefaService.tarefa_pendente()) == 0)
