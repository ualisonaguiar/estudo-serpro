from enum import Enum

class TipoTarefaEnum(Enum):
    CASA = 1
    TRABALHO = 2
    PESSOAL = 3
    OUTROS = 4
