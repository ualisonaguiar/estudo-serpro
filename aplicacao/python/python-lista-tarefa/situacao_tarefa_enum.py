from enum import Enum

class SituacaoTarefaEnum(Enum):
    NOVO = 1
    ABERTO = 2
    PENDENTE = 3
    CONCLUIDO = 4
    CANCELADO = 5
