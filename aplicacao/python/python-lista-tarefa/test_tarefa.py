import unittest

from tarefa import Tarefa
from tipo_tarefa_enum import TipoTarefaEnum
from situacao_tarefa_enum import SituacaoTarefaEnum

class TestTarefa(unittest.TestCase):
    def test_domino(self):
        tarefa = Tarefa(1, "Compras de Casa", TipoTarefaEnum.CASA, SituacaoTarefaEnum.NOVO)

        self.assertEqual(1, tarefa.id)
        self.assertEqual("Compras de Casa", tarefa.descricao)
        self.assertEqual(TipoTarefaEnum.CASA, tarefa.tipo_tarefa)
        self.assertEqual(SituacaoTarefaEnum.NOVO, tarefa.situacao)
