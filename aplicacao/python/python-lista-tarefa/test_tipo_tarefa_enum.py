import unittest

from tipo_tarefa_enum import TipoTarefaEnum

class TestTipoTarefaEnum(unittest.TestCase):
    def test_valores_do_enum(self):
        self.assertEqual(TipoTarefaEnum.CASA.value, 1)
        self.assertEqual(TipoTarefaEnum.TRABALHO.value, 2)
        self.assertEqual(TipoTarefaEnum.PESSOAL.value, 3)
        self.assertEqual(TipoTarefaEnum.OUTROS.value, 4)
