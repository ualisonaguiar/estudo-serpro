import unittest

from situacao_tarefa_enum import SituacaoTarefaEnum

class TestSituacaoTarefaEnum(unittest.TestCase):
    def test_valores_do_enum(self):
        self.assertEqual(SituacaoTarefaEnum.NOVO.value, 1)
        self.assertEqual(SituacaoTarefaEnum.ABERTO.value, 2)
        self.assertEqual(SituacaoTarefaEnum.PENDENTE.value, 3)
        self.assertEqual(SituacaoTarefaEnum.CONCLUIDO.value, 4)
        self.assertEqual(SituacaoTarefaEnum.CANCELADO.value, 5)
