class FilaDeImpressao:
    def __init__(self):
        self.impressoes = []

    def adicionar_impressao(self, documento: str) -> None:
        if documento:
            self.impressoes.append(documento)
        else:
            raise ValueError("Documento inválido")

    def remover_impressao(self) -> str:
        if len(self.impressoes) > 0:
            impressao_removida = self.impressoes.pop(0)
            return impressao_removida
        else:
            raise ValueError("Fila de impressão vazia.")

    def imprimir(self) -> str:
        if len(self.impressoes) > 0:
            proxima_impressao = self.impressoes[0]
            return proxima_impressao
