import unittest
from fila_de_impressao import FilaDeImpressao
from collections import deque

class TestFilaDeImpressao(unittest.TestCase):
    def setUp(self):
        self.fila = FilaDeImpressao()

    def test_adicionar_impressao(self):
        self.fila.adicionar_impressao("Documento1")
        self.assertEqual(self.fila.impressoes, ["Documento1"])

    def test_remover_impressao(self):
        self.fila.adicionar_impressao("Documento1")
        self.fila.adicionar_impressao("Documento2")

        impressao_removida = self.fila.remover_impressao()
        self.assertEqual(impressao_removida, "Documento1")
        self.assertEqual(self.fila.impressoes, ["Documento2"])

    def test_imprimir(self):
        self.assertIsNone(self.fila.imprimir())

        self.fila.adicionar_impressao("Documento1")
        proxima_impressao = self.fila.imprimir()
        self.assertEqual(proxima_impressao, "Documento1")

    def test_remover_de_fila_vazia(self):
        with self.assertRaises(ValueError):
            self.fila.remover_impressao()
