class Solution:
    def isValid(self, s: str) -> bool:
        
        while "()" in s or "[]" in s or "{}" in s:
            s = s.replace("()", "").replace("[]", "").replace("{}", "")
        
        return len(s) == 0
    

solution = Solution()
print(solution.isValid("{[]}"))