class Solution:
    def romanToInt(self, s: str) -> int:
        romanos = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000
        }
        
        resultado = 0
        numero_anterior = 0
        
        for romano in s:
            numero_romano = int(romanos.get(romano))
            if numero_romano > numero_anterior:
                resultado = resultado + numero_romano - 2 * numero_anterior
            else:
                resultado = resultado + numero_romano

            numero_anterior = numero_romano
        
        return resultado
    
solution = Solution()
print(solution.romanToInt("LVIII"))
