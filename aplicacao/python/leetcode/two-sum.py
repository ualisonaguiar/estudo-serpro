class Solution:
    def two_sum(self, nums: [int], target: int) -> [int]:
        tamanho = len(nums)
        for i in range(tamanho - 1) :
            for j in range(i + 1, tamanho) :
                if nums[i] + nums[j] == target:
                    return [i, j]
                
        return []

solution = Solution()
num3 = solution.two_sum([3, 3], 6)

print(num3)