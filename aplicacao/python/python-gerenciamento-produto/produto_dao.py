from conexao_db import ConexaoDB
from produto import Produto

class ProdutoDAO:
    def __init__(self) -> None:
        self._repository = ConexaoDB()
        self._conexao = self._repository.abre_conexao()
        
    def listagem(self) -> [Produto]:        
        cursor = self._conexao.cursor()
        cursor.execute("select * from produto order by nome asc")        
        produtos_data = cursor.fetchall()
        cursor.close
        
        produtos = []        
        for produto_result in produtos_data:
            produtos.append(self.populate_domain(produto_result))
        
        return produtos
    
    def cadastrar(self, produto: Produto) -> Produto:
        cursor = self._conexao.cursor()
        values = (produto.nome, produto.descricao, produto.preco, produto.quantidade)
        cursor.execute("insert into produto (nome, descricao, preco, quantidade) values (%s, %s, %s, %s)", values)
        self._conexao.commit()
        produto.id = cursor.lastrowid
        cursor.close        
        return produto
    
    def atualizar(self, produto: Produto) -> Produto:
        cursor = self._conexao.cursor()
        values = (produto.nome, produto.descricao, produto.preco, produto.quantidade, produto.id)
        cursor.execute("update produto set nome = %s, descricao = %s, preco = %s, quantidade = %s where id = %s", values)
        self._conexao.commit();
        
        cursor.close
        
        return produto
    
    def excluir(self, produto: Produto) -> None:
        cursor = self._conexao.cursor()
        cursor.execute("delete from produto where id = %s", (produto.id, ))
        self._conexao.commit()
        cursor.close
        
    def pesquisar(self, nome_produto: str) -> [Produto]:        
        cursor = self._conexao.cursor()
        cursor.execute("select * from produto where nome like %s order by nome asc", ("%" + nome_produto + "%", ))
        produtos_data = cursor.fetchall()
        cursor.close
        
        produtos = []        
        for produto_result in produtos_data:
            produtos.append(self.populate_domain(produto_result))
        
        return produtos

    
    def populate_domain(self, result) -> Produto :
        produto = Produto()        
        if result:
            id_produto, nome, descricao, preco, quantidade = result
            produto.id = id_produto
            produto.nome = nome
            produto.descricao = descricao
            produto.preco = preco
            produto.quantidade = quantidade
        return produto