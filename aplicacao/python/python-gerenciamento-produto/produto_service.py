from produto import Produto
from produto_dao import ProdutoDAO

class ProdutoService:
    def __init__(self) -> None:
        self._dao = ProdutoDAO()
    
    def cadatrar(self, produto: Produto) -> Produto:
        if produto.nome is None or produto.preco is None:
            print("Nome/Preço são preenchimentos obrigatórios")
        else:
            return self._dao.cadastrar(produto)
    
    def atualizar(self, produto: Produto) -> Produto:
        if produto.nome is None or produto.preco is None:
            print("Nome/Preço são preenchimentos obrigatórios")
        else:
            return self._dao.atualizar(produto)
    
    def listagem(self) -> [Produto]:
        return self._dao.listagem()
    
    def excluir(self, produto: Produto) -> None:
        self._dao.excluir(produto)
    
    def pesquisa(self, nome_produto: str) -> [Produto]:
        return self._dao.pesquisar(nome_produto)
    
    def controle_estoque(self) -> int:
        produtos = self.listagem()
        quantidade = 0
        for produto in produtos:
            quantidade += produto.quantidade

        return quantidade