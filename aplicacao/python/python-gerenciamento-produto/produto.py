class Produto:
    def __init__(self) -> None:
        self._id: int = 0
        self._nome: str = ""
        self._descricao: str = ""
        self._preco: float = 0.00
        self._quantidade: int = 0
    
    @property
    def id(self) -> int:
        return self._id
    
    @id.setter
    def id(self, id:int) -> None:
        self._id = id
    
    @property
    def nome(self) -> str:
        return self._nome
    
    @nome.setter
    def nome(self, nome: str) -> None:
        self._nome = nome
        
    @property
    def descricao(self) -> str:
        return self._descricao
    
    @descricao.setter
    def descricao(self, descricao: str) -> None:
        self._descricao = descricao
        
    @property
    def preco(self) -> float:
        return self._preco
    
    @preco.setter
    def preco(self, preco: str) -> None:
        self._preco = preco
        
    @property
    def quantidade(self) -> int:
        return self._quantidade
    
    @quantidade.setter
    def quantidade(self, quantidade: int) -> None:
        self._quantidade = quantidade