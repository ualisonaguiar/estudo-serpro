import unittest
from produto_service import ProdutoService
from produto import Produto

class TestService(unittest.TestCase):
    
    def __init__(self, method_name: str = "runTest") -> None:
        super().__init__(method_name)
        self._service = ProdutoService()
                
    def test_cadastro_produto(self) -> None:
        produto = Produto()
        produto.nome = "Garrafa de água mineral"
        produto.descricao = "Garrafa de água mineral de 500 ml"
        produto.preco = 2.79
        produto.quantidade = 100        
        self._service.cadatrar(produto)
        
        produtos = self._service.listagem()
        produto_encontrado = False
        for produto_data in produtos:
            if produto.id == produto_data.id and produto_data.nome == produto.nome:
                produto_encontrado = True
        
        self.assertTrue(produto_encontrado)
        
    def test_listagem(self) -> None:
        produtos = self._service.listagem()        
        self.assertTrue(len(produtos) != 0)        
    
    def test_atualizar(self) -> None:
        produto = Produto()
        produto.nome = "Celular samsung J6"
        produto.descricao = "Celular samsung J6 Android 14"
        produto.preco = 1223.79
        produto.quantidade = 100        
        self._service.cadatrar(produto)
        
        produto.preco = 1500.85
        produto.quantidade = 246       
        self._service.atualizar(produto)
        
        produtos = self._service.listagem()
        produto_alterado = Produto()
        for produto_data in produtos:
            if produto_data.id == produto.id :
                produto_alterado = produto_data
        
        self.assertEqual(produto.preco, produto_alterado.preco)
        self.assertEqual(produto.quantidade, produto_alterado.quantidade)
        
    def test_excluir(self) -> None:
        produto = Produto()
        produto.nome = "Notebook Asus"
        produto.descricao = "Notebook Asus K45A"
        produto.preco = 1924.14
        produto.quantidade = 20
        self._service.cadatrar(produto)    
        self._service.excluir(produto)
        produtos = self._service.listagem()
        
        produto_encontrado = None
        for produto_data in produtos:
            if produto_data.id == produto.id :
                produto_encontrado = produto_data
                
        self.assertIsNone(produto_encontrado)
        
    def test_encontre_produto_nome(self) -> None:
        produto = Produto()
        produto.nome = "Notebook Dell i12340002 Brasil"
        produto.descricao = "Notebook Dell latitude"
        produto.preco = 4999.99
        produto.quantidade = 20
        self._service.cadatrar(produto)
        
        produtos = self._service.pesquisa("i12340002")
        self.assertEqual(1, len(produtos))
        
        self._service.excluir(produto)
        
    def test_controle_estoque(self) -> None:
        for produto in self._service.listagem():
            self._service.excluir(produto)
    
        self.assertTrue(len(self._service.listagem()) == 0)                 
        
        produto = Produto()
        produto.nome = "Celular samsung J6"
        produto.descricao = "Celular samsung J6 Android 14"
        produto.preco = 1223.79
        produto.quantidade = 100        
        self._service.cadatrar(produto)
        
        produto.preco = 1500.85
        produto.quantidade = 246       
        self._service.atualizar(produto)
        
        produto2 = Produto()
        produto2.nome = "Celular samsung A20"
        produto2.descricao = "Celular samsung A20 Android 14"
        produto2.preco = 1223.79
        produto2.quantidade = 100        
        self._service.cadatrar(produto2)
        
        produto3 = Produto()
        produto3.nome = "Celular samsung A35"
        produto3.descricao = "Celular samsung A35 Android 14"
        produto3.preco = 1223.79
        produto3.quantidade = 100        
        self._service.cadatrar(produto3)
        
        quantidade_produtos = self._service.controle_estoque()
        quantidade_esperada = produto.quantidade + produto2.quantidade + produto3.quantidade
        self.assertEqual(quantidade_esperada, quantidade_produtos)
        