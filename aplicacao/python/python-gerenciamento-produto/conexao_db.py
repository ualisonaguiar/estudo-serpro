import mysql.connector

class  ConexaoDB:
    def __init__(self) -> None:
        self._conexao = None
        
    def abre_conexao(self):
        if self._conexao is None :
            try:
                self._conexao = mysql.connector.connect(
                    host = "localhost",
                    user = "root",
                    password = "abcd1234",
                    database = "gerenciamento_produto"
                )
            except mysql.connector.Error as e:
                print(f'Erro ao estabelecer conexão com o banco dados: {e}')

        return self._conexao