class Solution:
    def findMedianSortedArrays(self, nums1: [int], nums2: [int]) -> float:
    	nums3: [] = []
    	self.__adicionar_numero_lista(nums1, nums3)
    	self.__adicionar_numero_lista(nums2, nums3)
    	nums3 = sorted(nums3)
    	tamanho: int = len(nums3)

    	if tamanho % 2 == 0:
    		valor_centro_01 = nums3[(tamanho // 2) - 1]
    		valor_centro_02 = nums3[tamanho // 2]
    		median: float = (valor_centro_01 + valor_centro_02) / 2
    	else:
    		median: float = nums3[tamanho // 2]

    	return median

    def __adicionar_numero_lista(self, lista: [], lista_adicionada: []) -> None:
    	for numero in lista:
    		lista_adicionada.append(numero)