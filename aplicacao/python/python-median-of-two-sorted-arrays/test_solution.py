import unittest
from solution import Solution

class TestSolution(unittest.TestCase):
	def __init__(self, method_name: str = 'runTest') -> None:
		super().__init__(method_name)
		self.__service = Solution()

	def test_find_median_sorted_arrays_exemplo_01(self) -> None:
		l1: [] = [1, 3]
		l2: [] = [2]
		self.assertEqual(2.0, self.__service.findMedianSortedArrays(l1, l2))


	def test_find_median_sorted_arrays_exemplo_02(self) -> None:
		l1: [] = [1, 2]
		l2: [] = [3, 4]
		self.assertEqual(2.5, self.__service.findMedianSortedArrays(l1, l2))