from aluno_service import AlunoService
from aluno import Aluno

aluno_service = AlunoService()

def menu() -> int :
    print("###############################################################")
    print("Escolha uma opção:")
    print("1. Informar a nota de um aluno")
    print("2. Calcular média da turma")
    print("3. Calcular a mediana")
    print("4. Calcular o desvio padrão")
    print("5. Calcular tudo")
    print("6. Listagem dos alunos")
    print("7. Sair")
    print("###############################################################")
    return int(input("Escolha uma opção acima: "))

def formatar_titulo(message: str):
    print(f'****************************** {message} ******************************')

def cadastrar_aluno() -> None :
    aluno = Aluno()
    aluno.nome = input("Nome do Aluno: ")
    aluno.nota = float(input("Nota do aluno: "))
    aluno_service.adicionar(aluno)

def listagem_aluno() -> None:    
    alunos = aluno_service.listagem()    
    for (aluno) in alunos:
        print(f'----------------- ID: {aluno.id} ---------------------------')
        print(f'Aluno: {aluno.nome}')
        print(f'Nota: {aluno.nota}')
        
def calcular_media() -> None:
    print('-------------------------------------------------------')
    print(f'Media da turma: {aluno_service.calcular_media()}')
    print('-------------------------------------------------------')

def calcular_mediana() -> None:
    print('-------------------------------------------------------')
    print(f'Mediana da turma: {aluno_service.calcular_mediana()}')
    print('-------------------------------------------------------')

def calcular_desvio_padrao() -> None:
    print('-------------------------------------------------------')
    print(f'Desvio padrão: {aluno_service.calcular_devio_padrao()}')
    print('-------------------------------------------------------')

def calcular_tudo() -> None:
    calcular_media()
    calcular_mediana()
    calcular_desvio_padrao()

def escolha_opcao() -> None:
    opcao = menu()
    if opcao == 1:
        formatar_titulo('Cadastro de Notas')
        cadastrar_aluno()
    elif opcao == 2:
        formatar_titulo('Calcular média')
        calcular_media()
    elif opcao == 3:
        formatar_titulo("Calcular mediana")
        calcular_mediana()
    elif opcao == 4:
        print("Calcular desvio padrao")
        calcular_desvio_padrao()
    elif opcao == 5:
        print("Calcular tudo")
        calcular_tudo()
    elif opcao == 6:
        formatar_titulo('Listagem dos alunos')
        listagem_aluno()
    elif opcao == 7:
        print('Saindo do programa.....')
        exit()
        
    escolha_opcao()
        
escolha_opcao()