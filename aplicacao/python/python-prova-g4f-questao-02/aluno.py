class Aluno:
    def __init__(self) -> None:
        self._id: int = 0
        self._nome: str = '';
        self._nota: float = 0.00
    
    @property
    def id(self) -> int:
        return self._id
    
    @id.setter
    def id(self, id: int) -> None:
        self._id = id
    
    @property
    def nome(self) -> str:
        return self._nome
    
    @nome.setter
    def nome(self, nome: str) -> None:
        self._nome = nome    
    
    @property
    def nota(self) -> float:
        return self._nota
    
    @nota.setter
    def nota(self, nota: float) -> None:
        self._nota = nota