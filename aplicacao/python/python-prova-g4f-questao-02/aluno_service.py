import math
from aluno import Aluno

class AlunoService():
    def __init__(self) -> None:
        self._alunos = []
    
    def adicionar(self, aluno: Aluno) -> Aluno:
        if len(self._alunos) != 0:
            ultimo_aluno = self._alunos[-1]
            aluno.id = ultimo_aluno.id + 1
        else:
            aluno.id = 1
        self._alunos.append(aluno)
        return aluno
    
    def listagem(self) -> [Aluno]:
        return self._alunos
    
    def calcular_media(self) -> float:
        notas_alunos= self.__notas_alunos()
        if len(notas_alunos) == 0:
            return 0.0
        else:
            nota_geral = 0.0
            for nota in notas_alunos:
                nota_geral += nota
                
            return nota_geral / len(notas_alunos)
    
    def __notas_alunos(self) -> [float]:
        alunos = self.listagem()
        if len(alunos) == 0:
            return []
        else:
            notas_alunos = []
            for aluno in alunos:
                notas_alunos.append(aluno.nota)
            return notas_alunos
    
    def calcular_mediana(self) -> float:
        notas_alunos = sorted(self.__notas_alunos())
        if len(notas_alunos) == 0:
            return 0.0
        else:
            if len(notas_alunos) % 2 == 0:
                meio_a = len(notas_alunos) // 2
                meio_b = meio_a - 1
                return (notas_alunos[meio_a] + notas_alunos[meio_b]) / 2
            else:
                return notas_alunos[len(notas_alunos) // 2]
        
    def calcular_devio_padrao(self) -> float:
        notas_alunos = self.__notas_alunos()
        if len(notas_alunos) == 0:
            return 0.0        
        media = self.calcular_media()
        quadrado_difenca = 0.0
        
        for nota in notas_alunos:
            quadrado_difenca += (nota - media)**2
        
        varianca = quadrado_difenca / len(notas_alunos)
        return math.floor(math.sqrt(varianca))