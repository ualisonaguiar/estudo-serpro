import csv
from contato_dao import ContatoDAO
from contato import Contato

class ContatoService:
	def __init__(self):
		self.__repository = ContatoDAO()

	def importar_arquivo_salvar(self) -> None:
		contatos: [Contato] = self.importar_contato_arquivo()
		for contato in contatos:
			self.salvar(contato)

	def salvar(self, contato: Contato) -> Contato:
		if contato.id == 0:
			self.__repository.inserir(contato)
		else:
			self.__repository.atualizar(contato)

	def listagem(self) -> [Contato]:
		return self.__repository.listagem()

	def detalhar(self, id_contato: int) -> Contato:
		contato = self.__repository.detalhar(id_contato)
		if contato is None:
			raise ValueError(f'O contato {contato.id} não exite no banco de dados')
		return contato

	def excluir(self, contato: Contato) -> None:
		contato_db = self.detalhar(contato.id)
		self.__repository.excluir(contato_db.id)

	def pesquisa_contato_email(self, email: str) -> Contato:
		return self.__repository.pesquisa_contato_email(email)

	def importar_contato_arquivo(self) -> [Contato]:
		contatos: [] = []
		with open('contatos.csv') as arquivo:
			linhas: [] = csv.reader(arquivo)
			for index,linha in enumerate(linhas):
				if index != 0:
					contatos.append(self.__prepare_linha_entidade(linha))

		return self.__elimine_contato_duplicado(contatos)

	def __elimine_contato_duplicado(self, contatos: []) -> [Contato]:
		contato_email_existente = []
		contato_nao_duplicado: [Contato] = []

		for contato in contatos:
			if contato.email not in contato_email_existente:
				contato_nao_duplicado.append(contato)
				contato_email_existente.append(contato.email)

		return contato_nao_duplicado


	def __prepare_linha_entidade(self, linha: []) -> Contato:
		contato = Contato()
		contato.nome = linha[0]
		contato.sobrenome = linha[1]
		contato.numero_telefone = linha[2]
		contato.email = linha[3]
		contato.data_aniversario = linha[4]
		return contato