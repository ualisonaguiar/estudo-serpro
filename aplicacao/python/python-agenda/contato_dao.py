from conexao_db import ConexaoDb
from contato import Contato

class ContatoDAO:

	def __init__(self):
		self.__criar_tabela()

	def inserir(self, contato: Contato) -> Contato:
		with ConexaoDb() as conexao:
			cursor = conexao.cursor()
			values = (contato.nome, contato.sobrenome, contato.numero_telefone, contato.email, contato.data_aniversario,)
			cursor.execute("insert into contato (nome, sobrenome, numero_telefone, email, data_aniversario) values (?, ?, ?, ?, ?)", values)
			conexao.commit()
			contato.id = cursor.lastrowid
			cursor.close()
			return contato

	def atualizar(self, contato: Contato) -> None:
		with ConexaoDb() as conexao:
			cursor = conexao.cursor()
			values = (contato.nome, contato.sobrenome, contato.numero_telefone, contato.email, contato.data_aniversario, contato.id,)
			cursor.execute("update contato set nome = ?, sobrenome = ?, numero_telefone = ?, email = ?, data_aniversario = ? where id = ?", values)
			conexao.commit()
			cursor.close()

	def listagem(self) -> [Contato]:
		with ConexaoDb() as conexao:
			cursor = conexao.cursor()
			cursor.execute("select * from contato order by nome asc")
			contatos_data = cursor.fetchall()
			cursor.close()
			contatos: [Contato] = []

			for contato_data in contatos_data:
				contatos.append(self.__populate_entity(contato_data))

			return contatos

	def detalhar(self, id_contato: int) -> Contato:
		with ConexaoDb() as conexao:
			cursor = conexao.cursor()
			cursor.execute("select * from contato where id = ?", (id_contato,))
			contato_data = cursor.fetchone()
			cursor.close()
			return self.__populate_entity(contato_data);

	def excluir(self, id_contato: int) -> None:
		with ConexaoDb() as conexao:
			cursor = conexao.cursor()
			cursor.execute('delete from contato where id = ?', (id_contato,))
			conexao.commit()

	def pesquisa_contato_email(self, email: str) -> Contato:
		with ConexaoDb() as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from contato where email = ?', (email,))
			contato_data = cursor.fetchone()
			cursor.close()
			return self.__populate_entity(contato_data);

	def __populate_entity(self, result) -> Contato:
		if result:
			id_contato, nome, sobrenome, numero_telefone, email, data_aniversario = result
			contato = Contato()
			contato.id = id_contato
			contato.nome = nome
			contato.sobrenome = sobrenome
			contato.numero_telefone = numero_telefone
			contato.email = email
			contato.data_aniversario = data_aniversario
			return contato

	def __criar_tabela(self) -> None:
		with ConexaoDb() as conexao:
			cursor = conexao.cursor()
			cursor.execute('''
				CREATE TABLE IF NOT EXISTS contato(
					id integer not null primary key,
					nome varchar(250) not null,
					sobrenome varchar(250) not null,
					numero_telefone varchar(16) not null,
					email varchar(50) not null,
					data_aniversario date not null
				)
			''')
			conexao.commit()