import datetime

class Contato:
	def __init__(self):
		self.__id: int = 0
		self.__nome: str = ''
		self.__sobrenome: str = ''
		self.__numero_telefone: str = ''
		self.__email: str = ''
		self.__data_aniversario: datetime = datetime

	@property
	def id(self) -> int:
		return self.__id

	@id.setter
	def id(self, id: int) -> None:
		self.__id = id

	@property
	def nome(self) -> str:
		return self.__nome

	@nome.setter
	def nome(self, nome: str) -> None:
		self.__nome = nome

	@property
	def sobrenome(self) -> str:
		return self.__sobrenome

	@sobrenome.setter
	def sobrenome(self, sobrenome:str) -> None:
		self.__sobrenome = sobrenome

	@property
	def numero_telefone(self) -> str:
		return self.__numero_telefone

	@numero_telefone.setter
	def numero_telefone(self, numero_telefone: str) -> None:
		self.__numero_telefone = numero_telefone

	@property
	def email(self) -> str:
		return self.__email

	@email.setter
	def email(self, email) -> None:
		self.__email = email

	@property
	def data_aniversario(self) -> datetime:
		return self.__data_aniversario

	@data_aniversario.setter
	def data_aniversario(self, data_aniversario: datetime) -> None:
		self.__data_aniversario = data_aniversario