import csv
from contato_arquivo import ContatoArquivo
from contato import Contato

class ContatoServiceFile:
	def __init__(self):
		self.__repository = ContatoArquivo()

	def salvar(self, contato: Contato) -> Contato:
		if contato.id == 0:
			self.__repository.inserir(contato)
		else:
			self.__repository.atualizar(contato)
		return contato

	def detalhar(self, id_contato: int) -> Contato:
		contato = self.__repository.detalhar(id_contato)
		if contato is None:
			raise ValueError(f'O contato {contato.id} não exite no banco de dados')
		return contato

	def pesquisa_contato_email(self, email: str) -> Contato:
		return self.__repository.pesquisa_contato_email(email)

	def excluir(self, contato: Contato) -> None:
		contato_db = self.detalhar(contato.id)
		self.__repository.excluir(contato_db.id)		

	def listagem(self) -> [Contato]:
		return self.__repository.listagem()		
