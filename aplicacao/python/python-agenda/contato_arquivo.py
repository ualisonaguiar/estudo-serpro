import os
from contato import Contato

class ContatoArquivo:
	def __init__(self) -> None:
		self._path_arquivo = self.__criar_arquivo()
		self._separador_aquivo: str = ','


	def listagem(self) -> [Contato]:
		contatos: [Contato] = []
		with open(self._path_arquivo, 'r') as arquivo:
			linhas =  arquivo.readlines()

		for linha in linhas[1:]:
			contatos.append(self.__populate_entity(linha.strip()))

		return contatos

	def inserir(self, contato: Contato) -> Contato:		
		contato.id = self.__gerar_id()
		with open(self._path_arquivo, 'a+') as arquivo:
			arquivo.write(self.__populate_linha_arquivo(contato))
			arquivo.write('\n')
		return contato

	def atualizar(self, contato: Contato) -> Contato:
		contatos = self.listagem()
		for index,contato_file in enumerate(contatos):
			if contato_file.id == contato.id:
				contatos[index] = contato
		self.__atualizar_arquivo(contatos)

	def detalhar(self, id_contato: int) -> Contato:
		contatos = self.listagem()
		for contato_file in contatos:
			if contato_file.id == id_contato:
				return contato_file

	def pesquisa_contato_email(self, email: str) -> Contato:
		contatos = self.listagem()
		for contato_file in contatos:
			if contato_file.email == email:
				return contato_file

	def excluir(self, id_contato: int) -> None:
		contatos_antes = self.listagem()
		contatos_novos = []

		for contato in contatos_antes:
			if contato.id != id_contato:
				contatos_novos.append(contato)

		self.__atualizar_arquivo(contatos_novos)

	def __atualizar_arquivo(self, contatos) -> None:
		with open(self._path_arquivo, 'w') as arquivo:
			self.__criar_cabecalho_arquivo(arquivo)
			for contato in contatos:
				arquivo.write(self.__populate_linha_arquivo(contato))
				arquivo.write('\n')		
	
	def __gerar_id(self) -> int:
		contatos: [Contato] = self.listagem()
		if len(contatos):
			ultimo_contato = contatos[-1]
			return ultimo_contato.id + 1
		else:
			return 1

	def __criar_arquivo(self) -> str:
		path_arquivo: str = 'file_contato.csv'
		if os.path.exists(path_arquivo) == False:
			with open(path_arquivo, 'a+') as arquivo:
				self.__criar_cabecalho_arquivo(arquivo)
		return path_arquivo

	def __criar_cabecalho_arquivo(self, arquivo) -> None:
		arquivo.write('id,Nome,Sobrenome,Número de Telefone,E-mail,Data de Aniversário')
		arquivo.write('\n')		

	def __populate_linha_arquivo(self, contato: Contato) -> str:
		linha = str(contato.id) + self._separador_aquivo
		linha += contato.nome + self._separador_aquivo
		linha += contato.sobrenome + self._separador_aquivo
		linha += contato.numero_telefone + self._separador_aquivo
		linha += contato.email + self._separador_aquivo
		linha += str(contato.data_aniversario)

		return linha

	def __populate_entity(self, result) -> Contato:
		if result:
			id_contato, nome, sobrenome, numero_telefone, email, data_aniversario = result.split(self._separador_aquivo)
			contato = Contato()
			contato.id = int(id_contato)
			contato.nome = nome
			contato.sobrenome = sobrenome
			contato.numero_telefone = numero_telefone
			contato.email = email
			contato.data_aniversario = data_aniversario
			return contato
