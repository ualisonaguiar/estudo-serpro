import unittest
import datetime
from contato_service import ContatoService
from contato import Contato

class TestContatoService(unittest.TestCase):
	def __init__(self, method_name = 'runtest') -> None:
		super().__init__(method_name)
		self.__service = ContatoService()

	def test_importa_contato(self) -> None:
		self.assertEqual(27, len(self.__service.importar_contato_arquivo()))

	def test_importar_salvar(self) -> None:
		self.__service.importar_arquivo_salvar()
		self.assertEqual(27, len(self.__service.listagem()))

	def test_pesquisa_contato(self) -> None:
		contato = Contato()
		contato.nome = 'Ualison'
		contato.sobrenome = 'Aguiar'
		contato.numero_telefone = '(61) 98601-1959'
		contato.email = 'ualison.aguiar@gmail.com'
		contato.data_aniversario = datetime.datetime(1988, 2, 1)
		self.__service.salvar(contato)
		self.assertTrue(contato.id != 0)
		contato_db = self.__service.pesquisa_contato_email(contato.email)
		self.assertEqual(contato.id, contato_db.id)

	def test_pesquisa_alterar_contato(self) -> None:
		contato = Contato()
		contato.nome = 'João Paulo'
		contato.sobrenome = 'Rodrigues'
		contato.numero_telefone = '(61) 98601-1959'
		contato.email = 'joaopaulorodrigues@gmail.com'
		contato.data_aniversario = datetime.datetime(2010, 8, 20)
		self.__service.salvar(contato)

		contato_db = self.__service.detalhar(contato.id)
		contato_db.sobrenome += ' Frota'
		contato_db.numero_telefone = '(61) 3488-4667'

		self.__service.salvar(contato_db)

		contato_db1 = self.__service.detalhar(contato.id)
		self.assertEqual(contato_db.nome, contato_db1.nome)
		self.assertEqual(contato_db.sobrenome, contato_db1.sobrenome)
		self.assertEqual(contato_db.numero_telefone, contato_db1.numero_telefone)
		self.assertEqual(contato_db.email, contato_db1.email)
		self.assertEqual(contato_db.data_aniversario, contato_db1.data_aniversario)


	def test_excluir_importados(self) -> None:
		contatos = self.__service.listagem()
		for contato in contatos:
			self.__service.excluir(contato)
		self.assertEqual(0, len(self.__service.listagem()))