import sqlite3

class ConexaoDb(object):
	def __init__(self):
		self.conexao = None

	def __enter__(self):
		if self.conexao is None:
			self.conexao = sqlite3.connect('agenda.db')
		return self.conexao

	def __exit__(self, exe_type, exc_value, traceback):
		if self.conexao:
			self.conexao.close()

