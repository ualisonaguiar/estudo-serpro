import unittest
import datetime
import random
from contato_service_file import ContatoServiceFile
from contato import Contato

class TestContatoServiceFile(unittest.TestCase):

	def __init__(self, method_name = 'runtest') -> None:
		super().__init__(method_name)
		self.__service = ContatoServiceFile()

	def test_salvar_inserir(self) -> None:
		contato = Contato()
		contato.nome = 'João Paulo'
		contato.sobrenome = 'Rodrigues'
		contato.numero_telefone = '(61) 98601-1959'
		contato.email = 'joaopaulorodrigues@gmail.com'
		contato.data_aniversario = datetime.datetime(2010, 8, 20)
		self.__service.salvar(contato)
		self.assertTrue(contato.id != 0)


	def test_salvar_atualizar(self) -> None:
		contato = Contato()
		contato.nome = 'Ualison'
		contato.sobrenome = 'Aguiar'
		contato.numero_telefone = '(61) 98601-1959'
		contato.email = 'joaopaulorodrigues@gmail.com'
		contato.data_aniversario = datetime.datetime(2010, 8, 20)
		self.__service.salvar(contato)
		contato.email = 'ualison.aguiar@gmail.com'
		self.__service.salvar(contato)
		
		contato_db = self.__service.detalhar(contato.id)
		self.assertEqual(contato.email, contato_db.email)

	def test_pesquisa(self) -> None:
		contato = Contato()
		contato.nome = 'Ualison'
		contato.sobrenome = 'Aguiar'
		contato.numero_telefone = '(61) 98601-1959'
		contato.data_aniversario = datetime.datetime(1988, 2, 1)
		contato.email = 'ualison.aguiar@gmail.com__' + str(random.randint(1, 1000))
		self.__service.salvar(contato)
		self.assertTrue(contato.id != 0)
		contato_db = self.__service.pesquisa_contato_email(contato.email)
		self.assertEqual(contato.id, contato_db.id)

	def test_excluir(self) -> None:
		contato = Contato()
		contato.nome = 'Ualison'
		contato.sobrenome = 'Aguiar'
		contato.numero_telefone = '(61) 98601-1959'
		contato.data_aniversario = datetime.datetime(1988, 2, 1, )
		contato.email = 'ualison.aguiar@gmail.com__' + str(random.randint(1, 1000))
		self.__service.salvar(contato)
		self.assertTrue(contato.id != 0)
		self.__service.excluir(contato)
		self.assertIsNone(self.__service.pesquisa_contato_email(contato.email))

	def test_excluir_tudo(self) -> None:
		contatos = self.__service.listagem()
		for contato in contatos:
			self.__service.excluir(contato)
		self.assertEqual(0, len(self.__service.listagem()))