from conexao_dao import ConexaoDAO
from filme import Filme

class FilmeDAO:
	def __init__(self):
		conexao_dao = ConexaoDAO()
		self.__conexao = conexao_dao.abre_conexao()
		self.__criar_tabela()

	def adicionar(self, filme: Filme) -> Filme:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			values = (filme.nome, filme.cinema_id, filme.dt_inicio, filme.dt_fim, filme.valor,)
			cursor.execute('insert into filme (nome, cinema_id, dt_inicio, dt_fim, valor) values (?, ?, ?, ?, ?)', values)
			conexao.commit()
			filme.id = cursor.lastrowid
		return filme

	def alterar(self, filme: Filme) -> Filme:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			values = (filme.nome, filme.cinema_id, filme.dt_inicio, filme.dt_fim, filme.valor, filme.id)
			cursor.execute('update filme set nome = ?, cinema_id = ?, dt_inicio = ?, dt_fim = ?, valor = ? where id = ?', values)
			conexao.commit()
			return filme

	def detalhar(self, id: int) -> Filme:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from filme where id = ?', (id,))
			filme_result = cursor.fetchone()
			cursor.close()
			return self.__populate_domain(filme_result)

	def listagem(self) -> [Filme]:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from filme order by dt_fim desc')
			filmes_data = cursor.fetchall()
			cursor.close()

			filmes = []
			for indice,filme in enumerate(filmes_data):
				filmes.append(self.__populate_domain(filme))
			return filmes

	def filmes_por_cinema(self, cinema_id: int) -> [Filme]:
		filmes = []
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from filme where cinema_id = ? order by dt_fim desc', (cinema_id, ))
			filmes_result = cursor.fetchall()
			cursor.close()

			for indice,filme_result in enumerate(filmes_result):
				filmes.append(self.__populate_domain(filme_result))


		return filmes


	def __populate_domain(self, result):
		if result:
			filme = Filme()
			id_filme, nome, cinema_id, dt_inicio, dt_fim, valor = result
			filme.nome = nome
			filme.valor = valor
			filme.cinema_id = cinema_id
			filme.id = id_filme
			filme.dt_fim = dt_fim
			filme.dt_inicio = dt_inicio
			filme.valor = valor
			return filme

	def __criar_tabela(self) -> None:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('''
			    CREATE TABLE IF NOT EXISTS filme (
			        id INTEGER PRIMARY KEY AUTOINCREMENT,
			        nome varchar(250) not null,
			        cinema_id INTEGER not null,
			        dt_inicio datetime,
			        dt_fim datetime,
			        valor decimal(10, 2),
			        FOREIGN key (cinema_id) references cinema(id)
			    )
			''')
			cursor.close()			