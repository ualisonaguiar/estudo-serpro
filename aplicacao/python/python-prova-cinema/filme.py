from datetime import datetime

class Filme:
	def __init__(self) -> None:
		self._id: int = 0
		self._nome: str = ''
		self._cinema_id: int = 0
		self._dt_inicio: datetime = datetime.now()
		self._dt_fim: datetime = datetime.now()
		self._valor: float = 0.00


	@property
	def id(self) -> int:
		return self._id

	@id.setter
	def id(self, id: int) -> None:
		self._id = id

	@property
	def nome(self) -> str:
		return self._nome

	@nome.setter
	def nome(self, nome: str) -> None:
		self._nome = nome

	@property
	def cinema_id(self) -> int:
		return self._cinema_id

	@cinema_id.setter
	def cinema_id(self, cinema_id: int) -> None:
		self._cinema_id = cinema_id

	@property
	def dt_inicio(self) -> datetime:
		return self._dt_inicio

	@dt_inicio.setter
	def dt_inicio(self, dt_inicio: datetime) -> None:
		self._dt_inicio = dt_inicio

	@property
	def dt_fim(self) -> datetime:
		return self._dt_fim

	@dt_fim.setter
	def dt_fim(self, dt_fim: datetime) -> None:
		self._dt_fim = dt_fim		

	@property
	def valor(self) -> float:
		return self._valor

	@valor.setter
	def valor(self, valor) -> None:
		self._valor = valor