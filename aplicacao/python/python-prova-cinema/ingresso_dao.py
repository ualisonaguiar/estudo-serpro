from conexao_dao import ConexaoDAO
from ingresso import Ingresso

class IngressoDAO:
	def __init__(self):
		conexao = ConexaoDAO()
		self.__conexao = conexao.abre_conexao()
		self.__criar_tabela()

	def adicionar(self, ingresso: Ingresso) -> Ingresso:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			values = (ingresso.nome_pessoa, ingresso.filme_id, ingresso.quantidade, ingresso.data_compra, ingresso.valor, ingresso.assento,)
			cursor.execute('insert into ingresso (nome_pessoa, filme_id, quantidade, data_compra, valor, assento) values (?, ?, ?, ?, ?, ?)', values)
			conexao.commit
			ingresso.id = cursor.lastrowid
			cursor.close()
			return ingresso

	def alterar(self, ingresso: Ingresso) -> Ingresso:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			values = (ingresso.nome_pessoa, ingresso.filme_id, ingresso.quantidade, ingresso.data_compra, ingresso.valor, ingresso.assento, ingresso.id,)
			cursor.execute('update ingresso set nome_pessoa = ?, filme_id = ?, quantidade = ?, data_compra = ?, valor = ?, assento = ? where id = ?', values)
			conexao.commit()
			cursor.close()
			return ingresso

	def detalhar(self, id: int) -> Ingresso:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from ingresso where id = ?', (id,))
			ingresso_data = cursor.fetchone()
			cursor.close()
			return self.__populate_domain(ingresso_data)

	def excluir(self, id: int) -> None:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('delete from ingresso where id = ?', (id,))
			conexao.commit()
			cursor.close()

	def listagem(self) -> [Ingresso]:
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from ingresso')
			ingressos_results = cursor.fetchall()
			cursor.close()
			ingressos = []

			for ingresso_data in ingressos_results:
				ingressos.append(self.__populate_domain(ingresso_data))

			return ingressos

	def __populate_domain(self, result) -> Ingresso:		
		if result:
			ingresso = Ingresso()
			id_ingresso, nome_pessoa, filme_id, quantidade, data_compra, valor, assento = result
			ingresso.quantidade = quantidade
			ingresso.filme_id = filme_id
			ingresso.valor = valor
			ingresso.data_compra = data_compra
			ingresso.nome_pessoa = nome_pessoa
			ingresso.id = id_ingresso
			ingresso.assento = assento
			return ingresso

	def __criar_tabela(self):
		with self.__conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('''
				CREATE TABLE IF NOT EXISTS ingresso(
					id integer not null primary key AUTOINCREMENT,
					nome_pessoa varchat(150) not null,
					filme_id integer not null,
					quantidade integer not null,
					data_compra datetime not null,
					valor decimal(10, 2) not null,
					assento integer not null,
					foreign key (filme_id) references filme(id)
				)
			''')