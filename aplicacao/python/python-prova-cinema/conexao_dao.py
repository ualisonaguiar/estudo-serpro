import sqlite3

class ConexaoDAO:
	def __init__(self) -> None:
		self._conexao = None

	def abre_conexao(self):
		if self._conexao is None:
			self._conexao = sqlite3.connect('cinema.db')

		return self._conexao