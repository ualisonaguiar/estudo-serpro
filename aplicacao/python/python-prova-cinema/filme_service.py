from filme import Filme
from filme_dao import FilmeDAO

class FilmeService:
	def __init__(self):
		self.__repository = FilmeDAO()

	def salvar(self, filme: Filme) -> Filme:
		if filme.id == 0:
			self.__repository.adicionar(filme)
		else:
			self.__repository.alterar(filme)
		return filme


	def detalhar(self, id: int) -> Filme:
		filme = self.__repository.detalhar(id)
		if filme is None:
			raise ValueError(f'Filme ID: {id} não encontrado')

		return filme

	def listagem(self) -> [Filme]:
		return self.__repository.listagem();

	def filmes_por_cinema(self, cinema_id: int) -> [Filme]:
		return self.__repository.filmes_por_cinema(cinema_id)