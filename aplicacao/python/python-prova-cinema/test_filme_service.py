import unittest
from datetime import datetime
from dateutil.relativedelta import relativedelta
from filme import Filme
from cinema import Cinema
from filme_service import FilmeService
from cinema_service import CinemaService

class TestFilmeService(unittest.TestCase):

	def __init__(self, method_name = 'runTest'):
		super().__init__(method_name)
		self.__service = FilmeService()
		self.__cinema = None

	def test_adicionar(self):
		cinema = self.__cinema_test()
		filme = Filme()
		filme.nome = 'Era do Gelo 4'
		filme.cinema_id = cinema.id
		filme.dt_inicio = datetime(2023, 8, 18, 10, 0, 0)
		filme.dt_fim = filme.dt_inicio + relativedelta(months=3)
		filme.valor = 18.95
		self.__service.salvar(filme)
		self.assertNotEqual(0, filme.id)

	def test_atualizar(self):
		cinema = self.__cinema_test()
		
		filme = Filme()
		filme.nome = 'Jonh Wich 4'
		filme.cinema_id = cinema.id
		filme.dt_inicio = datetime(2023, 5, 18, 10, 0, 0)
		filme.dt_fim = filme.dt_inicio + relativedelta(months=3)
		filme.valor = 50.95
		self.__service.salvar(filme)

		filme.dt_fim = filme.dt_inicio + relativedelta(months=4)
		self.__service.salvar(filme)

		filme_db = self.__service.detalhar(filme.id)

		self.assertEqual(filme.id, filme_db.id)
		self.assertEqual(filme.valor, filme_db.valor)

	def test_listagem(self):
		cinema = self.__cinema_test()

		filme = Filme()
		filme.nome = 'Besouro Azul'
		filme.cinema_id = cinema.id
		filme.dt_inicio = datetime(2023, 8, 18, 16, 30, 0)
		filme.dt_fim = filme.dt_inicio + relativedelta(months=3)
		filme.valor = 120.95
		self.__service.salvar(filme)

		filme_encontrado = False
		for indice,filme_db in enumerate(self.__service.listagem()):
			if filme_db.id == filme.id:
				filme_encontrado = True
				break
		self.assertTrue(filme_encontrado)

	def test_listagem_filmes_cinema(self):
		data_atual = datetime.now()

		cinema_01 = self.__cadastrar_cinema_test(f'Shopping -- {data_atual}')
		cinema_02 = self.__cadastrar_cinema_test(f'Conjunto Nacional -- {data_atual}')

		filme1 = Filme()
		filme1.nome = 'Besouro Azul'
		filme1.cinema_id = cinema_02.id
		filme1.dt_inicio = datetime(2023, 8, 18, 16, 30, 0)
		filme1.dt_fim = filme1.dt_inicio + relativedelta(months=3)
		filme1.valor = 120.95
		self.__service.salvar(filme1)

		filme2 = Filme()
		filme2.nome = 'Elementos'
		filme2.cinema_id = cinema_02.id
		filme2.dt_inicio = datetime(2023, 8, 15, 16, 30, 0)
		filme2.dt_fim = filme2.dt_inicio + relativedelta(months=3)
		filme2.valor = 59.99
		self.__service.salvar(filme2)

		filme3 = Filme()
		filme3.nome = 'Barbie'
		filme3.cinema_id = cinema_02.id
		filme3.dt_inicio = datetime(2023, 8, 15, 16, 30, 0)
		filme3.dt_fim = filme3.dt_inicio + relativedelta(months=3)
		filme3.valor = 59.99
		self.__service.salvar(filme3)

		filme4 = Filme()
		filme4.nome = 'Mega Tubarão 2'
		filme4.cinema_id = cinema_01.id
		filme4.dt_inicio = datetime(2023, 8, 15, 16, 30, 0)
		filme4.dt_fim = filme4.dt_inicio + relativedelta(months=3)
		filme4.valor = 59.99
		self.__service.salvar(filme4)

		filme5 = Filme()
		filme5.nome = 'MISSÃO IMPOSSÍVEL - ACERTO DE CONTAS PARTE 1'
		filme5.cinema_id = cinema_01.id
		filme5.dt_inicio = datetime(2023, 8, 15, 16, 30, 0)
		filme5.dt_fim = filme5.dt_inicio + relativedelta(months=3)
		filme5.valor = 59.99
		self.__service.salvar(filme5)

		self.assertEqual(2, len(self.__service.filmes_por_cinema(cinema_01.id)))
		self.assertEqual(3, len(self.__service.filmes_por_cinema(cinema_02.id)))


	def __cinema_test(self) -> Cinema:
		if self.__cinema is None:
			self.__cinema = self.__cadastrar_cinema_test('Pátio Brasil')
		return self.__cinema

	def __cadastrar_cinema_test(self, nome) -> Cinema:
		cinema = Cinema()
		cinema.nome = nome
		cinema_service = CinemaService()
		cinema_service.salvar(cinema)
		return cinema
