from conexao_dao import ConexaoDAO
from cinema import Cinema

class CinemaDAO:
	def __init__ (self) -> None:
		conexao_dao = ConexaoDAO()
		self._conexao = conexao_dao.abre_conexao()
		self.__criar_tabela()

	def adicionar(self, cinema: Cinema) -> Cinema:
		with self._conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('insert into cinema (nome) values (?)', (cinema.nome,))
			conexao.commit()
			cinema.id = cursor.lastrowid
			cursor.close()

		return cinema

	def alterar(self, cinema: Cinema) -> Cinema:
		with self._conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('update cinema set nome = ? where id = ?', (cinema._nome, cinema.id,))
			conexao.commit()
			cursor.close()

		return cinema

	def detalhar(self, id: int) -> Cinema:
		with self._conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from cinema where id = ?', (id,))
			cinema_data = cursor.fetchone()
			cursor.close()
			return self.populate_domain(cinema_data)

	def listagem(self) -> [Cinema]:
		with self._conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('select * from cinema order by nome asc')
			cinemas_datas = cursor.fetchall()
			cursor.close
			cinemas = []

			for cinema_data in cinemas_datas:
				cinemas.append(self.populate_domain(cinema_data))

			return cinemas

	def excluir(self, id: int) -> None:
		with self._conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('delete from cinema where id = ?', (id, ))
			conexao.commit()

	def populate_domain(self, result) -> Cinema:
		if result:
			cinema = Cinema()
			id_cinema, nome = result
			cinema.id = id_cinema
			cinema.nome = nome
			return cinema

	def __criar_tabela(self) -> None:
		with self._conexao as conexao:
			cursor = conexao.cursor()
			cursor.execute('CREATE TABLE IF NOT EXISTS cinema (id INTEGER PRIMARY KEY AUTOINCREMENT, nome varchar(250))')
			cursor.close()