from ingresso_dao import IngressoDAO
from ingresso import Ingresso
from filme_service import FilmeService

class IngressoService():
	def __init__(self) -> None:
		self.__repository = IngressoDAO()
		self.__service_filme = FilmeService()

	def salvar(self, ingresso: Ingresso) -> Ingresso:
		if ingresso.id == 0:
			filme = self.__service_filme.detalhar(ingresso.filme_id)
			ingresso.valor = ingresso.quantidade * filme.valor
			self.__repository.adicionar(ingresso)
		else:
			self.__repository.alterar(ingresso)

	def detalhar(self, id: int) -> Ingresso:
		ingresso = self.__repository.detalhar(id)
		if ingresso is None:
			raise ValueError('Ingresso não encontrado')
		return ingresso

	def excluir(self, ingresso: Ingresso) -> None:
		self.__repository.excluir(ingresso.id)

	def listagem(self) -> [Ingresso]:
		return self.__repository.listagem()