from datetime import datetime

class Ingresso:
	def __init__(self):
		self._id: int = 0
		self._nome_pessoa: str = ''
		self._filme_id: int = 0
		self._quantidade: int = 0
		self._data_compra: datetime = datetime.now()
		self._valor: float = 0.00
		self._assento: int = 0

	@property
	def id(self) -> int:
		return self._id

	@id.setter
	def id(self, id: int) -> None:
		self._id = id

	@property
	def nome_pessoa(self) -> str:
		return self._nome_pessoa

	@nome_pessoa.setter
	def nome_pessoa(self, nome_pessoa: str) -> None:
		self._nome_pessoa = nome_pessoa

	@property
	def filme_id(self) -> int:
		return self._filme_id

	@filme_id.setter
	def filme_id(self, filme_id: int) -> None:
		self._filme_id = filme_id

	@property
	def quantidade(self) -> int:
		return self._quantidade

	@quantidade.setter
	def quantidade(self, quantidade: int) -> None:
		self._quantidade = quantidade

	@property
	def data_compra(self) -> datetime:
		return self._data_compra

	@data_compra.setter
	def data_compra(self, data_compra) -> None:
		self._data_compra = data_compra

	@property
	def valor(self) -> float:
		return self._valor

	@valor.setter
	def valor(self, valor: int) -> None:
		self._valor = valor

	@property
	def assento(self) -> int:
		return self._assento

	@assento.setter
	def assento(self, assento: int) -> None:
		self._assento = assento