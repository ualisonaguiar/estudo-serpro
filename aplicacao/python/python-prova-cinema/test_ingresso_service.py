import unittest
from datetime import datetime
from dateutil.relativedelta import relativedelta
from cinema import Cinema
from filme import Filme
from ingresso import Ingresso
from ingresso_service import IngressoService
from cinema_service import CinemaService
from filme_service import FilmeService

class TestIngressoService(unittest.TestCase):
	
	def __init__(self, method_name = 'runTest') -> None:
		super().__init__(method_name)
		self.__service = IngressoService()
		self.__filme: Filme = None

	def test_cadastrar(self) -> None:
		filme = self.__cadastrar_cinema_filme_test()
		ingresso = Ingresso()
		ingresso.quantidade = 10
		ingresso.filme_id = filme.id
		ingresso.data_compra = datetime.now()
		ingresso._nome_pessoa = 'Ualison'
		ingresso.assento = 14
		self.__service.salvar(ingresso)
		self.assertNotEqual(0, ingresso.id)

	def test_alterar(self) -> None:
		filme = self.__cadastrar_cinema_filme_test()
		ingresso = Ingresso()
		ingresso.quantidade = 10
		ingresso.filme_id = filme.id
		ingresso.data_compra = datetime.now()
		ingresso.nome_pessoa = 'Ualison'
		ingresso.assento = 30
		self.__service.salvar(ingresso)
		ingresso.nome_pessoa = 'Ualison Aguiar'
		self.__service.salvar(ingresso)

		ingresso_db = self.__service.detalhar(ingresso.id)
		self.assertEqual(ingresso.nome_pessoa, ingresso_db.nome_pessoa)

	def test_excluir(self) -> None:
		filme = self.__cadastrar_cinema_filme_test()
		ingresso = Ingresso()
		ingresso.quantidade = 5
		ingresso.filme_id = filme.id
		ingresso.data_compra = datetime.now()
		ingresso.nome_pessoa = 'João Paulo'
		ingresso.assento = 13
		self.__service.salvar(ingresso)
		self.__service.excluir(ingresso)

		with self.assertRaises(ValueError):
			self.__service.detalhar(ingresso.id)

	def test_listagem(self) -> None:
		filme = self.__cadastrar_cinema_filme_test()
		ingresso = Ingresso()
		ingresso.quantidade = 1
		ingresso.filme_id = filme.id
		ingresso.data_compra = datetime.now()
		ingresso.nome_pessoa = 'Liz Rodrigues'
		ingresso.assento = 36
		self.__service.salvar(ingresso)		

		ingressos = self.__service.listagem()
		self.assertTrue(len(ingressos) > 0)

		valor_encontrado = False
		for index,ingresso_db in enumerate(ingressos):
			if ingresso_db.id == ingresso.id:
				valor_encontrado = True

		self.assertTrue(valor_encontrado)


	def test_conferer_valor_total(self) -> None:
		filme = self.__cadastrar_cinema_filme_test()
		ingresso = Ingresso()
		ingresso.quantidade = 15
		ingresso.filme_id = filme.id
		ingresso.data_compra = datetime.now()
		ingresso.nome_pessoa = 'Liz Rodrigues'
		ingresso.assento = 76
		self.__service.salvar(ingresso)

		ingresso_db = self.__service.detalhar(ingresso.id)
		valor_total_ingresso = ingresso.quantidade * filme.valor

		self.assertEqual(valor_total_ingresso, ingresso_db.valor)

  
	def __cadastrar_cinema_filme_test(self) -> Filme:
		if self.__filme is None:
			data_atual = datetime.now()
			cinema_service = CinemaService()
			cinema = Cinema()
			cinema.nome = f'Shopping -- {data_atual}'
			cinema_service = CinemaService()
			cinema_service.salvar(cinema)

			filme_service = FilmeService()
			filme = Filme()
			filme.nome = 'Besouro Azul'
			filme.cinema_id = cinema.id
			filme.dt_inicio = datetime(2023, 8, 18, 16, 30, 0)
			filme.dt_fim = filme.dt_inicio + relativedelta(months=3)
			filme.valor = 120.95
			filme_service.salvar(filme)
			self.__filme = filme

		return self.__filme