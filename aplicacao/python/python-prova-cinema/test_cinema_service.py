import unittest
from cinema import Cinema
from cinema_service import CinemaService

class TestCinemaService(unittest.TestCase):
    def __init__(self, method_name: str = 'runTest') -> None:
    	super().__init__(method_name)
    	self.__service = CinemaService()

    def test_adicionar(self) -> None:
    	cinema = Cinema()
    	cinema.nome = 'Pátio Brasil'
    	self.__service.salvar(cinema)
    	self.assertNotEqual(0, cinema.id)

    def test_editar(self) -> None:
    	cinema = Cinema()
    	cinema.nome = 'Pátio Brasil'
    	self.__service.salvar(cinema)

    	cinema.nome = 'Park Shopping'
    	self.__service.salvar(cinema)

    	cinema_bd = self.__service.detalhar(cinema.id)

    	self.assertEqual(cinema.id, cinema_bd.id)
    	self.assertEqual(cinema.nome, cinema_bd.nome)

    def test_listagem(self) -> None:
    	cinema = Cinema()
    	cinema.nome = 'Pátio Brasil'
    	self.__service.salvar(cinema)

    	cinema = Cinema()
    	cinema.nome = 'Park Shopping'
    	self.__service.salvar(cinema)

    	cinemas = self.__service.listagem()
    	self.assertTrue(len(cinemas) != 0)


    def test_excluir(self) -> None:
        cinema = Cinema()
        cinema.nome = 'Conjunto Nacional'
        self.__service.salvar(cinema)
        self.__service.excluir(cinema)
        with self.assertRaises(ValueError):
            self.__service.detalhar(cinema.id)


    def test_limpar_todos_registros(self) -> None:
        cinemas = self.__service.listagem()
        for index, cinema in enumerate(cinemas):
            self.__service.excluir(cinema)

        self.assertEqual(0, len(self.__service.listagem()))