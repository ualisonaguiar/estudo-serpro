from cinema_dao import CinemaDAO
from cinema import Cinema

class CinemaService:
	def __init__(self):
		self.__repository = CinemaDAO()

	def salvar(self, cinema: Cinema) -> Cinema:
		if cinema.id == 0:
			cinema = self.__repository.adicionar(cinema)
		else:
			self.__repository.alterar(cinema)
		return cinema

	def detalhar(self, id: int) -> Cinema:
		cinema = self.__repository.detalhar(id)
		if cinema is None:
			raise ValueError(f'Cinema ID: {id} não encontrado.')			
		return cinema

	def listagem(self) -> [Cinema]:
		return self.__repository.listagem()

	def excluir(self, cinema: Cinema) -> bool:
		self.__repository.excluir(cinema.id)

		return True