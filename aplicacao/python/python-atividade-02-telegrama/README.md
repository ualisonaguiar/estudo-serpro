dia 2
Exercício Avançado para a Prova de Conhecimentos Aplicados
Contexto:
Uma empresa financeira deseja desenvolver um sistema de gerenciamento de transações. Esse sistema será responsável por registrar, listar e analisar transações financeiras realizadas por seus clientes em diferentes modalidades e moedas.
Requisitos:

O sistema deve permitir a inserção de uma nova transação financeira com os seguintes dados: cliente, valor, moeda (USD, EUR, BRL, etc.) e tipo de transação (Depósito, Retirada, Transferência).
Deve ser possível filtrar e listar transações por cliente e/ou tipo de transação.
O sistema deve calcular a taxa de câmbio para transações em moedas estrangeiras.
Deve ser possível calcular o saldo total de um cliente em sua moeda local.
Implementar um mecanismo de detecção de transações suspeitas.

Uma transação é considerada suspeita se um cliente realiza três ou mais retiradas num período de 24 horas.

(OK) - Questão 1: Desenvolva uma classe Transacao que represente uma transação financeira, considerando os atributos e métodos necessários para atender aos requisitos acima.

(OK) Questão 2: Desenvolva uma funcionalidade que permita filtrar e listar transações por cliente e/ou tipo de transação, usando estruturas de dados eficientes para filtragem e ordenação.

(OK) Questão 3: Implemente uma classe ConversorMoeda que use uma API externa (ou uma simulação de uma) para pegar as taxas de câmbio atualizadas e realize a conversão entre diferentes moedas.

(OK) Questão 4: No sistema, crie uma funcionalidade que, ao consultar o saldo total de um cliente, faça a conversão de todas as suas transações para sua moeda local, considerando as taxas de câmbio do dia.

Questão 5: Implemente a detecção de transações suspeitas. Crie testes unitários para validar essa funcionalidade.