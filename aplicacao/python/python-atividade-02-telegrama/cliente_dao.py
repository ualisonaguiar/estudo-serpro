from cliente import Cliente
from conexao_db import Conexao


class ClienteDAO:
    def __init__(self) -> None:
        conexao = Conexao()
        self._conexao = conexao.abre_conexao()

    def listagem(self) -> [Cliente]:
        cursor = self._conexao.cursor()
        cursor.execute("select * from cliente order by nome asc")

        clientes_data = cursor.fetchall()
        cursor.close()

        clientes = []
        for cliente_data in clientes_data:
            clientes.append(self.populate_domain(cliente_data))

        return clientes


    def find_by_id(self, id: int) -> Cliente:
        cursor = self._conexao.cursor()
        cursor.execute("select * from cliente where id = %s", (id,))

        result = cursor.fetchone()
        cursor.close()

        return self.populate_domain(result)

    def populate_domain(self, result) -> Cliente:
        if result:
            cliente = Cliente()
            id, nome = result
            cliente.id = id
            cliente.nome = nome
            return cliente

        return None


    def inserir(self, cliente: Cliente) -> Cliente:
        cursor = self._conexao.cursor()

        cursor.execute("insert into cliente (nome) values (%s)", (cliente.nome,))
        self._conexao.commit()

        cliente.id = cursor.lastrowid
        cursor.close()

        return cliente

    def alterar(self, cliente: Cliente) -> Cliente:
        cursor = self._conexao.cursor()

        cursor.execute("update cliente set nome = %s where id = %s", (cliente.nome, cliente.id))
        self._conexao.commit()

        return cliente

    def excluir(self, id: int) -> None:
        cursor = self._conexao.cursor()
        print("Cliente excluido com o ID:", id)

        cursor.execute("delete from cliente where id = %s", (id,))
        self._conexao.commit()