import decimal
import unittest
from transacao import Transacao
from transacao_service import TransacaoService
from cliente import Cliente
from tipo_moeda import TipoMoedaEnum
from tipo_transacao import TipoTransacaoEnum


class TestTransacaoService(unittest.TestCase):
    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self._service = TransacaoService()

    def test_registrar(self):
        cliente = Cliente()
        cliente.id = 140
        cliente.nome = "Ualison Aguiar"

        transacao = Transacao()
        transacao.cliente = cliente
        transacao.moeda = TipoMoedaEnum.BRL
        transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
        transacao.valor = decimal.Decimal('120.10')
        transacao = self._service.registrar(transacao)

        self.assertIsNotNone(transacao.id)
        self.assertTrue(len(self._service.listagem()) > 0)

    def test_listagem_transacao_cliente(self):
        cliente = Cliente()
        cliente.id = 2
        cliente.nome = "Ualison Aguiar"

        transacao = Transacao()
        transacao.cliente = cliente
        transacao.moeda = TipoMoedaEnum.BRL
        transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
        transacao.valor = decimal.Decimal('120.10')
        transacao = self._service.registrar(transacao)

        transacao = Transacao()
        transacao.cliente = cliente
        transacao.moeda = TipoMoedaEnum.EUR
        transacao.tipo_transacao = TipoTransacaoEnum.RETIRADA
        transacao.valor = decimal.Decimal('12.65')
        transacao = self._service.registrar(transacao)

        transacao = Transacao()
        transacao.cliente = cliente
        transacao.moeda = TipoMoedaEnum.EUR
        transacao.tipo_transacao = TipoTransacaoEnum.RETIRADA
        transacao.valor = decimal.Decimal('1850.65')
        transacao = self._service.registrar(transacao)

        cliente2 = Cliente()
        cliente2.id = 3
        cliente2.nome = "João Paulo"

        transacao = Transacao()
        transacao.cliente = cliente2
        transacao.moeda = TipoMoedaEnum.BRL
        transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
        transacao.valor = decimal.Decimal('120.10')
        transacao = self._service.registrar(transacao)

        transacoes_cliente = self._service.find_transacao_cliente(cliente.id)
        self.assertEqual(3, len(transacoes_cliente))
        self.assertEqual(decimal.Decimal('1850.65'), transacoes_cliente[0].valor)

        transacoes_cliente = self._service.find_transacao_cliente(
            cliente.id, TipoTransacaoEnum.DEPOSITO)
        self.assertEqual(1, len(transacoes_cliente))

        transacoes_cliente = self._service.find_transacao_cliente(
            cliente.id, TipoTransacaoEnum.RETIRADA)
        self.assertEqual(2, len(transacoes_cliente))

    def test_listagem_transacao_cliente_valor_convertido(self):
        cliente = Cliente()
        cliente.id = 2
        cliente.nome = "Ualison Aguiar"

        transacao = Transacao()
        transacao.cliente = cliente
        transacao.moeda = TipoMoedaEnum.BRL
        transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
        transacao.valor = decimal.Decimal('120.10')
        self._service.registrar(transacao)
        
        transacao = Transacao()
        transacao.cliente = cliente
        transacao.moeda = TipoMoedaEnum.USD
        transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
        transacao.valor = decimal.Decimal('120.10')
        self._service.registrar(transacao)        

        transacoes_cliente_convertido = self._service.find_transacao_cliente_valor_convertido(
            cliente.id, TipoMoedaEnum.USD)
        
        self.assertEqual(cliente.id, transacoes_cliente_convertido[0].cliente.id)
        self.assertEqual(decimal.Decimal('744.62'), transacoes_cliente_convertido[0].valor_convertido)
        
    # def test_saldo_cliente_valor_convertido(self):
    #     cliente = Cliente()
    #     cliente.id = 2
    #     cliente.nome = "Ualison Aguiar"

    #     transacao = Transacao()
    #     transacao.cliente = cliente
    #     transacao.moeda = TipoMoedaEnum.BRL
    #     transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
    #     transacao.valor = 120.10
    #     transacao = self._service.registrar(transacao)

    #     transacao = Transacao()
    #     transacao.cliente = cliente
    #     transacao.moeda = TipoMoedaEnum.EUR
    #     transacao.tipo_transacao = TipoTransacaoEnum.RETIRADA
    #     transacao.valor = 12.65
    #     transacao = self._service.registrar(transacao)

    #     transacao = Transacao()
    #     transacao.cliente = cliente
    #     transacao.moeda = TipoMoedaEnum.EUR
    #     transacao.tipo_transacao = TipoTransacaoEnum.RETIRADA
    #     transacao.valor = 1850.65
    #     transacao = self._service.registrar(transacao)
        
    #     self.assertEqual(8384.86, self._service.saldo_cliente_valor_convertido(cliente.id, TipoMoedaEnum.BRL))
