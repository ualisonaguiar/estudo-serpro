from enum import Enum

class TipoMoedaEnum(Enum):
    USD = 1
    EUR = 2
    BRL = 3