import decimal
from cliente import Cliente
from tipo_moeda import TipoMoedaEnum
from tipo_transacao import TipoTransacaoEnum

class Transacao:
    def __init__(self) -> None:
        self._id: int = 0
        self._cliente: Cliente = Cliente()
        self._valor: decimal.Decimal = decimal.Decimal("0.00")
        self._valor_convertido: decimal.Decimal = decimal.Decimal("0.00")
        self._moeda: TipoMoedaEnum = TipoMoedaEnum.BRL
        self._tipo_transacao: TipoTransacaoEnum = TipoTransacaoEnum.DEPOSITO

    @property
    def id(self) -> int:
        return self._id

    @id.setter
    def id(self, id: int) -> int:
        self._id = id

    @property
    def cliente(self) -> Cliente:
        return self._cliente

    @cliente.setter
    def cliente(self, cliente: Cliente) -> None:
        self._cliente = cliente

    @property
    def valor(self) -> float:
        return self._valor

    @valor.setter
    def valor(self, valor: float) -> None:
        self._valor = round(decimal.Decimal(valor), 2)

    @property
    def valor_convertido(self) -> decimal.Decimal:
        return self._valor_convertido

    @valor_convertido.setter
    def valor_convertido(self, valor_convertido: decimal.Decimal) -> None:
        self._valor_convertido = round(decimal.Decimal(valor_convertido), 2)

    @property
    def moeda(self) -> TipoMoedaEnum:
        return self._moeda

    @moeda.setter
    def moeda(self, moeda: TipoMoedaEnum) -> None:
        self._moeda = moeda

    @property
    def tipo_transacao(self) -> TipoTransacaoEnum:
        return self._tipo_transacao

    @tipo_transacao.setter
    def tipo_transacao(self, tipo_transacao: TipoTransacaoEnum) -> None:
        self._tipo_transacao = tipo_transacao
