import unittest

import pytest

from cliente import Cliente
from cliente_service import ClienteService


class TestClienteService(unittest.TestCase):

    def __init__(self, method_name: str = "runTest") -> None:
        super().__init__(method_name)
        self.__service = ClienteService()

    @pytest.mark.run(order=1)
    def test_inserir_cliente(self) -> None:
        cliente = Cliente()
        cliente.nome = "Ualison Aguiar"

        self.__service.salvar(cliente)

        self.assertIsNotNone(cliente.id)

    @pytest.mark.run(order=2)
    def test_alterar_cliente(self) -> None:
        cliente = Cliente()
        cliente.nome = "Vanda Silva"
        self.__service.salvar(cliente)

        cliente.nome = "1º Ten Ualison"
        self.__service.salvar(cliente)
        self.assertEqual(cliente.nome, self.__service.busca_id(cliente.id).nome)

    @pytest.mark.run(order=3)
    def test_excluir_cliente(self) -> None:
        cliente = Cliente()
        cliente.nome = "Liz Rodrigues"
        self.__service.salvar(cliente)
        self.assertTrue(self.__service.excluir(cliente))
        self.assertIsNone(self.__service.busca_id(cliente.id))

    @pytest.mark.run(order=4)
    def test_listagem(self) -> None:
        cliente = Cliente()
        cliente.nome = "João Paulo"
        self.__service.salvar(cliente)

        clientes = self.__service.listagem()
        self.assertTrue(len(clientes) > 1)

        cliente_localizado = False
        for cliente_result in clientes:
            if cliente_result.id == cliente.id:
                cliente_localizado = True
                break

        self.assertTrue(cliente_localizado)

    @pytest.mark.run(order=5)
    def test_excluir_todos(self) -> None:
        cliente = Cliente()
        cliente.nome = "Uanderson"
        self.__service.salvar(cliente)

        cliente = Cliente()
        cliente.nome = "Ualison"
        self.__service.salvar(cliente)

        cliente = Cliente()
        cliente.nome = "Bruna"
        self.__service.salvar(cliente)

        for cliente in self.__service.listagem():
            self.__service.excluir(cliente)

        self.assertTrue(len(self.__service.listagem()) == 0)