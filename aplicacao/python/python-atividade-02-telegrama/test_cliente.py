import unittest
from cliente import Cliente

class TestCliente(unittest.TestCase):
    def test_populate(self):
        cliente = Cliente()
        cliente.id = 2
        cliente.nome = "Ualison Aguiar"
        
        self.assertEqual(2, cliente.id)
        self.assertEqual("Ualison Aguiar", cliente.nome)