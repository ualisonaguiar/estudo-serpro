import decimal
from tipo_moeda import TipoMoedaEnum


class ConversorMoeda:
    def __init__(self) -> None:
        self._valores_moedas = {}
        self.carregar_valor()

    def carregar_valor(self) -> None:
        self._valores_moedas[TipoMoedaEnum.BRL] = decimal.Decimal("4.50")
        self._valores_moedas[TipoMoedaEnum.EUR] = decimal.Decimal("5.50")
        self._valores_moedas[TipoMoedaEnum.USD] = decimal.Decimal("6.20")

    def converter_valor(self, tipo_moeda: TipoMoedaEnum, valor: decimal.Decimal) -> decimal.Decimal:
        return decimal.Decimal(round(valor * self._valores_moedas.get(tipo_moeda), 2))
