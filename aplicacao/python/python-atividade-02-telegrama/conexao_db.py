import mysql.connector


class Conexao:
    def __init__(self) -> None:
        self._conexao = None


    def abre_conexao(self) -> None:
        if self._conexao is None:
            try:
                parametros = self.__parametros()
                self._conexao = mysql.connector.connect(
                    host=parametros['host'],
                    user=parametros['user'],
                    password=parametros['password'],
                    database=parametros['database'],
                )
            except mysql.connector.Error as e:
                print(f'Erro ao conectar ao banco de dados: {e}')

        return self._conexao


    def fechar_conexao(self) -> None:
        if self._conexao.is_connected():
            self._conexao.close()

    def __parametros(self) -> {}:
        return {
            'user': 'root',
            'password': 'DCT@_bd&244',
            'host': '10.67.40.244',
            'database': 'ten_ualison'
        }