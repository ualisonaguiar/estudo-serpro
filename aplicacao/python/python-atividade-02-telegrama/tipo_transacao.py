from enum import Enum

class TipoTransacaoEnum(Enum):
    DEPOSITO = 1
    RETIRADA = 2
    TRANSFERENCIA = 3