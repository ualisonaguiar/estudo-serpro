from transacao import Transacao
from tipo_transacao import TipoTransacaoEnum
from tipo_moeda import TipoMoedaEnum
from conversor_moeda import ConversorMoeda
from transacao_dao import TransacaoDAO

class TransacaoService:
    def __init__(self) -> None:
        self.id = 0
        self._transacoes = []
        self._repository = TransacaoDAO()
        
    def listagem(self) -> [Transacao]:
        return self._repository.listagem()        

    def registrar(self, transacao: Transacao) -> Transacao:
        if transacao.id == 0:
            self._repository.inserir(transacao)
        else:
            self._repository.alterar(transacao)

        return transacao

    def find_transacao_cliente(self, id_cliente: int, tipo_transacao: TipoTransacaoEnum = None) -> [Transacao]:
        
        if tipo_transacao is not None:
            transacoes_cliente = self._repository.find_transacao_cliente_tipo_transacao(id_cliente, tipo_transacao)
        else:
            transacoes_cliente = self._repository.find_transacao_cliente(id_cliente)
        
        return sorted(transacoes_cliente, key=lambda transacao: transacao.valor, reverse=True);
    
    def find_transacao_cliente_valor_convertido(self, id_cliente: int, moeda_local: TipoMoedaEnum) -> [Transacao]:
        transacoes_clinte = self.find_transacao_cliente(id_cliente)
        conversor = ConversorMoeda()
        
        for transacao in transacoes_clinte:
            if transacao.moeda is not moeda_local:
                transacao.valor_convertido = conversor.converter_valor(moeda_local, transacao.valor)
                self.registrar(transacao)
        
        return transacoes_clinte;
    
    def saldo_cliente_valor_convertido(self, id_cliente: int, moeda_local: TipoMoedaEnum) -> float:
        transacoes_cliente = self.find_transacao_cliente_valor_convertido(id_cliente, moeda_local)
        
        return round(sum(map(lambda transacao: transacao.valor_convertido, transacoes_cliente)), 2)
