from conexao_db import Conexao
from transacao import Transacao
from cliente_service import ClienteService
from tipo_transacao import TipoTransacaoEnum
from tipo_moeda import TipoMoedaEnum

class TransacaoDAO:
    def __init__(self) -> None:
        conexao = Conexao()
        self.__conexao = conexao.abre_conexao()
        self.__moeda_map = {m.value: m for m in TipoMoedaEnum}
        self.__tipo_transacao_map = {m.value: m for m in TipoTransacaoEnum}
        
    def listagem(self) -> [Transacao]:
        cursor = self.__conexao.cursor()
        cursor.execute("select * from transacao")
        transacoes_data = cursor.fetchall()
        
        transacoes = []
        for transacao_data in transacoes_data:
            transacoes.append(self.populate_domain(transacao_data))

        return transacoes
    
    def inserir(self, transacao: Transacao)-> Transacao:
        cursor = self.__conexao.cursor()
        values = (transacao.cliente.id, transacao.valor, transacao.valor_convertido, transacao.moeda.value, transacao.tipo_transacao.value)
        cursor.execute("insert into transacao (cliente_id,valor,valor_convertido,moeda,tipo_transacao) values (%s,%s,%s,%s,%s)", values)
        self.__conexao.commit()
        transacao.id = cursor.lastrowid
        cursor.close()
        
        return transacao
    
    def find_transacao_cliente(self, id_cliente: int) -> [Transacao]:
        cursor = self.__conexao.cursor()
        cursor.execute("select * from transacao where cliente_id = %s", (id_cliente, ))
        transacoes_data = cursor.fetchall()
        cursor.close()
        
        transacoes_cliente = []
        for transacao_data in transacoes_data:
            transacoes_cliente.append(self.populate_domain(transacao_data))
        
        return transacoes_cliente    
    
    def find_transacao_cliente_tipo_transacao(self, id_cliente: int, tipo_transacao: TipoTransacaoEnum = None) -> [Transacao]:
        cursor = self.__conexao.cursor()
        cursor.execute("select * from transacao where cliente_id = %s and tipo_transacao = %s", (id_cliente, tipo_transacao.value))
        transacoes_data = cursor.fetchall()
        cursor.close()
        
        transacoes_cliente = []
        for transacao_data in transacoes_data:
            transacoes_cliente.append(self.populate_domain(transacao_data))
        
        return transacoes_cliente
    
    def alterar(self, transacao: Transacao)-> Transacao:
        cursor = self.__conexao.cursor()
        values = (transacao.cliente.id, transacao.valor, transacao.valor_convertido, transacao.moeda.value, transacao.tipo_transacao.value, transacao.id)
        cursor.execute("update transacao set cliente_id=%s, valor=%s, valor_convertido=%s, moeda=%s, tipo_transacao=%s where id=%s", values)
        self.__conexao.commit()
        cursor.close()
        
        return transacao
    
    def populate_domain(self, result) -> Transacao:
        id, cliente_id, valor, valor_esperado, moeda, tipo_transacao = result
        transacao = Transacao()
        transacao.id = id
        transacao.valor = valor
        transacao.valor_esperado = valor_esperado
        transacao.moeda = self.__moeda_map.get(moeda)
        transacao.tipo_transacao = self.__tipo_transacao_map.get(tipo_transacao)
        
        cliente_service = ClienteService()
        transacao.cliente = cliente_service.busca_id(cliente_id)
        
        return transacao