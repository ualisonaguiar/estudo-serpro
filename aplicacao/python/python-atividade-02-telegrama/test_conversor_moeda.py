import unittest
from conversor_moeda import ConversorMoeda
from tipo_moeda import TipoMoedaEnum


class TestConversorMoeda(unittest.TestCase):
    def __init__(self, method_name: str = "runTest") -> None:
        super().__init__(method_name)
        self.conversor = ConversorMoeda()

    def test_valor_convertido(self):
        self.assertEqual(654.3, self.conversor.converter_valor(
            TipoMoedaEnum.BRL, 145.40))
        self.assertEqual(799.7, self.conversor.converter_valor(
            TipoMoedaEnum.EUR, 145.40))
        self.assertEqual(901.48, self.conversor.converter_valor(
            TipoMoedaEnum.USD, 145.40))
