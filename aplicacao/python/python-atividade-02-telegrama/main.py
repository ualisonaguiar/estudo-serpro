from cliente import Cliente
from transacao import Transacao
from transacao_service import TransacaoService
from cliente_service import ClienteService
from tipo_moeda import TipoMoedaEnum
from tipo_transacao import TipoTransacaoEnum

if __name__ == "__main__":
    transacao_service = TransacaoService()
    cliente_service = ClienteService()

    cliente = Cliente()
    cliente.id = 132
    cliente.nome = "Liz Rodrigues"
    cliente_service.salvar(cliente)
    
    transacao = Transacao()
    transacao.id = 49
    transacao.cliente = cliente
    transacao.moeda = TipoMoedaEnum.BRL
    transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
    transacao.valor = 10.40
    
    # transacao_service.registrar(transacao)
    # print(len(transacao_service.listagem()))
    
    # transacao.valor = 100.88
    # transacao_service.registrar(transacao)
    
    # print(len(transacao_service.find_transacao_cliente(cliente.id)))
    # print(len(transacao_service.find_transacao_cliente(cliente.id, TipoTransacaoEnum.RETIRADA)))
    
    transacao_service.find_transacao_cliente_valor_convertido(cliente.id, TipoMoedaEnum.USD)