from cliente import Cliente
from cliente_dao import ClienteDAO

class ClienteService():

    def __init__(self) -> None:
        self._respository = ClienteDAO()

    def listagem(self) -> [Cliente]:
        return self._respository.listagem()

    def busca_id(self, id: int) -> Cliente:
        return self._respository.find_by_id(id)

    def salvar(self, cliente: Cliente) -> Cliente:
        if cliente.id == 0:
            self._respository.inserir(cliente)
        else:
            self._respository.alterar(cliente)

        return cliente

    def excluir(self, cliente: Cliente) -> bool:
        self._respository.excluir(cliente.id)

        return True
