import unittest
from cliente import Cliente
from transacao import Transacao
from tipo_moeda import TipoMoedaEnum
from tipo_transacao import TipoTransacaoEnum

class TestTransacao(unittest.TestCase):
    def test_populate(self):
        cliente = Cliente()
        cliente.id = 2
        cliente.nome = "Ualison Aguiar"
        
        transacao = Transacao()
        transacao.cliente = cliente
        transacao.id = 10
        transacao.moeda = TipoMoedaEnum.BRL
        transacao.tipo_transacao = TipoTransacaoEnum.DEPOSITO
        transacao.valor = 120.10
        
        self.assertEqual(cliente.id, transacao.cliente.id)
        self.assertEqual(10, transacao.id)
        self.assertEqual(TipoMoedaEnum.BRL, transacao.moeda)
        self.assertEqual(TipoTransacaoEnum.DEPOSITO, transacao.tipo_transacao)
        self.assertEqual(120.10, transacao.valor)
