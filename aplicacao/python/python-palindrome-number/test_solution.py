import unittest
from solution import Solution

class TestSolution(unittest.TestCase):
	def __init__(self, method_name = 'runtest'):
		super().__init__(method_name)
		self.__service = Solution()

	def test_is_palindrome_01(self):
		self.assertTrue(self.__service.isPalindrome(121))

	def test_is_palindrome_02(self):
		self.assertFalse(self.__service.isPalindrome(-121))

	def test_is_palindrome_03(self):
		self.assertFalse(self.__service.isPalindrome(10))