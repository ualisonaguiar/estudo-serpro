class Solution:
	def add_two_numbers(self, l1: [], l2: []) -> []:
		numero_reverso_l1: int = self.__numero_reverso(l1)
		numero_reverso_l2: int = self.__numero_reverso(l2)
		soma_numero_revero: int = numero_reverso_l1 + numero_reverso_l2

		return self.__converter_numero_em_lista(soma_numero_revero)

	def __converter_numero_em_lista(self, numero: int) -> []:
		lista_retorno: [int] = []
		numero_string = str(numero)
		for num in range(len(numero_string) -1, -1, -1):			
			lista_retorno.append(int(numero_string[num]))

		return lista_retorno


	def __numero_reverso(self, lista) -> int:	
		numero_reverso: str = ''
		for numero in lista[::-1]:
			numero_reverso += str(numero)

		return int(numero_reverso)