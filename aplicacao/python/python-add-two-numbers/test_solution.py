import unittest
from solution import Solution

class TestSolution(unittest.TestCase):
	def __init__(self, method_name = 'runTest') -> None:
		super().__init__(method_name)
		self.__service = Solution()

	def test_add_two_numbers_exemplo_01(self):
		self.assertEqual([7, 0, 8], self.__service.add_two_numbers([2,4,3], [5,6,4]))

	def test_add_two_numbers_exemplo_02(self):
		self.assertEqual([0], self.__service.add_two_numbers([0], [0]))

	def test_add_two_numbers_exemplo_03(self):
		self.assertEqual([8,9,9,9,0,0,0,1], self.__service.add_two_numbers([9,9,9,9,9,9,9], [9,9,9,9]))		