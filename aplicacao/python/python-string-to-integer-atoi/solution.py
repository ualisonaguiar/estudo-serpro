class Solution:
    def myAtoi(self, s: str) -> int:
    	s = s.strip()
    	if len(s) == 0:
    		return 0

    	resultado_negativo = False
    	numero_str: str = ''
    	numero: int = 0

    	for idx, caracter in enumerate(s):
    		if caracter == '-' and idx == 0:
    			resultado_negativo = True
    		elif caracter == '+' and idx == 0:
    			resultado_negativo = False
    		elif not caracter.isdigit() and len(numero_str) == 0:
    			return 0
    		elif caracter.isdigit():
    			numero_str += caracter
    		else:
    			break

    	if len(numero_str) == 0:
    		return 0   			


    	numero = int(numero_str)
    	if resultado_negativo:
    		numero *= -1

    	return max(min(numero, 2**31 - 1), -2**31)