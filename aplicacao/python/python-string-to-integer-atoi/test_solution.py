import unittest
from solution import Solution

class TestSolution(unittest.TestCase):
	def __init__(self, method_name = 'runtest'):
		super().__init__(method_name)
		self.__service = Solution()

	def test_my_atoi_exemplo01(self):
		self.assertEqual(-42, self.__service.myAtoi("   -42"))

	def test_my_atoi_exemplo02(self):
		self.assertEqual(42, self.__service.myAtoi("42"))

	def test_my_atoi_exemplo03(self):
		self.assertEqual(4193, self.__service.myAtoi("4193 with words"))

	def test_my_atoi_exemplo04(self):
		self.assertEqual(0, self.__service.myAtoi("words and 987"))

	def test_my_atoi_exemplo05(self):
		self.assertEqual(4193, self.__service.myAtoi("4193 with words"))

	def test_my_atoi_exemplo06(self):
		self.assertEqual(-2147483648, self.__service.myAtoi("-91283472332"))

	def test_my_atoi_exemplo07(self):
		self.assertEqual(3, self.__service.myAtoi("3.14159"))	

	def test_my_atoi_exemplo08(self):
		self.assertEqual(0, self.__service.myAtoi("-"))

	def test_my_atoi_exemplo09(self):
		self.assertEqual(0, self.__service.myAtoi(""))

	def test_my_atoi_exemplo10(self):
		self.assertEqual(1, self.__service.myAtoi("+1"))

	def test_my_atoi_exemplo11(self):
		self.assertEqual(0, self.__service.myAtoi("+-12"))		
