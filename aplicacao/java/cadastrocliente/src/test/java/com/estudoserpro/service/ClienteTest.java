package com.estudoserpro.service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.estudoserpro.model.Cliente;

public class ClienteTest {

	private ClienteService service;

	@BeforeEach
	public void setUp() {
		service = new ClienteService();
	}

	@Test
	public void salvarClienteComIdadeValida()
			throws ClienteIdadeNaoPermitidadeException, ClienteEmailInvalidoException, SQLException {
		Cliente cliente = new Cliente().setNome("João da Silva").setIdade(25).setEmail("joao.silva@gmail.com");

		service.salvar(cliente);

		assertNotNull(cliente.getId());
	}

	@Test
	public void salvarClienteComIdadeInvalida() throws ClienteEmailInvalidoException, SQLException {
		Cliente cliente = new Cliente().setNome("Maria Souza").setIdade(16).setEmail("maria.souza@hotmail.com");

		try {
			service.salvar(cliente);
			fail("Aqui deveria gerar uma Exception");
		} catch (ClienteIdadeNaoPermitidadeException e) {
			String msgError = "O Cliente: "
					.concat(cliente.getNome().concat(" é menor de idade. Sua idade é: " + cliente.getIdade()));
			assertEquals(msgError, e.getMessage());
		}
	}

	@Test
	public void salvarClienteComEmailInvalido() throws ClienteIdadeNaoPermitidadeException, SQLException {
		Cliente cliente = new Cliente().setNome("Carlos Pereira").setIdade(30).setEmail("carlos.pereira");

		try {
			service.salvar(cliente);
			fail("Aqui deveria gerar uma Exception");
		} catch (ClienteEmailInvalidoException e) {
			String msgError = "O Cliente: ".concat(
					cliente.getNome().concat(" informou e-mail inválido. E-mail informado: " + cliente.getIdade()));
			assertEquals(msgError, e.getMessage());
		}
	}

	@Test
	public void salvarClienteEAlteraDados()
			throws ClienteIdadeNaoPermitidadeException, ClienteEmailInvalidoException, SQLException {
		Cliente cliente = new Cliente().setNome("João da Silva").setIdade(25).setEmail("joao.silva@gmail.com");
		service.salvar(cliente);

		cliente.setNome("Ualison Aguiar").setIdade(32).setEmail("ualison.aguiar@gmail.com");
		service.salvar(cliente);

		Cliente cliente2 = service.getId(cliente.getId());

		assertEquals(cliente.getNome(), cliente2.getNome());
		assertEquals(cliente.getIdade(), cliente2.getIdade());
		assertEquals(cliente.getEmail(), cliente2.getEmail());
	}

	@Test
	public void listagem() throws ClienteIdadeNaoPermitidadeException, ClienteEmailInvalidoException, SQLException {
		Cliente cliente = new Cliente().setNome("João da Silva").setIdade(25).setEmail("joao.silva@gmail.com");
		service.salvar(cliente);

		List<Cliente> clientes = service.listagem();
		Boolean clienteEncontrado = clientes.stream().anyMatch(cliente2 -> cliente2.getId().equals(cliente.getId()));
		assertTrue(clienteEncontrado);
	}

	@Test
	public void delete() throws ClienteIdadeNaoPermitidadeException, ClienteEmailInvalidoException, SQLException {
		Cliente cliente = new Cliente().setNome("João da Silva").setIdade(25).setEmail("joao.silva@gmail.com");
		service.salvar(cliente);

		service.delete(cliente.getId());

		List<Cliente> clientes = service.listagem();
		Boolean clienteEncontrado = clientes.stream().anyMatch(cliente2 -> cliente2.getId().equals(cliente.getId()));
		assertFalse(clienteEncontrado);
	}
}
