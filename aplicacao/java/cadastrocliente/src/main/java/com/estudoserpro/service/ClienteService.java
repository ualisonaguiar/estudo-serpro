package com.estudoserpro.service;

import java.sql.SQLException;
import java.util.List;

import com.estudoserpro.model.Cliente;
import com.estudoserpro.model.ClienteDAO;

public class ClienteService {

	public static final Integer IDADE_MAX_PERMITIDA = 18;

	private ClienteDAO clienteDao;

	public ClienteService() {
		clienteDao = new ClienteDAO();
	}

	public Cliente salvar(final Cliente cliente)
			throws ClienteIdadeNaoPermitidadeException, ClienteEmailInvalidoException, SQLException {
		validarDadosCliente(cliente);
		clienteDao.salvar(cliente);

		return cliente;
	}

	public Cliente getId(Integer id) throws SQLException {
		return clienteDao.getId(id);
	}

	public List<Cliente> listagem() throws SQLException {
		return clienteDao.listagem();
	}

	public void delete(Integer id) throws SQLException {
		clienteDao.delete(id);
	}

	private void validarDadosCliente(final Cliente cliente)
			throws ClienteIdadeNaoPermitidadeException, ClienteEmailInvalidoException {
		if (cliente.getIdade() < IDADE_MAX_PERMITIDA) {
			throw new ClienteIdadeNaoPermitidadeException("O Cliente: "
					.concat(cliente.getNome().concat(" é menor de idade. Sua idade é: " + cliente.getIdade())));
		}

		if (!cliente.getEmail().contains("@")) {
			throw new ClienteEmailInvalidoException("O Cliente: ".concat(
					cliente.getNome().concat(" informou e-mail inválido. E-mail informado: " + cliente.getIdade())));
		}
	}

}
