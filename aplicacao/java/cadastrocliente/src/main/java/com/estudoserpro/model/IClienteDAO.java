package com.estudoserpro.model;

import java.sql.SQLException;
import java.util.List;

public interface IClienteDAO {

	public Cliente salvar(Cliente cliente) throws SQLException;

	public void inserir(Cliente cliente) throws SQLException;

	public void atualizar(Cliente cliente) throws SQLException;
	
	public Cliente getId(Integer id) throws SQLException;
	
	public List<Cliente> listagem() throws SQLException;
	
	public void delete(Integer id) throws SQLException;

}
