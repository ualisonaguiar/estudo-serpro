package com.estudoserpro.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ClienteDAO extends AbstractConexao implements IClienteDAO {

	private String table = "cliente";
	private static final Logger LOGGER = Logger.getLogger(ClienteDAO.class.getName());

	@Override
	public Cliente salvar(Cliente cliente) throws SQLException {

		if (cliente.getId() == null) {
			inserir(cliente);
		} else {
			atualizar(cliente);
		}

		return cliente;
	}

	@Override
	public void inserir(Cliente cliente) throws SQLException {
		Connection conexao = getConection();
		String sql = "insert into ".concat(table).concat(" (nome, idade, email) values (?, ?, ?)");
		PreparedStatement statement = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

		try {
			statement.setString(1, cliente.getNome());
			statement.setInt(2, cliente.getIdade());
			statement.setString(3, cliente.getEmail());
			statement.execute();

			try (ResultSet resultSet = statement.getGeneratedKeys()) {
				if (resultSet.next()) {
					cliente.setId(resultSet.getInt(1));
				}
			}

		} catch (SQLException e) {
			LOGGER.info("Erro ao inserir o dado do cliente");
		} finally {
			statement.close();
			closeConnection();
		}
	}

	@Override
	public void atualizar(Cliente cliente) throws SQLException {
		Connection conexao = getConection();
		String sql = "update ".concat(table).concat(" set nome = ?, idade = ?, email = ? where id = ?");
		PreparedStatement statament = conexao.prepareStatement(sql);

		try {
			statament.setString(1, cliente.getNome());
			statament.setInt(2, cliente.getIdade());
			statament.setString(3, cliente.getEmail());
			statament.setInt(4, cliente.getId());
			statament.execute();

		} catch (SQLException e) {
			LOGGER.info("Erro ao atualizar o dado do cliente");
		} finally {
			statament.close();
			closeConnection();
		}
	}

	@Override
	public Cliente getId(Integer id) throws SQLException {
		Cliente cliente = new Cliente();
		Connection conexao = getConection();
		String sql = "select * from ".concat(table).concat(" where id = ?");
		PreparedStatement statament = conexao.prepareStatement(sql);

		try {
			statament.setInt(1, id);
			ResultSet result = statament.executeQuery();

			while (result.next()) {
				populateCliente(result, cliente);
			}

		} catch (SQLException e) {
			LOGGER.info("Erro ao buscar o dado do cliente");
		} finally {
			statament.close();
			closeConnection();
		}

		return cliente;
	}

	@Override
	public List<Cliente> listagem() throws SQLException {
		List<Cliente> clientes = new ArrayList<>();
		Connection conexao = getConection();
		String sql = "select * from ".concat(table);
		PreparedStatement statament = conexao.prepareStatement(sql);
		
		try {
			ResultSet result = statament.executeQuery();
			while (result.next()) {
				Cliente cliente = new Cliente();
				populateCliente(result, cliente);
				clientes.add(cliente);
			}
		} catch (SQLException e) {
			LOGGER.info("Erro ao buscar os dados do clientes");
		} finally {
			statament.close();
			closeConnection();
		}

		return clientes;
	}

	@Override
	public void delete(Integer id) throws SQLException {
		Connection conexao = getConection();
		String sql = "delete from ".concat(table).concat(" where id = ?");
		PreparedStatement statement = conexao.prepareStatement(sql);	
		
		try {		
			statement.setInt(1, id);
			statement.execute();
		} catch (SQLException e) {
			LOGGER.info("Erro ao deletar o registro");
		} finally {
			statement.close();
			closeConnection();
		}
	}

	private void populateCliente(ResultSet result, Cliente cliente) throws SQLException {
		cliente.setEmail(result.getString("email")).setId(result.getInt("id")).setNome(result.getString("nome"))
				.setIdade(result.getInt("idade"));
	}

}
