package com.estudoserpro.model;

public class Cliente {

	private Integer id;
	private String nome;
	private Integer idade;
	private String email;

	public Cliente setId(Integer id) {
		this.id = id;
		return this;
	}

	public Integer getId() {
		return id;
	}

	public Cliente setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public Cliente setIdade(Integer idade) {
		this.idade = idade;
		return this;
	}

	public Integer getIdade() {
		return idade;
	}

	public Cliente setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getEmail() {
		return email;
	}
}
