package com.estudoserpro.model;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

abstract class AbstractConexao {

	private Connection conexao;
	private static final Logger LOGGER = Logger.getLogger(AbstractConexao.class.getName());

	protected Connection getConection() throws SQLException {

		if (conexao == null) {
			Properties properties = getProperties();

			conexao = DriverManager.getConnection(properties.getProperty("db.url"),
					properties.getProperty("db.username"), properties.getProperty("db.password"));
		}

		return conexao;
	}

	protected void closeConnection() throws SQLException {
		conexao.close();
		conexao = null;
	}

	private Properties getProperties() {
		Properties prop = new Properties();

		try (InputStream input = AbstractConexao.class.getClassLoader().getResourceAsStream("database.properties")) {
			prop.load(input);
		} catch (IOException ex) {
			LOGGER.info("Não foi possível carregar o arquivo das configurações de banco de dados");
		}

		return prop;
	}

}
