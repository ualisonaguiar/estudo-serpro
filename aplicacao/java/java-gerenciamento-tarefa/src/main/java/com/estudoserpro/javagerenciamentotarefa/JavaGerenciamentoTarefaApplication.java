package com.estudoserpro.javagerenciamentotarefa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaGerenciamentoTarefaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaGerenciamentoTarefaApplication.class, args);
	}

}
