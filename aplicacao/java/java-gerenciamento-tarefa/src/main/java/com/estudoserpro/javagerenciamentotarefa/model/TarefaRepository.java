package com.estudoserpro.javagerenciamentotarefa.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TarefaRepository extends JpaRepository<Tarefa, Long> {
    
    List<Tarefa> findAllOrderById();

    @Query(name = "findAllBySituacao")
    List<Tarefa> findAllBySituacao(@Param("situacaoTarefa") SituacaoTarefaEnum situacaoTarefa);

}
