package com.estudoserpro.javagerenciamentotarefa.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SituacaoTarefaEnum {
    
    PENDENTE(1L),
    CONCLUIDO(2L);

    private Long id;
}
