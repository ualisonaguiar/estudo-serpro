package com.estudoserpro.javagerenciamentotarefa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.estudoserpro.javagerenciamentotarefa.dto.TarefaDTO;
import com.estudoserpro.javagerenciamentotarefa.model.SituacaoTarefaEnum;
import com.estudoserpro.javagerenciamentotarefa.model.Tarefa;
import com.estudoserpro.javagerenciamentotarefa.service.TarefaNaoEncontradaExpetion;
import com.estudoserpro.javagerenciamentotarefa.service.TarefaService;
import com.estudoserpro.javagerenciamentotarefa.service.TarefaServiceImp;

@RestController
@RequestMapping(value = "/api/tarefa")
public class TarefaController {

    private final TarefaService service;

    @Autowired
    public TarefaController(TarefaService tarefaService) {
        this.service = tarefaService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Tarefa> listagem() {
        return service.listagem();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Tarefa> detalhe(@PathVariable Long id) {
        try {
            return ResponseEntity.ok().body(service.detalhe(id));
        } catch (TarefaNaoEncontradaExpetion e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Tarefa> inserir(@RequestBody TarefaDTO tarefa) {
        return salvar(tarefa);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Tarefa> atualizar(@RequestBody TarefaDTO tarefa, @PathVariable Long id) {
        tarefa.setId(id);
        return salvar(tarefa);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Tarefa> excluir(@PathVariable Long id) {
        try {
            return ResponseEntity.ok().body(service.excluir(id));
        } catch (TarefaNaoEncontradaExpetion e) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @PutMapping("/alterar-situacao/{id}")
    public ResponseEntity<Tarefa> alterarSituacao(@PathVariable Long id) {
        try {
            return ResponseEntity.ok().body(service.alterarSituacao(id));
        } catch (TarefaNaoEncontradaExpetion e) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/listagem-situacao/{tipo_situacao}")
    public List<Tarefa> listagemSituacao(@PathVariable SituacaoTarefaEnum situacao) {
        return service.listagemPorSituacao(situacao);
    }

    protected ResponseEntity<Tarefa> salvar(final TarefaDTO tarefaDTO) {
        try {
            return ResponseEntity.ok().body(service.salvar(tarefaDTO.convertToEntity()));
        } catch (TarefaNaoEncontradaExpetion e) {
            return ResponseEntity.internalServerError().build();
        }
    }

}
