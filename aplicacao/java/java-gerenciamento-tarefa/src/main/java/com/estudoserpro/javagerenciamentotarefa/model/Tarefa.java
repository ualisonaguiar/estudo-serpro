package com.estudoserpro.javagerenciamentotarefa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"titulo"})})
@Entity
@NamedQueries({
    @NamedQuery(name = "Tarefa.findAllOrderById", query = "SELECT t FROM Tarefa t ORDER BY t.id"),
    @NamedQuery(name = "findAllBySituacao", query = "SELECT t FROM Tarefa t where t.situacaoTarefa = :situacaoTarefa")
})
public class Tarefa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 50, nullable = false)
    private String titulo;

    @Column(length = 150, nullable = false)
    private String descricao;

    @Column(name = "situacao_tarefa", nullable = false)
    private SituacaoTarefaEnum situacaoTarefa = SituacaoTarefaEnum.PENDENTE;

}
