package com.estudoserpro.javagerenciamentotarefa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estudoserpro.javagerenciamentotarefa.model.SituacaoTarefaEnum;
import com.estudoserpro.javagerenciamentotarefa.model.Tarefa;
import com.estudoserpro.javagerenciamentotarefa.model.TarefaRepository;

@Service
public class TarefaServiceImp implements TarefaService {

    @Autowired
    TarefaRepository repository;

    @Override
    public List<Tarefa> listagem() {
        return repository.findAllOrderById();
    }

    @Override
    public Tarefa detalhe(Long id) throws TarefaNaoEncontradaExpetion {
        Optional<Tarefa> tarefa = repository.findById(id);

        if (!tarefa.isPresent()) {
            throw new TarefaNaoEncontradaExpetion("Tarefa não encontrada");
        }

        return tarefa.get();
    }

    @Override
    public Tarefa salvar(Tarefa tarefa) throws TarefaNaoEncontradaExpetion {
        return repository.save(tarefa);
    }

    @Override
    public Tarefa excluir(Long id) throws TarefaNaoEncontradaExpetion {
        Tarefa tarefa = detalhe(id);
        repository.deleteById(id);

        return tarefa;
    }

    @Override
    public List<Tarefa> listagemPorSituacao(SituacaoTarefaEnum situacao) {
        return repository.findAllBySituacao(situacao);
    }

    @Override
    public Tarefa alterarSituacao(Long id) throws TarefaNaoEncontradaExpetion {
        Tarefa tarefa = detalhe(id);

        if (tarefa.getSituacaoTarefa().equals(SituacaoTarefaEnum.PENDENTE)) {
            tarefa.setSituacaoTarefa(SituacaoTarefaEnum.CONCLUIDO);
        } else {
            tarefa.setSituacaoTarefa(SituacaoTarefaEnum.PENDENTE);
        }
        repository.save(tarefa);

        return tarefa;
    }

}
