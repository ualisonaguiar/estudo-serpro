package com.estudoserpro.javagerenciamentotarefa.dto;

import com.estudoserpro.javagerenciamentotarefa.model.SituacaoTarefaEnum;
import com.estudoserpro.javagerenciamentotarefa.model.Tarefa;

import lombok.Data;

@Data
public class TarefaDTO {

    protected Long id;
    protected String titulo;
    protected String descricao;
    protected SituacaoTarefaEnum situacao;

    public Tarefa convertToEntity() {
        Tarefa tarefa = new Tarefa();
        tarefa.setId(getId());
        tarefa.setTitulo(getTitulo());
        tarefa.setDescricao(getDescricao());
        tarefa.setSituacaoTarefa(getSituacao());

        return tarefa;
    }
}
