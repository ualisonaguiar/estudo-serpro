package com.estudoserpro.javagerenciamentotarefa.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.estudoserpro.javagerenciamentotarefa.model.SituacaoTarefaEnum;
import com.estudoserpro.javagerenciamentotarefa.model.Tarefa;

@Service
public interface TareServicefaInterface {
    
    public List<Tarefa> listagem();

    public Tarefa detalhe(final Long id) throws TarefaNaoEncontradaExpetion;

    public Tarefa salvar(final Tarefa tarefa) throws TarefaNaoEncontradaExpetion;

    public Tarefa excluir(final Long id) throws TarefaNaoEncontradaExpetion;

    public List<Tarefa> listagemPorSituacao(final SituacaoTarefaEnum situacao);

    public Tarefa alterarSituacao(final Long id) throws TarefaNaoEncontradaExpetion;

}
