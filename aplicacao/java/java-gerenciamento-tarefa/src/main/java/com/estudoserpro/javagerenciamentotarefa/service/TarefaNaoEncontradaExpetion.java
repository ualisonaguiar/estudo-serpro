package com.estudoserpro.javagerenciamentotarefa.service;

public class TarefaNaoEncontradaExpetion extends Exception {
    
    public TarefaNaoEncontradaExpetion(String message) {
        super(message);
    }

}
