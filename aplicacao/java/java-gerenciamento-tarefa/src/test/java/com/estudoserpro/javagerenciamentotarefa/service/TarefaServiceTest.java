package com.estudoserpro.javagerenciamentotarefa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.estudoserpro.javagerenciamentotarefa.model.SituacaoTarefaEnum;
import com.estudoserpro.javagerenciamentotarefa.model.Tarefa;
import com.estudoserpro.javagerenciamentotarefa.model.TarefaRepository;

@SpringBootTest
public class TarefaServiceTest {

    @Autowired
    private TarefaServiceImp tarefaService;

    @Autowired
    private TarefaRepository repository;

    @BeforeEach
    public void setUp() {
        repository.deleteAll();
    }

    @Test
    public void listagem() {
        assertTrue(tarefaService.listagem().isEmpty());
    }

    @Test
    public void inserir() throws TarefaNaoEncontradaExpetion {
        Tarefa tarefa = new Tarefa();
        tarefa.setTitulo("Desenvolver a aplicação");
        tarefa.setDescricao("Desenvolver a aplicação usando a linguagem java");
        tarefaService.salvar(tarefa);

        Tarefa tarefa2 = tarefaService.detalhe(tarefa.getId());

        assertEquals(tarefa2.getSituacaoTarefa(), SituacaoTarefaEnum.PENDENTE);
    }

    @Test
    public void alterar() throws TarefaNaoEncontradaExpetion {
        Tarefa tarefa = new Tarefa();
        tarefa.setTitulo("Desenvolver a aplicação");
        tarefa.setDescricao("Desenvolver a aplicação usando a linguagem java");
        tarefaService.salvar(tarefa);

        tarefa.setDescricao("Desenvolver a aplicação usando a linguagem java/php");
        tarefaService.salvar(tarefa);

        Tarefa tarefa2 = tarefaService.detalhe(tarefa.getId());

        assertEquals(tarefa2.getId(), tarefa.getId());
        assertEquals(tarefa2.getDescricao(), tarefa.getDescricao());
    }

    @Test
    public void alterarSituacaoTarefa() throws TarefaNaoEncontradaExpetion {
        Tarefa tarefa = new Tarefa();
        tarefa.setTitulo("Modelar o banco de dados");
        tarefa.setDescricao("Modelar o banco de dados com o mysql");
        tarefaService.salvar(tarefa);

        Tarefa tarefa2 = tarefaService.detalhe(tarefa.getId());
        assertEquals(SituacaoTarefaEnum.PENDENTE, tarefa2.getSituacaoTarefa());

        tarefaService.alterarSituacao(tarefa.getId());
        tarefa2 = tarefaService.detalhe(tarefa.getId());
        assertEquals(SituacaoTarefaEnum.CONCLUIDO, tarefa2.getSituacaoTarefa());

        tarefaService.alterarSituacao(tarefa.getId());
        tarefa2 = tarefaService.detalhe(tarefa.getId());
        assertEquals(SituacaoTarefaEnum.PENDENTE, tarefa2.getSituacaoTarefa());
    }

    @Test
    public void listagemPorSituacao() throws TarefaNaoEncontradaExpetion {
        Tarefa tarefa = new Tarefa();
        tarefa.setTitulo("Modelar o banco de dados");
        tarefa.setDescricao("Modelar o banco de dados com o mysql");
        tarefaService.salvar(tarefa);

        Tarefa tarefa2 = new Tarefa();
        tarefa2.setTitulo("Desenvolver a aplicação");
        tarefa2.setDescricao("Modelar o banco de dados com o mysql");
        tarefaService.salvar(tarefa2);

        Tarefa tarefa3 = new Tarefa();
        tarefa3.setTitulo("Criar o ambiente de teste");
        tarefa3.setDescricao("Modelar o banco de dados com o mysql");
        tarefaService.salvar(tarefa3);

        tarefaService.alterarSituacao(tarefa3.getId());

        assertEquals(2, tarefaService.listagemPorSituacao(SituacaoTarefaEnum.PENDENTE).size());
        assertEquals(1, tarefaService.listagemPorSituacao(SituacaoTarefaEnum.CONCLUIDO).size());
    }

    @Test
    public void excluir() {
        try {
            Tarefa tarefa = new Tarefa();
            tarefa.setTitulo("Modelar o banco de dados");
            tarefa.setDescricao("Modelar o banco de dados com o mysql");
            tarefaService.salvar(tarefa);
            tarefaService.excluir(tarefa.getId());
            
            assertNull(tarefaService.detalhe(tarefa.getId()));
            tarefaService.excluir(tarefa.getId());
            fail("Erro. Aqui não deveria executar o teste.");
        } catch (TarefaNaoEncontradaExpetion e) {
            assertEquals("Tarefa não encontrada", e.getMessage());
        }
    }
}
