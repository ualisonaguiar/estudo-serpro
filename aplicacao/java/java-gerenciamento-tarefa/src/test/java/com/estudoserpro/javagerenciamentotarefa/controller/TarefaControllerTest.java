package com.estudoserpro.javagerenciamentotarefa.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.estudoserpro.javagerenciamentotarefa.model.Tarefa;
import com.estudoserpro.javagerenciamentotarefa.model.TarefaRepository;
import com.estudoserpro.javagerenciamentotarefa.service.TarefaService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TarefaController.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TarefaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TarefaService service;

    @Autowired
    ObjectMapper objectMapper;

    private String urlRest = "/api/tarefa";

    @Test
    @Order(1)
    public void listagem() throws Exception {
        List<Tarefa> tarefas = new ArrayList<>();
        when(service.listagem()).thenReturn(tarefas);

        String response = mockMvc.perform(MockMvcRequestBuilders.get(urlRest))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertEquals("[]", response);
    }

    @Test
    @Order(2)
    public void criarTarefa() throws Exception {
        Tarefa tarefa = new Tarefa();
        tarefa.setDescricao("Desenvolvimento de teste com JUnit");
        tarefa.setTitulo("Teste com JUnit");

        when(service.salvar(any())).thenReturn(tarefa);

        mockMvc.perform(MockMvcRequestBuilders.post(urlRest)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(tarefa)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
