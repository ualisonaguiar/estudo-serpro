package com.estudoserpro.javagerenciamentotarefa.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(TarefaRest.class)
public class TarefaRestTest {
    
    @Autowired
    private MockMvc mockMvc;

    private String urlRest = "/tarefa";

    @Test
    public void listagem() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(urlRest))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }

}
