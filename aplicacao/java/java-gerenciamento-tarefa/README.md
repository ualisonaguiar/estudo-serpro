Descrição:
Desenvolver um programa em Java para criar e gerenciar uma lista de tarefas (To-Do List). O sistema permitirá que o usuário adicione, visualize, marque como concluída, edite e remova tarefas da lista.

Detalhes do Requisito:

1) Interface de Usuário:

O sistema deve possuir uma interface de usuário simples e amigável.
Deve exibir uma lista de tarefas, mostrando seus títulos, descrições e status (concluída ou pendente).
O usuário poderá interagir com a lista através de botões ou menus.

(OK) - 2) Adicionar Tarefa:

O usuário deve poder adicionar uma nova tarefa à lista, informando título e descrição.
A nova tarefa será adicionada à lista com o status inicial "pendente".

(OK) - 3)Visualizar Tarefas:

O sistema permitirá ao usuário visualizar todas as tarefas presentes na lista.
As tarefas deverão ser exibidas em ordem de criação, com as mais recentes no topo da lista.

(OK) - 4) Marcar Tarefa como Concluída:

O usuário poderá marcar uma tarefa como concluída.
A tarefa marcada como concluída não será removida da lista, mas seu status será atualizado.

(OK) - 5) Editar Tarefa:

O sistema possibilitará ao usuário editar o título e a descrição de uma tarefa existente.
As edições realizadas devem ser salvas automaticamente.

(OK) - 6)Remover Tarefa:

O usuário terá a opção de remover uma tarefa da lista.
Após a remoção, a tarefa não deverá mais aparecer na lista.

(OK) - 7)Persistência de Dados:

As tarefas criadas pelo usuário devem ser salvas em algum formato de armazenamento persistente, como um arquivo de texto ou banco de dados.
O programa deverá ser capaz de carregar os dados previamente salvos ao ser executado novamente.

8) Validação:

O sistema deve realizar validações para garantir que o usuário insira informações válidas ao criar ou editar uma tarefa.
Caso haja falha na validação, o sistema deve fornecer mensagens de erro adequadas.


9) Tratamento de Erros:

O programa deverá possuir um tratamento adequado de erros e exceções para evitar interrupções inesperadas durante sua execução.

Esse requisito apresenta uma complexidade média, pois envolve a criação de uma interface de usuário simples, a implementação de funcionalidades básicas de gerenciamento de tarefas e o uso de persistência de dados. Ele aborda conceitos fundamentais de programação em Java, como criação de classes, manipulação de listas, tratamento de eventos de interface e persistência de dados.


- Tarefa
-- id
-- titulo
-- descrição
-- status