package com.estudoserpro.ordenacao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SelectionSortTest {

	private SelectionSort selectionSort;

	@BeforeEach
	public void setUp() {
		selectionSort = new SelectionSort();
	}

	@Test
	public void encontreMenorValor() {

		Integer[] lista1 = { 64, 34, 25, 12, 22, 11, 90 };
		selectionSort.setLista(lista1);
		assertEquals(11, selectionSort.encontreMenorValor());

		Integer[] lista2 = { 3, 1, 7, 5, 2 };
		selectionSort.setLista(lista2);
		assertEquals(1, selectionSort.encontreMenorValor());

		Integer[] lista3 = { 64, 34, 25, 12, 22, 11, 90, 3, 1, 7, 5, 2, 64, 34, 25, 12, 22, 11, 90, 3, 1, 7, 5, 2 };
		selectionSort.setLista(lista3);
		assertEquals(1, selectionSort.encontreMenorValor());
	}

}
