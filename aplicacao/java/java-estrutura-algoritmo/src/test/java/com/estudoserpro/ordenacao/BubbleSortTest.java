package com.estudoserpro.ordenacao;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BubbleSortTest {

    private BubbleSort bubbleSort;

    @BeforeEach
    public void setUp() {
        this.bubbleSort = new BubbleSort();
    }

    @Test
    public void ordenacaoAsc() {

        Integer[] lista = { 64, 34, 25, 12, 22, 11, 90 };
        Integer[] listaOrdenada = { 11, 12, 22, 25, 34, 64, 90 };

        bubbleSort.setLista(lista);

        assertArrayEquals(listaOrdenada, bubbleSort.ordenacao(true));

    }

    @Test
    public void ordenacaoDesc() {

        Integer[] lista = { 64, 34, 25, 12, 22, 11, 90 };
        Integer[] listaOrdenada = { 90, 64, 34, 25, 22, 12, 11 };

        bubbleSort.setLista(lista);

        assertArrayEquals(listaOrdenada, bubbleSort.ordenacao(false));

    }    
}
