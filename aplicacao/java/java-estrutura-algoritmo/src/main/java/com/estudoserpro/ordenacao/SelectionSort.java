package com.estudoserpro.ordenacao;

public class SelectionSort {

	private Integer[] lista;

	public void setLista(Integer[] lista2) {
		this.lista = lista2;
	}

	public Integer[] getLista() {
		return lista;
	}

	public Integer encontreMenorValor() {
		Integer menorValor = lista[0];
		
		for (int indice = 1; indice < lista.length; indice++) {
			if (lista[indice] < menorValor) {
				menorValor = lista[indice];
			}
		}
		
		return menorValor;
	}

}
