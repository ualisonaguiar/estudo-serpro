package com.estudoserpro.ordenacao;

public class BubbleSort {

    private Integer[] lista;

    public Integer[] getLista() {
        return this.lista;
    }

    public void setLista(Integer[] lista) {
        this.lista = lista;
    }

    public Integer[] ordenacao(boolean ascending) {
        Integer[] array = getLista();
        int n = array.length;
        boolean swapped;

        do {
            swapped = false;
            for (int i = 0; i < n - 1; i++) {
                boolean needToSwap = ascending ? array[i] > array[i + 1] : array[i] < array[i + 1];

                if (needToSwap) {
                    trocaElemento(array, i, i);
                    swapped = true;
                }
            }
            n--;
        } while (swapped);

        return array;
    }

    protected void trocaElemento(Integer[] array, int i, int j) {
        int temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
    }

}
