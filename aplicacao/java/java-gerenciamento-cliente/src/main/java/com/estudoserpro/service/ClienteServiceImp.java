package com.estudoserpro.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.estudoserpro.model.Cliente;
import com.estudoserpro.model.ClienteDAO;

public class ClienteServiceImp implements ClienteServiceInterface {

	private ClienteDAO repository;
	private static Logger log = Logger.getLogger(ClienteServiceImp.class);

	public ClienteServiceImp() {
		this.repository = new ClienteDAO();
	}

	public List<Cliente> listagem() {
		List<Cliente> clientes = new ArrayList<>();
		try {
			clientes = repository.listagem();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clientes;
	}

	public Cliente informacaoCliente(Long id) throws ClienteNaoEncontradoException {
		Cliente cliente = null;
		try {
			cliente = repository.informacaoCliente(id);
			if (cliente == null) {
				throw new ClienteNaoEncontradoException("Cliente não encontrado");
			}
		} catch (SQLException e) {
			LOG.info(e);
		}

		return cliente;
	}

	public Cliente salvar(Cliente cliente) {

		try {
			if (cliente.getId() == null) {
				repository.inserir(cliente);
			} else {
				repository.atualizar(cliente);
			}
		} catch (SQLException e) {
			LOG.info(e);
		}

		return cliente;
	}

	public Boolean excluir(final Long id) {
		try {
			repository.informacaoCliente(id).getId();
			repository.excluir(id);
		} catch (SQLException e) {
			LOG.info(e);
		}
		
		return true;
	}

}
