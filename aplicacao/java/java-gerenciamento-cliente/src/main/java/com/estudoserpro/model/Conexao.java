package com.estudoserpro.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

abstract class AbstractConexao {

	private Connection conexao;

	public Connection getConexao() throws SQLException {

		if (conexao == null) {
			conexao = DriverManager.getConnection("jdbc:mysql://10.67.43.196:3306/db_estudo_serpro", "root", "abcd1234");
		}

		return conexao;
	}

}
