package com.estudoserpro.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO extends AbstractConexao {

	public List<Cliente> listagem() throws SQLException {
		List<Cliente> clientes = new ArrayList<>();
		String sql = "select * from cliente";
		Connection conexao = getConexao();
		PreparedStatement statement = conexao.prepareStatement(sql);		
		try {
			statement.execute();
			ResultSet resultSet = statement.getResultSet();
			while (resultSet.next()) {
				clientes.add(poupalteResult(resultSet));
			}
		} finally {
			statement.close();
			conexao.close();
		}

		return clientes;
	}

	public Cliente informacaoCliente(final Long id) throws SQLException {
		String sql = "select * from cliente where id = ?";

		Connection conexao = getConexao();
		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setLong(1, id);
		statement.execute();
		ResultSet resultSet = statement.getResultSet();

		Cliente cliente = null;
		while (resultSet.next()) {
			cliente = poupalteResult(resultSet);
		}

		return cliente;
	}

	public Cliente inserir(Cliente cliente) throws SQLException {
		Connection conexao = getConexao();
		String sql = "insert into cliente (nome, data_nascimento) values (?, ?)";
		PreparedStatement statement = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, cliente.getNome());
		statement.setDate(2, Date.valueOf(cliente.getDataNascimento()));
		statement.execute();

		try (ResultSet resultSet = statement.getGeneratedKeys()) {
			if (resultSet.next()) {
				cliente.setId(resultSet.getLong(1));
			}
		}

		return cliente;
	}

	public Cliente atualizar(Cliente cliente) throws SQLException {
		Connection conexao = getConexao();
		String sql = "update cliente set nome = ?, data_nascimento = ? where id = ?";

		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setString(1, cliente.getNome());
		statement.setDate(2, Date.valueOf(cliente.getDataNascimento()));
		statement.setLong(3, cliente.getId());
		statement.execute();

		return cliente;
	}

	public void excluir(long Id) throws SQLException {
		Connection conexao = getConexao();

		String sql = "delete from cliente where id = ?";
		PreparedStatement statement = conexao.prepareStatement(sql);
		statement.setLong(1, Id);
		statement.execute();
	}

	protected Cliente poupalteResult(ResultSet resultSet) throws SQLException {
		Cliente cliente = new Cliente();
		cliente.setId(resultSet.getLong("id"));
		cliente.setNome(resultSet.getString("nome"));
		cliente.setDataNascimento(resultSet.getDate("data_nascimento").toLocalDate());

		return cliente;
	}
}
