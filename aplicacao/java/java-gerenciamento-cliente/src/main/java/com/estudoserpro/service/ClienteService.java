package com.estudoserpro.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

import com.estudoserpro.model.Cliente;

public class ClienteService {

	private ClienteServiceInterface repository;

	public ClienteService(ClienteServiceInterface repository) {
		this.repository = repository;
	}

	public List<Cliente> listagem() {
		return repository.listagem();
	}

	public Cliente informacaoCliente(final Long id) throws ClienteNaoEncontradoException {
		Cliente cliente = repository.informacaoCliente(id);
		if (cliente == null) {
			throw new ClienteNaoEncontradoException("Cliente não encontrado.");
		}
		
		return cliente;
	}

	public Cliente salvar(final Cliente cliente) {
		return repository.salvar(cliente);
	}

	public Boolean excluir(final Long id) throws ClienteNaoEncontradoException {
		repository.informacaoCliente(id).getId();
		repository.excluir(id);
		
		return true;
	}
	
	public int idade(final Long id) throws ClienteNaoEncontradoException {
		Cliente cliente = repository.informacaoCliente(id);
		
		return Period.between(cliente.getDataNascimento(), LocalDate.now()).getYears();
	}
}
