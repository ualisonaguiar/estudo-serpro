package com.estudoserpro.service;

public class ClienteNaoEncontradoException extends Exception {

	public ClienteNaoEncontradoException(final String message) {
		super(message);
	}

}
