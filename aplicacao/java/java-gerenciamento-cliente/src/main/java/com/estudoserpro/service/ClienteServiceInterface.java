package com.estudoserpro.service;

import java.util.List;

import com.estudoserpro.model.Cliente;

public interface ClienteServiceInterface {

	public List<Cliente> listagem();

	public Cliente informacaoCliente(final Long id) throws ClienteNaoEncontradoException;

	public Cliente salvar(final Cliente cliente);

	public Boolean excluir(final Long id);

}
