package com.estudoserpro.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import com.estudoserpro.model.Cliente;

public class ClienteStub implements ClienteServiceInterface {
	
	private HashMap<Long, Cliente> clientes = new HashMap<Long, Cliente>();
	
	public List<Cliente> listagem() {
		List<Cliente> clientes = new ArrayList<Cliente>();
		clientes().forEach((id, cliente) -> clientes.add(cliente));
		
		return clientes;
	}

	public Cliente informacaoCliente(Long id) throws ClienteNaoEncontradoException {
		return clientes().get(id);
	}

	public Cliente salvar(Cliente cliente) {
		if (cliente.getId() == null) {
			clientes();
			cliente.setId(generateId());
		}
		
		clientes.put(cliente.getId(), cliente);
		return cliente;
	}

	public Boolean excluir(Long id) {
		if (clientes.containsKey(id)) {
			clientes.remove(id);
		}

		return true;
	}
	
	public Long generateId() {
		clientes();
		
		Iterator<Entry<Long, Cliente>> iterator = clientes.entrySet().iterator();
		Entry<Long, Cliente> ultimoCliente = null;
		
		
		while(iterator.hasNext()) {
			ultimoCliente = iterator.next();
		}
		
		return ultimoCliente.getValue().getId() + 1;
	}
	
	
	protected HashMap<Long, Cliente> clientes() {
		clientes.put(1L, new Cliente(1L, "Ualison Aguiar", LocalDate.of(1988, 2, 1)));
		clientes.put(2L, new Cliente(2L, "Vanda Silva", LocalDate.of(1986, 7, 17)));
		clientes.put(3L, new Cliente(3L, "JOão Paulo", LocalDate.of(2010, 8, 20)));
		clientes.put(4L, new Cliente(4L, "Liz Rodrigues", LocalDate.of(2021, 6, 15)));
		
		return clientes;
	}

}
