package com.estudoserpro.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.estudoserpro.model.Cliente;

public class ClienteServiceFakeTest {
	
	private ClienteService service;
	
	@Before
	public void setUp() {
		service = new ClienteService(new ClienteFake());
	}
	
	@Test
	public void listagem() {
		assertTrue(service.listagem().isEmpty());
	}
	
	@Test
	public void informacaoCliente() throws ClienteNaoEncontradoException {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Ualison Aguiar");
		cliente.setDataNascimento(LocalDate.of(1988, 2, 1));		
		
		Cliente cliente2 = service.informacaoCliente(cliente.getId());
		
		assertEquals(cliente.getId(), cliente2.getId());
		assertEquals(cliente.getNome(), cliente2.getNome());
		assertEquals(cliente.getDataNascimento(), cliente2.getDataNascimento());
	}
	
	
	public void salvar() throws ClienteNaoEncontradoException {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Ualison Aguiar");
		cliente.setDataNascimento(LocalDate.of(1988, 2, 1));
		
		service.salvar(cliente);
		
		Cliente cliente2 = service.informacaoCliente(cliente.getId());
		
		assertEquals(cliente.getNome(), cliente2.getNome());
		assertEquals(cliente.getDataNascimento(), cliente2.getDataNascimento());
		assertEquals(cliente.getId(), cliente2.getId());
	}
	
	@Test
	public void excluir() throws ClienteNaoEncontradoException {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Ualison Aguiar");
		cliente.setDataNascimento(LocalDate.of(1988, 2, 1));
		
		assertTrue(service.excluir(cliente.getId()));
	}

	
	@Test
	public void calcularIdade() throws ClienteNaoEncontradoException {
		assertEquals(35, service.idade(1L));
	}
	
}
