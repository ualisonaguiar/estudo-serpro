package com.estudoserpro.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.estudoserpro.model.Cliente;

public class ClienteServiceStubTest {
	
	private ClienteService service;
	
	@Before
	public void setUp() {
		service = new ClienteService(new ClienteStub());
	}
	
	@Test
	public void listagem() {
		assertFalse(service.listagem().isEmpty());
	}
	
	@Test
	public void informacaoCliente() throws ClienteNaoEncontradoException {
		Optional<Cliente> clientes = service.listagem().stream().filter(cliente -> cliente.getId().equals(2L)).findAny();
		Cliente cliente = service.informacaoCliente(clientes.get().getId());
		
		assertEquals(clientes.get().getId(), cliente.getId());
		assertEquals(clientes.get().getNome(), cliente.getNome());
		assertEquals(clientes.get().getDataNascimento(), cliente.getDataNascimento());
	}
	
	@Test
	public void inserir() throws ClienteNaoEncontradoException {
		Cliente cliente = new Cliente();
		cliente.setNome("Iza");
		cliente.setDataNascimento(LocalDate.of(2017, 9, 23));
		service.salvar(cliente);
		
		Cliente cliente2 = service.informacaoCliente(cliente.getId());
		assertEquals(cliente.getId(), cliente2.getId());
		assertEquals(cliente.getNome(), cliente2.getNome());
		assertEquals(cliente.getDataNascimento(), cliente2.getDataNascimento());		
	}
	
	public void salvar() throws ClienteNaoEncontradoException {
		Cliente cliente = new Cliente();
		cliente.setNome("Iza");
		cliente.setDataNascimento(LocalDate.of(2017, 9, 23));
		service.salvar(cliente);
		
		cliente.setNome("Lurdy");
		cliente.setDataNascimento(LocalDate.of(2010, 11, 23));
		service.salvar(cliente);
		
		Cliente cliente2 = service.informacaoCliente(cliente.getId());
		
		assertEquals(cliente.getNome(), cliente2.getNome());
		assertEquals(cliente.getDataNascimento(), cliente2.getDataNascimento());
		assertEquals(cliente.getId(), cliente2.getId());
	}
	
	@Test
	public void excluir() throws ClienteNaoEncontradoException {
		List<Cliente> clientes = service.listagem();
		assertTrue(service.excluir(clientes.get(2).getId()));
	}
	
	@Test
	public void clienteNaoExiste() {
		try {
			service.informacaoCliente(8L);
			fail("Aqui deverai retrona uma falha");
		} catch (ClienteNaoEncontradoException e) {
			assertEquals("Cliente não encontrado.", e.getMessage());
		}
	}
	
	@Test
	public void calcularIdade() throws ClienteNaoEncontradoException {
		Cliente cliente = service.informacaoCliente(1L);
		assertEquals(35, service.idade(cliente.getId()));
	}
	
}
