package com.estudoserpro.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.estudoserpro.model.Cliente;

public class ClienteFake implements ClienteServiceInterface {

	@Override
	public List<Cliente> listagem() {
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		return clientes;
	}

	@Override
	public Cliente informacaoCliente(Long id) throws ClienteNaoEncontradoException {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Ualison Aguiar");
		cliente.setDataNascimento(LocalDate.of(1988, 2, 1));
		
		return cliente;
	}

	@Override
	public Cliente salvar(Cliente cliente) {
		cliente.setId(1L);
		cliente.setNome("Ualison Aguiar");
		cliente.setDataNascimento(LocalDate.of(1988, 2, 1));
		
		return cliente;
	}

	@Override
	public Boolean excluir(Long id) {
		return true;
	}

}
