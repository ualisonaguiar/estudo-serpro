package com.estudoserpro.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.estudoserpro.model.Cliente;

public class ClienteServiceTest {

	private ClienteService service;

	@Before
	public void setUp() {
		service = new ClienteService(new ClienteServiceImp());
	}

	@Test
	public void listagem() {
		cargaInicial();
		assertTrue(service.listagem().size() > 0);
	}
	
	@Test
	public void inserir() {
		Cliente cliente = cargaInicial();
		assertNotNull(cliente.getId());

		Optional<Cliente> resultCliente = service.listagem().stream()
				.filter(cliente2 -> cliente2.getId().equals(cliente.getId())).findAny();
		
		assertTrue(resultCliente.isPresent());
	}
	
	@Test
	public void atualizar() throws ClienteNaoEncontradoException {
		Cliente cliente = cargaInicial();
		cliente.setNome(cliente.getNome().concat("---"));
		service.salvar(cliente);
		
		Cliente cliente2 = service.informacaoCliente(cliente.getId());
		
		assertEquals(cliente.getNome(), cliente2.getNome());
		assertEquals(cliente.getId(), cliente2.getId());
	}
	
	@Test
	public void excluir() throws ClienteNaoEncontradoException {
		Cliente cliente = cargaInicial();
		assertTrue(service.excluir(cliente.getId()));
	}
	
	@Test
	public void calcularIdade() throws ClienteNaoEncontradoException {
		Cliente cliente = cargaInicial();
		
		assertEquals(23, service.idade(cliente.getId()));
	}
	
	public Cliente cargaInicial() {
		Cliente cliente = new Cliente();
		cliente.setDataNascimento(LocalDate.of(2000, 3, 18));
		cliente.setNome("De Moura");
		service.salvar(cliente);
		
		return cliente;
	}
	
}
