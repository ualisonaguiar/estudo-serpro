package com.estudoserpro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCalculadora {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp() {
        calculadora = new Calculadora();
    }

    @Test
    public void testSomaDouble() {
        assertEquals(calculadora.soma(100.00, 200.00), 300.00);
    }

    @Test
    public void testSomaInt() {
        assertEquals(calculadora.soma(100, 200), 300);
    }

    @Test
    public void testSubtracaoDouble() {
        assertEquals(calculadora.subtracao(100.00, 200.00), -100.00);
    }

    @Test
    public void testSubtracao() {
        assertEquals(calculadora.subtracao(100, 200), -100);
    }

    @Test
    public void testMultiplicacaoDouble() {
        assertEquals(calculadora.multiplicacao(100.00, 200.00), 20000.00);
    }

    @Test
    public void testMultiplicacao() {
        assertEquals(calculadora.multiplicacao(100, 200), 20000);
    }    

    @Test
    public void testDiviscao() throws ExceptionDivisaoValorNaoPermitido {
        assertEquals(calculadora.divisao(200, 10), 20.0);
    }
}
