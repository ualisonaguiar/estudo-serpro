package com.estudoserpro;

public class ExceptionDivisaoValorNaoPermitido extends Exception {

    public ExceptionDivisaoValorNaoPermitido(String message) {
        super(message);
    }    
}
