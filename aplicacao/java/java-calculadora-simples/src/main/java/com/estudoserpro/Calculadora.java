package com.estudoserpro;

public class Calculadora {

    private static final float VALOR_NAO_PERMITIDO_DIVISAO = 0;

    public double soma(double digito1, double digito2) {
        return digito1 + digito2;
    }

    public int soma(int digito1, int digito2) {
        return digito1 + digito2;
    }    

    public double subtracao(double digito1, double digito2) {
        return digito1 - digito2;
    }

    public int subtracao(int digito1, int digito2) {
        return digito1 - digito2;
    }    

    public double multiplicacao(double digito1, double digito2) {
        return digito1 * digito2;
    }

    public int multiplicacao(int digito1, int digito2) {
        return digito1 * digito2;
    }    

    public float divisao(float digito1, float digito2) throws ExceptionDivisaoValorNaoPermitido {
        if (digito2 == VALOR_NAO_PERMITIDO_DIVISAO) {
            throw new ExceptionDivisaoValorNaoPermitido(
                    String.format("Valor %s não permitido para divisão", digito2));
        }

        return digito1 / digito2;
    }
}
