Sistema de Lista de Tarefas (sem utilização de banco de dados)
Este é um sistema simples de lista de tarefas desenvolvido em Java que não utiliza banco de dados, mas sim coleções Java para armazenar e gerenciar as tarefas inseridas pelos usuários.

Requisitos Funcionais:
Adicionar tarefa: O sistema permite que o usuário insira uma nova tarefa na lista, fornecendo um título e uma descrição para a tarefa.

Remover tarefa: O usuário pode remover uma tarefa existente da lista, indicando o título ou um identificador único da tarefa.

Visualizar lista de tarefas: O sistema exibe a lista completa de tarefas cadastradas, mostrando seus títulos e descrições.

Marcar tarefa como concluída: O usuário tem a opção de marcar uma tarefa como concluída, alterando seu status para "feito".

Requisitos Não Funcionais:
Interface de linha de comando: O sistema foi implementado com uma interface de linha de comando simples e intuitiva, permitindo ao usuário interagir com o sistema através de comandos no terminal.

Armazenamento em coleções Java: O sistema utiliza coleções Java, como ArrayList, para armazenar as tarefas e seus atributos em tempo de execução.

Persistência temporária: Todas as informações são mantidas na memória durante a execução do programa e não são armazenadas além disso.

Tratamento de erros: O sistema lida adequadamente com possíveis erros e exceções, informando ao usuário e permitindo a continuidade da execução sempre que possível.

Performance: O sistema é eficiente para manipular a lista de tarefas, mesmo com um grande número de itens.

Como usar:
Clone este repositório para o seu computador ou faça o download dos arquivos.

Certifique-se de ter o Java instalado em sua máquina.

Abra um terminal (ou prompt de comando) e navegue até o diretório onde os arquivos do sistema estão localizados.

Compile o código Java, caso necessário: javac ListaDeTarefas.java

Execute o programa: java ListaDeTarefas

A partir daí, siga as instruções exibidas no terminal para adicionar, remover, visualizar e marcar tarefas como concluídas.

Observações:
Este sistema de lista de tarefas é um exemplo simples de aplicação que utiliza coleções Java para gerenciar informações sem a necessidade de um banco de dados. Ele permite que o usuário adicione, remova, visualize e marque como concluída as tarefas de uma lista. As informações são armazenadas na memória durante a execução do programa e não persistem além disso. Para sistemas mais complexos e com necessidades de armazenamento permanentes, a utilização de um banco de dados seria mais adequada.