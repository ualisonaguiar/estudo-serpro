package br.estudoserpro.listatarefa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

import br.estudoserpro.listatarefa.domain.SituacaoTarefaEnum;
import br.estudoserpro.listatarefa.domain.Tarefa;
import br.estudoserpro.listatarefa.domain.TipoTarefaEnum;

public class TarefaServiceTest {

    TarefaService service;

    TarefaServiceTest() {
        service = new TarefaService();
    }

    @Test
    public void listaVazia() {
        assertTrue(service.getListagem().isEmpty());
    }

    @Test
    public void adicionar() throws NoSuchElementException {
        Tarefa tarefa = Tarefa.builder()
                .descricao("Lava louça do jantar")
                .tipoTarefa(TipoTarefaEnum.CASA)
                .build();

        this.service.salvar(tarefa);

        List<Tarefa> tarefas = service.getListagem();
        assertFalse(tarefas.isEmpty());

        assertEquals(tarefa, service.getId(tarefa.getId()));
    }

    @Test
    public void alterar() throws NoSuchElementException {
        Tarefa tarefa = Tarefa.builder()
                .descricao("Fazer deploy no ambiente de N-1 da aplicação")
                .tipoTarefa(TipoTarefaEnum.OUTROS)
                .build();

        service.salvar(tarefa);
        tarefa.setTipoTarefa(TipoTarefaEnum.TRABALHO);
        service.salvar(tarefa, tarefa.getId());

        assertEquals(tarefa, service.getId(tarefa.getId()));
    }

    @Test
    public void excluir() throws NoSuchElementException {
        Tarefa tarefa = Tarefa.builder()
                .descricao("Fazer compras do mês")
                .tipoTarefa(TipoTarefaEnum.FAMILIA)
                .build();

        this.service.salvar(tarefa);

        assertEquals(1, service.getListagem().size());
        service.excluir(tarefa);
        assertTrue(service.getListagem().isEmpty());
    }

    @Test
    public void marcarConcluida() throws NoSuchElementException {
        Tarefa tarefa = Tarefa.builder()
                .descricao("Lava louça do jantar")
                .tipoTarefa(TipoTarefaEnum.CASA)
                .build();

        service.salvar(tarefa);
        service.marcarConcluida(tarefa);

        assertEquals(SituacaoTarefaEnum.CONCLUIDA, tarefa.getSituacao());
    }    
}
