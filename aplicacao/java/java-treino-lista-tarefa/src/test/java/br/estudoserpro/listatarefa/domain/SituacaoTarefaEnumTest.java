package br.estudoserpro.listatarefa.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SituacaoTarefaEnumTest {

    @Test
    public void testValoresEnum() {

        assertEquals(SituacaoTarefaEnum.NOVA, SituacaoTarefaEnum.valueOf("NOVA"));
        assertEquals(SituacaoTarefaEnum.EM_ANDAMENTO, SituacaoTarefaEnum.valueOf("EM_ANDAMENTO"));
        assertEquals(SituacaoTarefaEnum.CONCLUIDA, SituacaoTarefaEnum.valueOf("CONCLUIDA"));
        assertEquals(SituacaoTarefaEnum.CANCELADA, SituacaoTarefaEnum.valueOf("CANCELADA"));

    }

}
