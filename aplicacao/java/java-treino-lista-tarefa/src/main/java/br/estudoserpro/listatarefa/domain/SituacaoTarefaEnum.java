package br.estudoserpro.listatarefa.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum SituacaoTarefaEnum {

    NOVA(1),
    EM_ANDAMENTO(2),
    CONCLUIDA(3),
    CANCELADA(4);

    private final int codigo;
}
