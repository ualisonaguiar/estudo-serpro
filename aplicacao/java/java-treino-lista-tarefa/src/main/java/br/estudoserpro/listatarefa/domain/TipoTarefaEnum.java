package br.estudoserpro.listatarefa.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TipoTarefaEnum {

    CASA(1),
    TRABALHO(2),
    FAMILIA(3),
    OUTROS(4);

    private final int codigo;
}
