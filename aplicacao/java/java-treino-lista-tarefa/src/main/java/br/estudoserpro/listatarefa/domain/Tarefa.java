package br.estudoserpro.listatarefa.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Tarefa {
    
    private Long id;
    private String descricao;
    private TipoTarefaEnum tipoTarefa;
    private SituacaoTarefaEnum situacao;
}
