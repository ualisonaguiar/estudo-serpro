package br.estudoserpro.listatarefa;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

import br.estudoserpro.listatarefa.domain.SituacaoTarefaEnum;
import br.estudoserpro.listatarefa.domain.Tarefa;
import br.estudoserpro.listatarefa.domain.TipoTarefaEnum;
import br.estudoserpro.listatarefa.service.TarefaService;

public class App {

    private static final Logger log = Logger.getLogger(App.class.getName());

    public static void main(String[] args) {

        try {
            TarefaService service = new TarefaService();
            Tarefa tarefa = Tarefa.builder()
                    .descricao("Lava louça do jantar")
                    .tipoTarefa(TipoTarefaEnum.CASA)
                    .build();

            service.salvar(tarefa);
            tarefa.setSituacao(SituacaoTarefaEnum.EM_ANDAMENTO);
            service.salvar(tarefa, tarefa.getId());

            log.info("=====================");
            log.info(tarefa.getDescricao());

        } catch (NoSuchElementException e) {
            log.info("========= Erro ============");
            log.info(e.getMessage());
        }

    }

}
