package br.estudoserpro.listatarefa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import br.estudoserpro.listatarefa.domain.SituacaoTarefaEnum;
import br.estudoserpro.listatarefa.domain.Tarefa;

public class TarefaService {

    private List<Tarefa> listaTarefa;

    public TarefaService() {
        this.listaTarefa = new ArrayList<>();
    }

    public List<Tarefa> getListagem() {
        return listaTarefa;
    }

    public Tarefa getId(Long id) throws NoSuchElementException {

        return getListagem().get(getIndex(id));
    }

    public Tarefa salvar(Tarefa tarefa) {
        tarefa.setId(System.currentTimeMillis());

        if (tarefa.getSituacao() == null) {
            tarefa.setSituacao(SituacaoTarefaEnum.NOVA);
        }

        this.listaTarefa.add(tarefa);

        return tarefa;
    }

    public Tarefa salvar(Tarefa tarefa, Long id) throws NoSuchElementException {
        getListagem().add(getIndex(id), tarefa);
        return tarefa;
    }

    public void excluir(Tarefa tarefa) throws NoSuchElementException {
        getListagem().remove(getIndex(tarefa.getId()));
    }

    public Tarefa marcarConcluida(Tarefa tarefa) throws NoSuchElementException {
        int index = getIndex(tarefa.getId());
        tarefa.setSituacao(SituacaoTarefaEnum.CONCLUIDA);
        getListagem().add(index, tarefa);

        return tarefa;
    }

    protected int getIndex(Long id) throws NoSuchElementException {
        int index = -1;
        for (int i = 0; i < getListagem().size(); i++) {
            if (getListagem().get(i).getId().equals(id)) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            throw new NoSuchElementException("ID" + id + " não encontrado");
        }

        return index;
    }
}
