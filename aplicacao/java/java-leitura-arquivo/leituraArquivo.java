import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

class leituraArquivo {

	private static final Logger LOGGER = Logger.getLogger(leituraArquivo.class.getName());

	public static void main(String args[]) throws IOException {
		Path pathArquivo = Paths.get("linguagens.txt");
		List<String> linhas = Files.readAllLines(pathArquivo);

		FileWriter arquivo = new FileWriter("linguagem_nao_duplicado.txt");
		linhas.stream().distinct().sorted().forEach(linguagem -> {
			try {
				arquivo.write(linguagem.concat("\n"));
			} catch (IOException e) {
				LOGGER.info(e.toString());
			}
		});
		arquivo.close();
	}
}