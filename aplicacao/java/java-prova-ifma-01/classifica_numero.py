
class ClassificaNumero():
    def __init__(self, lista_numeros) -> None:
        self.lista_numeros = lista_numeros
        
    def trata_numeros(self) -> [int]:
        numero_tratado = [x ** 2 if x % 2 == 0 else x**3 for x in self.lista_numeros]

        return numero_tratado