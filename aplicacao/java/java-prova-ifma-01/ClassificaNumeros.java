import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClassificaNumeros {

    private List<Integer> listaNumeros = new ArrayList<>();

    public ClassificaNumeros setListaNumeros(List<Integer> listaNumeros) {
        this.listaNumeros = listaNumeros;
        return this;
    }

    public List<Integer> tratarNumeros() {
        return listaNumeros.stream().map(numero -> numero % 2 == 0 ? numero * numero : numero * numero * numero)
                .collect(Collectors.toList());
    }
}
