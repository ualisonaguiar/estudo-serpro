from classifica_numero import ClassificaNumero

lista_numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

classifica_numero = ClassificaNumero(lista_numeros)

print("numeros pares: ")
print(classifica_numero.trata_numeros())