import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class estudo_01 {
    
    public static void main(String[] args) {
        List<Integer> pilha = new ArrayList<>(Arrays.asList(30, 50, 60, 10, 9));

        removeDaPilha(50, pilha);
    }

    public static void removeDaPilha(Integer numero, List<Integer> pilha) {
        int index = pilha.indexOf(numero);

        if (index != -1) {
            pilha.remove(index);
            System.out.println("Item removido com sucesso");
        } else {
            System.out.println("Item não pode ser removido");
        }
    }

}
