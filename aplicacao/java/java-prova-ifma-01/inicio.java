import java.util.Arrays;
import java.util.List;

public class inicio {
    public static void main(String args[]) {

        ClassificaNumeros numeros = new ClassificaNumeros();

        numeros.setListaNumeros(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

        System.out.println(numeros.tratarNumeros());
    }
}