package br.com.ualison.downloadmaterial.estrategia.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;

import br.com.ualison.downloadmaterial.SituacaoDownloadEnum;
import br.com.ualison.downloadmaterial.estrategia.dto.ArquivoDTO;
import br.com.ualison.downloadmaterial.estrategia.model.Arquivo;
import br.com.ualison.downloadmaterial.estrategia.model.TipoMaterialEnum;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@PropertySource("classpath:application.properties")
public class ArquivoImpServiceTest {

	@Autowired
	private ArquivoImpService service;


	@Test
	@Order(1)
	public void listagemArquivoPendenteVazia() {
		assertTrue(service.listagemArquivoSituacao(SituacaoDownloadEnum.PENDENTE).isEmpty());
	}

	@Test
	@Order(2)
	public void registrarArquivo() {
		ArquivoDTO arquivoDTO = new ArquivoDTO();
		arquivoDTO.setCurso(
				"DataPrev (Analista de Tecnologia da Informação - Análise de Negócios) Engenharia de Software - 2023 (Pós-Edital)");
		arquivoDTO.setNome("Versão simplificada");
		arquivoDTO.setTipoMaterial(TipoMaterialEnum.PDF);
		arquivoDTO.setUrl("https://api.estrategiaconcursos.com.br");
		arquivoDTO.setAula("Aula 01");

		service.registrar(arquivoDTO.convertToEntity());

		assertFalse(service.listagemArquivoSituacao(SituacaoDownloadEnum.PENDENTE).isEmpty());
	}

	@Test
	@Order(3)
	public void alterarSituacaoDownloadArquivo() {
		List<Arquivo> arquivos = service.listagemArquivoSituacao(SituacaoDownloadEnum.PENDENTE);
		arquivos.get(0).setSituacaoDownload(SituacaoDownloadEnum.ANDAMENTO);
		service.registrar(arquivos.get(0));
		assertFalse(service.listagemArquivoSituacao(SituacaoDownloadEnum.ANDAMENTO).isEmpty());
	}

	@Test
	@Order(2)
	public void registrarArquivoVideoAula() {
		ArquivoDTO arquivoDTO = new ArquivoDTO();
		arquivoDTO.setCurso(
				"DataPrev (Analista de Tecnologia da Informação - Análise de Negócios) Engenharia de Software - 2023 (Pós-Edital)");
		arquivoDTO.setNome("Vídeo 1 - Análise e Projeto Orientados a Objeto");
		arquivoDTO.setTipoMaterial(TipoMaterialEnum.VIDEOAULA);
		arquivoDTO.setUrl(
				"https://cdn.estrategiaconcursos.com.br/storage/video/190810/ed_flopes_INT146691_temasespeciaisdetecn_01_360p.mp4?Expires=1691784440&Signature=DpHlI-9raYIwAkcjVoIFY4vRi5pWSayTGnCLvekEZ0DOXyhEaGlN0CtCJqHoOyxhnhyAe4r7MSNYwQmBRIWRBwxvoEYMPd9QMMLrSyOqr8f9bg7Wc~HcuiD9HejTNU-~vG8DihiQyyinHlznTADH632-sVt3VEEpMIFTCQHNZyzgDdx~H2TwXzXvR84-oA1rNaJcyZ47LbDA2QHcRoFBhbFnWwiu4ZNqobp9TL8vZCRY2suVPASGIthbBdDckrP8sERrSoGIZwTR1teQSCwiTZ4e2WkgiuqagXXdmAZp8UlrIhOx~pNO-B0CP-OEYS0V0iNMSUvGG-0lanKfEqZrVg__&Key-Pair-Id=APKAIMR3QKSK2UDRJITQ");
		arquivoDTO.setAula("Aula 02");

		service.registrar(arquivoDTO.convertToEntity());

		assertFalse(service.listagemArquivoSituacao(SituacaoDownloadEnum.PENDENTE).isEmpty());
	}

	@Test
	@Order(3)
	public void realizarDownload() {
		ArquivoDTO arquivoDTO = new ArquivoDTO();
		arquivoDTO.setCurso("COFFITO - Língua Portuguesa - 2023 (Pós-Edital)");
		arquivoDTO.setNome("Vídeo 1 - Causa e Consequência - Parte 02");
		arquivoDTO.setTipoMaterial(TipoMaterialEnum.VIDEOAULA);
		arquivoDTO.setUrl(
				"https://cdn.estrategiaconcursos.com.br/storage/video/122046/ed_afigueiredo_AUD-21669_portugues-causa-e-consequencia-pt-02.mp4?Expires=1692132551&Signature=iIWsCM57Quhm5fimrshJ9CNoossvnLWZZC0mNUWdbmSrJaKiuk5Wea7HgUSQlErRMEPycMGfcOTxWHzio-8r0hYNAFOQLnElP9ywpytqgp9kvQwEU~OPGPrtQDrf6N0DnZTGvsXi3PFN95L33zcVW3t8g5UafqEshD~KQ1GOhMj9K2wQQW-Bj6YmbytrSrQA2He054aoBDc6rbIXAYP1y0l~yG1vs2iZ5biW2btBC0iQD9ZV6qdr-2gxgO1cqiONX23BOJUa1B1h7MC6W3nZI5~u~u2MAajphe2xGmECX-Jmvn9b8ds9NUSksHx02g7f1iVa6QGOenxQc9cU~flvEA__&Key-Pair-Id=APKAIMR3QKSK2UDRJITQ");
		arquivoDTO.setAula("Aula 02");

		service.registrar(arquivoDTO.convertToEntity());
		
		service.realizarDownload(SituacaoDownloadEnum.PENDENTE, "/tmp/estrategia");
		
		assertTrue(1 == 1);
	}

}
