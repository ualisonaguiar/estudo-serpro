/**
 * 1º Parte
 */
//Insere a biblioteca JQUERY na página
var script = document.createElement("script");
script.src = "https://code.jquery.com/jquery-3.6.0.min.js"; // Substitua pela versão desejada do jQuery
script.onload = function() {
	console.log("jQuery carregado com sucesso!");

	$('.LessonList div').each(function() {
		setTimeout(() => {
			$(this).find('h2').click();
		}, 3000);
	});	
};
script.onerror = function() {
	console.error("Erro ao carregar o jQuery.");
};
document.body.appendChild(script);

/**
 * 2º Parte
 */
//Pegando informações princiapis
function registrarArquivoApi(aula, tipoMaterial, nome, url, nextAula = false) {
	var jsonData = {
		"aula": aula,
		"curso": $('.CourseInfo-content-title').text(),
		"tipoMaterial": tipoMaterial,
		"nome": nome,
		"url": url
	};

	$.ajax({
		type: "POST",
		url: "http://localhost:8080/api/estrategia/arquivo", // Substitua pela URL real do seu endpoint
		data: JSON.stringify(jsonData),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function(response) {
			console.log("Requisição bem-sucedida:", response);

			if (nextAula == true) {
				aulaAtual += 1;
				let tipoMaterialVideo = (tipoMaterial == 2);
				
				carregaMaterialAula(aulaAtual, tipoMaterialVideo);
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.error("Erro na requisição:", textStatus, errorThrown);
		}
	});
}

// Registrando arquivos do tipo PDF
//$('.LessonButton-icon .icon-file').css('border', '2px solid red');
function registrarMaterialPDF(aula) {
	$('.LessonList-item .isOpened .LessonButton-icon .icon-file').each(function(index) {
		let tipoMaterial = 1;
		let nome = $(this).parent().parent().parent().find('span:last').text();
		let url = $(this).parent().parent().parent().find('a').attr('href');
		let nextAula = (index == ($('.LessonList-item .isOpened .LessonButton-icon .icon-file').length - 1));
		
		console.log("Fim do arquivo: ", nextAula, index, $('.LessonList-item .isOpened .LessonButton-icon .icon-file').length - 1);
		
		registrarArquivoApi(aula, tipoMaterial, nome, url, nextAula);
	});
}

// Registrando o slides
function registrarMaterialSlide(aula) {
	$('.LessonList-item .isOpened .LessonButton-icon .icon-slides').each(function() {
		let tipoMaterial = 1;
		let nome = "Slides da Aula";
		let url = $(this).parent().parent().parent().parent().parent().find('a').attr('href');

		registrarArquivoApi(aula, tipoMaterial, nome, url, false);
	});
}

// Registrando o resumo
function registrarMaterialResumo(aula) {
	$('.LessonList-item .isOpened .LessonButton-icon .icon-list').each(function() {
		let tipoMaterial = 1;
		let nome = "Resumo";
		let url = $(this).parent().parent().parent().parent().parent().find('a').attr('href');

		registrarArquivoApi(aula, tipoMaterial, nome, url, false);
	});
}

// Registrando os videos
function registrarMaterialVideo(aula) {
	$('.LessonList-item .isOpened .ListVideos-items-video').each(function(index) {
		//pegando o nome da aula
		let nomeVideoAula = $(this).find('.VideoItem-info-index').html() + " - " + $(this).find('.VideoItem-info-title').text();
		$(this).find('.VideoItem').find('.VideoItem-info-title').trigger('click');


		if ($('.LessonList-item .isOpened sc-jlyJG:eq(0)').length == 0) {
			$('.LessonList-item .isOpened .sc-jlyJG:eq(0)').trigger('click');
		}

		let url = $('.LessonList-item .isOpened .sc-Rmtcm a:eq(0)').attr('href');
		let nextAula = (index == ($('.LessonList-item .isOpened .ListVideos-items-video').length - 1));

		registrarArquivoApi(aula, 2, nomeVideoAula, url, nextAula);
	});
}

function carregaMaterialAula(index, registraVideo = false) {
	let totalAulas = $('.LessonList div.LessonList-item').length;
	let totalAulasIndisponivel = $('.LessonList-item section').find('div.Box').find('.isDisabled').length;

	if (index <= (totalAulas - totalAulasIndisponivel)) {
		console.log("*******************************************************************************************");
		console.log("Fechando Index: ", index - 1);
		console.log("*******************************************************************************************");

		console.log("*******************************************************************************************");
		console.log("Próximo Index: ", index);
		console.log("*******************************************************************************************");
		$('.LessonList-item h2').eq(index).click();
		let aula = $('.isOpened .SectionTitle').text();
		
		if (registraVideo) {
			registrarMaterialVideo(aula);
		} else {
			registrarMaterialSlide(aula);
			registrarMaterialResumo(aula);
			registrarMaterialPDF(aula);
		}
	}
}

//listando as aulas
let aulaAtual = 0;
carregaMaterialAula(aulaAtual);


aulaAtual = 0;
carregaMaterialAula(aulaAtual, true);
