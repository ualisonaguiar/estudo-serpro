package br.com.ualison.downloadmaterial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
public class DownloadMaterialApplication {

	public static void main(String[] args) {
		SpringApplication.run(DownloadMaterialApplication.class, args);
	}

}
