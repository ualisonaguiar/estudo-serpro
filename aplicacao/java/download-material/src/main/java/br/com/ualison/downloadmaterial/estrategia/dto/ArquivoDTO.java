package br.com.ualison.downloadmaterial.estrategia.dto;

import br.com.ualison.downloadmaterial.SituacaoDownloadEnum;
import br.com.ualison.downloadmaterial.estrategia.model.Arquivo;
import br.com.ualison.downloadmaterial.estrategia.model.TipoMaterialEnum;
import lombok.Data;

@Data
public class ArquivoDTO {

	private Long id;
	private String aula;
	private String curso;
	private TipoMaterialEnum tipoMaterial;
	private String nome;
	private String url;
	private SituacaoDownloadEnum situacaoDownload;

	public Arquivo convertToEntity() {
		Arquivo arquivo = new Arquivo();
		arquivo.setId(getId());
		arquivo.setCurso(getCurso());
		arquivo.setTipoMaterial(getTipoMaterial());
		arquivo.setNome(getNome());
		arquivo.setUrl(getUrl());
		arquivo.setSituacaoDownload(getSituacaoDownload());
		arquivo.setAula(getAula());

		return arquivo;
	}	
}
