package br.com.ualison.downloadmaterial.estrategia.service;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.stereotype.Component;

import br.com.ualison.downloadmaterial.estrategia.model.Arquivo;
import br.com.ualison.downloadmaterial.estrategia.model.TipoMaterialEnum;

@Component
public class ArquivoDownloadService {

	public void download(final Arquivo arquivo, String pathDestino) {
		try {
			pathDestino = criarSubDiretorio(arquivo, pathDestino).concat("/").concat(getNomeArquivo(arquivo));

			URL url = new URL(arquivo.getUrl());
			URLConnection connection = url.openConnection();

			try (InputStream inputStream = connection.getInputStream();
					BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
					FileOutputStream fileOutputStream = new FileOutputStream(pathDestino)) {

				byte[] buffer = new byte[1024];
				int bytesRead;
				while ((bytesRead = bufferedInputStream.read(buffer)) != -1) {
					fileOutputStream.write(buffer, 0, bytesRead);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected String criarSubDiretorio(final Arquivo arquivo, String pathDestino) {
		try {
			pathDestino = pathDestino.concat("/").concat(arquivo.getCurso()).concat("/").concat(arquivo.getAula())
					.concat("/");

			if (arquivo.getTipoMaterial().equals(TipoMaterialEnum.PDF)) {
				pathDestino = pathDestino.concat("material-pdf");
			}

			if (arquivo.getTipoMaterial().equals(TipoMaterialEnum.VIDEOAULA)) {
				pathDestino = pathDestino.concat("videoaula");
			}

			Files.createDirectories(Paths.get(pathDestino));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return pathDestino;
	}

	protected String getNomeArquivo(Arquivo arquivo) {
		String extensao = "";
		if (arquivo.getTipoMaterial().equals(TipoMaterialEnum.PDF)) {
			extensao = ".pdf";
		}

		if (arquivo.getTipoMaterial().equals(TipoMaterialEnum.VIDEOAULA)) {
			extensao = ".mp4";
		}

		return arquivo.getNome().concat(extensao);
	}

}
