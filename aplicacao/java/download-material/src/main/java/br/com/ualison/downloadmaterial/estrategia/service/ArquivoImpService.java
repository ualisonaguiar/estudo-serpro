package br.com.ualison.downloadmaterial.estrategia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ualison.downloadmaterial.SituacaoDownloadEnum;
import br.com.ualison.downloadmaterial.estrategia.model.Arquivo;
import br.com.ualison.downloadmaterial.estrategia.model.ArquivoRepository;

@Service
public class ArquivoImpService implements ArquivoSevice {
	
	@Autowired
	private ArquivoRepository repository;
	
	@Override
	public Arquivo registrar(Arquivo arquivo) {
		if (arquivo.getId() == null) {
			arquivo.setSituacaoDownload(SituacaoDownloadEnum.PENDENTE);
		}
		
		repository.save(arquivo);
		return arquivo;
	}

	@Override
	public List<Arquivo> listagemArquivoSituacao(SituacaoDownloadEnum situacaoDownload) {
		return repository.findBysituacao(situacaoDownload);
	}

	@Override
	public void realizarDownload(final SituacaoDownloadEnum situacaoDownload, final String pathDiretorio) {
		List<Arquivo> listagemArquivos = listagemArquivoSituacao(situacaoDownload);
		ArquivoDownloadService arquivoDownload = new ArquivoDownloadService();

		
		listagemArquivos.stream().parallel().forEach(arquivo -> {
			arquivoDownload.download(arquivo, pathDiretorio);
		});
	}

}
