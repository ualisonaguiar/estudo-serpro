package br.com.ualison.downloadmaterial.estrategia.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;

import br.com.ualison.downloadmaterial.SituacaoDownloadEnum;
import br.com.ualison.downloadmaterial.estrategia.dto.ArquivoDTO;
import br.com.ualison.downloadmaterial.estrategia.model.Arquivo;
import br.com.ualison.downloadmaterial.estrategia.service.ArquivoSevice;

@RestController
@RequestMapping(value = "/api/estrategia/arquivo")
public class ArquivoEstrategiaController {

	private ArquivoSevice service;
	
	@Value("${app.diretorio.download}")
	private String diretorio;	

	@Autowired
	public ArquivoEstrategiaController(ArquivoSevice service) {
		this.service = service;
	}

	@GetMapping(value = { "", "/{situacaoDownload}" })
	@CrossOrigin
	public List<Arquivo> listagemArquivoSituacao(
			@PathVariable(required = false) Optional<SituacaoDownloadEnum> situacao) {
		SituacaoDownloadEnum situacaoDownload = SituacaoDownloadEnum.PENDENTE;
		if (situacao.isPresent()) {
			situacaoDownload = situacao.get();
		}

		
		return this.service.listagemArquivoSituacao(situacaoDownload);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<Arquivo> inserir(@RequestBody ArquivoDTO arquivo) {
		return ResponseEntity.ok().body(service.registrar(arquivo.convertToEntity()));
	}
	
	@GetMapping("/realizar-dwonload")
	public void teste() {
		service.realizarDownload(SituacaoDownloadEnum.PENDENTE, diretorio);
	}
}
