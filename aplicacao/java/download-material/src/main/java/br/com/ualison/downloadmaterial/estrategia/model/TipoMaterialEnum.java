package br.com.ualison.downloadmaterial.estrategia.model;

import com.fasterxml.jackson.annotation.JsonCreator;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoMaterialEnum {

	PDF("1"), VIDEOAULA("2");

	private String tipoMaterial;
	
	@JsonCreator
	public static TipoMaterialEnum getTipoMaterialFromCode(String value) {
		for (TipoMaterialEnum tipoMaterial: TipoMaterialEnum.values()) {
			if (tipoMaterial.getTipoMaterial().equals(value)) {
				return tipoMaterial;
			}
		}
		return null;
	}
}
