package br.com.ualison.downloadmaterial.estrategia.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ualison.downloadmaterial.SituacaoDownloadEnum;

@Repository
public interface ArquivoRepository extends JpaRepository<Arquivo, Long> {
	
	@Query(name = "Arquivo.findBySituacao")
	public List<Arquivo> findBysituacao(@Param("situacaoDownload") SituacaoDownloadEnum situacaoDownload);
	
}
