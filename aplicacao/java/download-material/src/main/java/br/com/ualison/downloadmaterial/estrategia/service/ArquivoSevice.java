package br.com.ualison.downloadmaterial.estrategia.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.ualison.downloadmaterial.SituacaoDownloadEnum;
import br.com.ualison.downloadmaterial.estrategia.model.Arquivo;

@Service
public interface ArquivoSevice {
	
	public Arquivo registrar(final Arquivo arquivo);
	
	public List<Arquivo> listagemArquivoSituacao(final SituacaoDownloadEnum situacaoDownload);
	
	public void realizarDownload(final SituacaoDownloadEnum situacaoDownload, final String diretorio);
}
