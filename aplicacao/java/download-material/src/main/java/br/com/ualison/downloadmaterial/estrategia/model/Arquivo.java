package br.com.ualison.downloadmaterial.estrategia.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import br.com.ualison.downloadmaterial.SituacaoDownloadEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "tb_arquivo_estrategia")
@NamedQueries({
	@NamedQuery(name = "Arquivo.findBySituacao", query = "SELECT a FROM Arquivo a WHERE a.situacaoDownload = :situacaoDownload")
})
public class Arquivo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 255, nullable = false)
	private String curso;
	
	@Column(length = 150, nullable = false)
	private String aula;
	
	@Enumerated(EnumType.STRING)
	private TipoMaterialEnum tipoMaterial;
	
	@Column(length = 255, nullable = false)
	private String nome;
	
	@Column(length = 1024, nullable = false)
	private String url;
	
	@Enumerated(EnumType.STRING)
	private SituacaoDownloadEnum situacaoDownload;	
}
