package br.com.ualison.downloadmaterial;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SituacaoDownloadEnum {

	PENDENTE(1), ANDAMENTO(2), CONCLUIDO(3), FALHA(4);
	
	private Integer situacaoDownload;
	
}
