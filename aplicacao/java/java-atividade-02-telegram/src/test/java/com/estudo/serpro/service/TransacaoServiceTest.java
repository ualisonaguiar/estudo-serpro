package com.estudo.serpro.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.estudo.serpro.model.Cliente;
import com.estudo.serpro.model.MoedaEnum;
import com.estudo.serpro.model.TipoTransacaoEnum;
import com.estudo.serpro.model.Transacao;
import com.estudo.serpro.util.ConversorMoedaNaoDefinida;
import com.estudo.serpro.util.ConversorMoedaUtil;

public class TransacaoServiceTest {

	TransacaoService service;

	@BeforeEach
	public void setup() {
		service = new TransacaoService();
	}

	@Test
	public void registrarTransacao() throws TransacaoNaoExisteException {
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Ualison Aguiar");

		Transacao transacao = new Transacao();
		transacao.setCliente(cliente);
		transacao.setMoeda(MoedaEnum.BRL);
		transacao.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacao.setValor(120.45);

		service.registrar(transacao);
		assertEquals(transacao, service.findTransacaoId(transacao.getId()));
	}

	@Test
	public void localizarTransicaoNaoExistente() {
		try {
			service.findTransacaoId(10000L);
			fail("Aqui deveria retorna um erro");
		} catch (TransacaoNaoExisteException e) {
			assertEquals("Transação não localizada", e.getMessage());
		}
	}

	@Test
	public void localizarTransicaoCliente() {

		populateCadastroTransacaoCliente();

		List<Transacao> transacaoCliente = service.findTransacaoCliente(3L);

		assertEquals(4, transacaoCliente.size());
		assertEquals(59.45, transacaoCliente.get(0).getValor());
	}

	@Test
	public void localizarTransicaoTipoTransacao() {
		populateCadastroTransacaoCliente();

		assertEquals(4, service.findTransacaoTransacao(TipoTransacaoEnum.DEPOSITO).size());
		assertEquals(1, service.findTransacaoTransacao(TipoTransacaoEnum.RETIRADA).size());
		assertEquals(1, service.findTransacaoTransacao(TipoTransacaoEnum.TRANSFERENCIA).size());
	}

	@Test
	public void converterMoeda() {
		populateCadastroTransacaoCliente();
		ConversorMoedaUtil conversor = new ConversorMoedaUtil();

		service.converterMoeda().stream().forEach(transacao -> {
			try {
				Double valorConvertido = conversor.convertaMoeda(transacao.getMoeda(), transacao.getValor());
				assertEquals(valorConvertido, transacao.getValorConvertido());
			} catch (ConversorMoedaNaoDefinida e) {
				fail("Moeda não encontrada. Não deveria acontecer isso.");
			}
		});
	}

	@Test
	public void saldoClienteMoedaConvertida() throws ConversorMoedaNaoDefinida {
		Cliente cliente = new Cliente();
		cliente.setId(8L);
		cliente.setNome("Vanda Silva");

		Transacao transacao1 = new Transacao();
		transacao1.setCliente(cliente);
		transacao1.setMoeda(MoedaEnum.EUR);
		transacao1.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacao1.setValor(560.45);
		service.registrar(transacao1);

		Transacao transacao2 = new Transacao();
		transacao2.setCliente(cliente);
		transacao2.setMoeda(MoedaEnum.USD);
		transacao2.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacao2.setValor(120.45);
		service.registrar(transacao2);

		Transacao transacao3 = new Transacao();
		transacao3.setCliente(cliente);
		transacao3.setMoeda(MoedaEnum.USD);
		transacao3.setTipoTransacao(TipoTransacaoEnum.RETIRADA);
		transacao3.setValor(120.45);
		service.registrar(transacao3);

		ConversorMoedaUtil conversor = new ConversorMoedaUtil();

		Double saldoEsperado = conversor.convertaMoeda(MoedaEnum.BRL, transacao1.getValor());
		saldoEsperado += conversor.convertaMoeda(MoedaEnum.BRL, transacao2.getValor());
		saldoEsperado += conversor.convertaMoeda(MoedaEnum.BRL, transacao3.getValor());

		assertEquals(saldoEsperado, service.saldoClienteMoedaConvertida(cliente.getId(), MoedaEnum.BRL));
	}

	@Test
	public void identificarTransacaoSuspeite() {

		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setNome("Liz Rodrigues");
		Transacao transacaoCliente1 = new Transacao();
		transacaoCliente1.setCliente(cliente);
		transacaoCliente1.setMoeda(MoedaEnum.BRL);
		transacaoCliente1.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente1.setValor(560.45);
		service.registrar(transacaoCliente1);

		Cliente cliente2 = new Cliente();
		cliente.setId(2L);
		cliente.setNome("João Paulo");
		Transacao transacaoCliente2 = new Transacao();
		transacaoCliente2.setCliente(cliente2);
		transacaoCliente2.setMoeda(MoedaEnum.BRL);
		transacaoCliente2.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente2.setValor(150.85);
		service.registrar(transacaoCliente2);

		Cliente cliente3 = new Cliente();
		cliente.setId(3L);
		cliente.setNome("Ualison Aguiar");
		Transacao transacaoCliente3 = new Transacao();
		transacaoCliente3.setCliente(cliente3);
		transacaoCliente3.setMoeda(MoedaEnum.BRL);
		transacaoCliente3.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente3.setValor(2980.85);
		service.registrar(transacaoCliente3);

		Cliente cliente4 = new Cliente();
		cliente.setId(4L);
		cliente.setNome("Vanda Silva");
		Transacao transacaoCliente4 = new Transacao();
		transacaoCliente4.setCliente(cliente4);
		transacaoCliente4.setMoeda(MoedaEnum.BRL);
		transacaoCliente4.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente4.setValor(5804.85);
		service.registrar(transacaoCliente4);

		List<Transacao> transacoesSuspeitas = service.identificarTransacaoSuspeita(2000.00);
		assertEquals(2, transacoesSuspeitas.size());
		assertTrue(transacoesSuspeitas.stream().filter(transacao -> transacao.getCliente().equals(cliente3)).findAny()
				.isPresent());
		assertTrue(transacoesSuspeitas.stream().filter(transacao -> transacao.getCliente().equals(cliente4)).findAny()
				.isPresent());
		
		transacoesSuspeitas = service.identificarTransacaoSuspeita(3000.00);
		assertEquals(1, transacoesSuspeitas.size());
		assertTrue(transacoesSuspeitas.stream().filter(transacao -> transacao.getCliente().equals(cliente4)).findAny()
				.isPresent());
		
		transacoesSuspeitas = service.identificarTransacaoSuspeita(500.00);
		assertEquals(3, transacoesSuspeitas.size());
		assertTrue(transacoesSuspeitas.stream().filter(transacao -> transacao.getCliente().equals(cliente)).findAny()
				.isPresent());		
		assertTrue(transacoesSuspeitas.stream().filter(transacao -> transacao.getCliente().equals(cliente3)).findAny()
				.isPresent());
		assertTrue(transacoesSuspeitas.stream().filter(transacao -> transacao.getCliente().equals(cliente4)).findAny()
				.isPresent());			
	}

	protected void populateCadastroTransacaoCliente() {
		Cliente cliente = new Cliente();
		cliente.setId(3L);
		cliente.setNome("Liz Rodrigues");

		Transacao transacaoCliente3 = new Transacao();
		transacaoCliente3.setCliente(cliente);
		transacaoCliente3.setMoeda(MoedaEnum.BRL);
		transacaoCliente3.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente3.setValor(560.45);
		service.registrar(transacaoCliente3);

		transacaoCliente3 = new Transacao();
		transacaoCliente3.setCliente(cliente);
		transacaoCliente3.setMoeda(MoedaEnum.BRL);
		transacaoCliente3.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente3.setValor(120.45);
		service.registrar(transacaoCliente3);

		transacaoCliente3 = new Transacao();
		transacaoCliente3.setCliente(cliente);
		transacaoCliente3.setMoeda(MoedaEnum.BRL);
		transacaoCliente3.setTipoTransacao(TipoTransacaoEnum.RETIRADA);
		transacaoCliente3.setValor(120.45);
		service.registrar(transacaoCliente3);

		transacaoCliente3 = new Transacao();
		transacaoCliente3.setCliente(cliente);
		transacaoCliente3.setMoeda(MoedaEnum.USD);
		transacaoCliente3.setTipoTransacao(TipoTransacaoEnum.TRANSFERENCIA);
		transacaoCliente3.setValor(59.45);
		service.registrar(transacaoCliente3);

		Cliente cliente2 = new Cliente();
		cliente2.setId(2L);
		cliente2.setNome("João Paulo");

		Transacao transacaoCliente2 = new Transacao();
		transacaoCliente2.setCliente(cliente2);
		transacaoCliente2.setMoeda(MoedaEnum.BRL);
		transacaoCliente2.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente2.setValor(120.45);
		service.registrar(transacaoCliente2);

		transacaoCliente2 = new Transacao();
		transacaoCliente2.setCliente(cliente2);
		transacaoCliente2.setMoeda(MoedaEnum.BRL);
		transacaoCliente2.setTipoTransacao(TipoTransacaoEnum.DEPOSITO);
		transacaoCliente2.setValor(120.45);
		service.registrar(transacaoCliente2);
	}
}
