package com.estudo.serpro.model;

public class Transacao {

    private Long id;
    private Cliente cliente;
    private MoedaEnum moeda;
    private TipoTransacaoEnum tipoTransacao;
    private Double valor;
    private Double valorConvertido;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValor() {
        return valor;
    }
    
    public void setValorConvertido(Double valorConvertido) {
        this.valorConvertido = valorConvertido;
    }

    public Double getValorConvertido() {
        return valorConvertido;
    }    

    public void setMoeda(MoedaEnum moeda) {
        this.moeda = moeda;
    }

    public MoedaEnum getMoeda() {
        return moeda;
    }

    public void setTipoTransacao(TipoTransacaoEnum tipoTransacao) {
        this.tipoTransacao = tipoTransacao;
    }

    public TipoTransacaoEnum getTipoTransacao() {
        return tipoTransacao;
    }
}
