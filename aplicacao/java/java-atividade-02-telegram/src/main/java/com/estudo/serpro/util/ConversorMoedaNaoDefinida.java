package com.estudo.serpro.util;

public class ConversorMoedaNaoDefinida extends Exception {

	ConversorMoedaNaoDefinida(String message) {
		super(message);
	}
	
}
