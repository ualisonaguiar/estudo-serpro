package com.estudo.serpro.model;

public enum TipoTransacaoEnum {
    DEPOSITO,
    RETIRADA,
    TRANSFERENCIA;
}
