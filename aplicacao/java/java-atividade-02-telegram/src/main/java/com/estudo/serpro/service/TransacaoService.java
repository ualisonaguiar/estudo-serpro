package com.estudo.serpro.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.estudo.serpro.model.MoedaEnum;
import com.estudo.serpro.model.TipoTransacaoEnum;
import com.estudo.serpro.model.Transacao;
import com.estudo.serpro.util.ConversorMoedaNaoDefinida;
import com.estudo.serpro.util.ConversorMoedaUtil;

public class TransacaoService {

	private static final Logger LOGGER = Logger.getLogger(TransacaoService.class.getName());
	private List<Transacao> transacoes = new ArrayList<>();
	private Long id = 1L;

	public Transacao registrar(final Transacao transacao) {
		transacao.setId(generateId());
		transacoes.add(transacao);

		return transacao;
	}

	public List<Transacao> listagem() {
		return transacoes;
	}

	public Transacao findTransacaoId(final Long id) throws TransacaoNaoExisteException {
		Optional<Transacao> findTransacao = transacoes.stream().filter(transacao -> transacao.getId().equals(id))
				.findAny();

		if (!findTransacao.isPresent()) {
			throw new TransacaoNaoExisteException("Transação não localizada");
		}

		return findTransacao.get();
	}

	public List<Transacao> findTransacaoCliente(final Long idCliente) {
		return transacoes.stream()
				.filter(transacao -> transacao.getCliente().getId().equals(idCliente))
				.sorted(Comparator.comparingDouble(Transacao::getValor)).collect(Collectors.toList());
	}

	public List<Transacao> findTransacaoTransacao(final TipoTransacaoEnum tipoTransacaoEnum) {
		return transacoes.stream()
				.filter(transacao -> transacao.getTipoTransacao().equals(tipoTransacaoEnum))
				.sorted(Comparator.comparingDouble(Transacao::getValor)).collect(Collectors.toList());
	}

	public List<Transacao> converterMoeda() {
		List<Transacao> transacoesMoedaConvertida = this.transacoes;
		ConversorMoedaUtil conversor = new ConversorMoedaUtil();

		transacoesMoedaConvertida.stream().forEach(transacao -> {
			try {
				transacao.setValorConvertido(conversor.convertaMoeda(transacao.getMoeda(), transacao.getValor()));
			} catch (ConversorMoedaNaoDefinida e) {
				LOGGER.info(e.getMessage());
			}
		});

		return transacoesMoedaConvertida;
	}

	public Double saldoClienteMoedaConvertida(final Long idCliente, MoedaEnum moedaLocal) {
		List<Transacao> transacoesCliente = findTransacaoCliente(idCliente);
		ConversorMoedaUtil conversor = new ConversorMoedaUtil();
		Double saldo = 0.00;

		if (!transacoesCliente.isEmpty()) {
			for (Transacao transacao : transacoesCliente) {
				try {
					saldo += conversor.convertaMoeda(moedaLocal, transacao.getValor());
				} catch (ConversorMoedaNaoDefinida e) {
					LOGGER.info(e.getMessage());
				}				
			}
		}

		return saldo;
	}
	
	public List<Transacao> identificarTransacaoSuspeita(final Double valor) {
		return transacoes.stream().filter(transacao -> transacao.getValor() >= valor).collect(Collectors.toList());
	}

	private Long generateId() {
		return id++;
	}
}
