package com.estudo.serpro.service;

public class TransacaoNaoExisteException extends Exception {
    
    TransacaoNaoExisteException(final String message) {
        super(message);
    }
}
