package com.estudo.serpro.util;

import java.util.EnumMap;
import java.util.Map;

import com.estudo.serpro.model.MoedaEnum;

public class ConversorMoedaUtil {

	Map<MoedaEnum, Double> valoresMoedas = new EnumMap<>(MoedaEnum.class);

	public ConversorMoedaUtil() {
		carragaValoresMoeda();
	}

	public void carragaValoresMoeda() {
		valoresMoedas.clear();
		valoresMoedas.put(MoedaEnum.BRL, 4.50);
		valoresMoedas.put(MoedaEnum.EUR, 5.75);
		valoresMoedas.put(MoedaEnum.USD, 5.85);
	}

	public Double convertaMoeda(MoedaEnum moeda, Double valor) throws ConversorMoedaNaoDefinida {
		if (!valoresMoedas.containsKey(moeda)) {
			throw new ConversorMoedaNaoDefinida("Valor da moeda não definida");
		}

		return Math.round((valoresMoedas.get(moeda) * valor) * 100.0) / 100.0;
	}
}
