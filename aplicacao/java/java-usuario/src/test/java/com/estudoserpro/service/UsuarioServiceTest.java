package com.estudoserpro.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.estudoserpro.model.Usuario;
import com.estudoserpro.service.UsuarioService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UsuarioServiceTest {

	private UsuarioService service;

	@BeforeEach
	public void setUp() {
		this.service = new UsuarioService();
	}

	@Test
	public void criandoUsuario() {
		Usuario usuario = new Usuario();
		usuario.setNome("Uálison Aguiar");
		usuario.setEmail("ualison.aguiar@gmail.com");
		this.service.salvar(usuario);

		Usuario usuario2 = this.service.pesquisaUsuarioEmail(usuario.getEmail());
		assertNotNull(usuario2.getIdUsuario());
	}

	@Test
	public void atualizarUsuario() {
		Usuario usuario = new Usuario();
		usuario.setNome("João Paulo");
		usuario.setEmail("joaopaulorodrigues@gmail.com");
		this.service.salvar(usuario);

		usuario.setEmail("uafrota@hotmail.com");
		this.service.salvar(usuario);

		Usuario usuario2 = this.service.obterUsuarioId(usuario.getIdUsuario());
		assertEquals(usuario.getIdUsuario(), usuario2.getIdUsuario());
		assertEquals(usuario.getEmail(), usuario2.getEmail());
	}

	@Test
	public void excluirUsuario() {
		Usuario usuario = new Usuario();
		usuario.setNome("Vanda Silva");
		usuario.setEmail("vandasilva@gmail.com");
		this.service.salvar(usuario);
		this.service.excluirUsuario(usuario.getIdUsuario());

		Usuario usuario2 = this.service.obterUsuarioId(usuario.getIdUsuario());
		assertNull(usuario2);
	}

	@Test
	public void excluirUsuarioEmail() {
		Usuario usuario = new Usuario();
		usuario.setNome("Vanessa Silva");
		usuario.setEmail("vanessa@gmail.com");
		this.service.salvar(usuario);

		this.service.excluirUsuario(usuario.getEmail());
		Usuario usuario2 = this.service.obterUsuarioId(usuario.getIdUsuario());
		assertNull(usuario2);		
	}
}
