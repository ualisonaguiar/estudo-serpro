package com.estudoserpro.service;

import java.util.logging.Logger;
import java.util.logging.Level;

import com.estudoserpro.model.Usuario;
import com.estudoserpro.model.UsuarioRepository;

public class UsuarioService {

	private UsuarioRepository repository;
	private static final Logger LOGGER = Logger.getLogger(UsuarioService.class.getName());
	
	public UsuarioService()	 {
		repository = new UsuarioRepository();
	}

	public void salvar(Usuario usuario) {
		if (usuario.getIdUsuario() == null) {
			repository.inserir(usuario);
		} else {
			repository.atualizar(usuario);
		}
	}

	public Usuario pesquisaUsuarioEmail(String email) {
		Usuario usuario = repository.pesquisaUsuarioEmail(email);
		if (usuario == null) {
			LOGGER.log(Level.INFO, "Usuário com e-mail: {0} não encontrado", email);
			return null;
		}
		return usuario;
	}

	public Usuario obterUsuarioId(Long idUsuario) {
		Usuario usuario = repository.obterUsuarioId(idUsuario);
		if (usuario == null) {
			LOGGER.log(Level.INFO, "Usuário com ID: {0} não encontrado", idUsuario);
			return null;
		}
		return usuario;		
	}

	public void excluirUsuario(Long idUsuario) {
		Usuario usuario = obterUsuarioId(idUsuario);
		if (usuario != null) {
			repository.excluirUsuario(usuario.getIdUsuario());
		}
	}

	public void excluirUsuario(String email) {
		Usuario usuario = repository.pesquisaUsuarioEmail(email);
		if (usuario != null) {
			repository.excluirUsuario(usuario.getEmail());
		}
	}
}