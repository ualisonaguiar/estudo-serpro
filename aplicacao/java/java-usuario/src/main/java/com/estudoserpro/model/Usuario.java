package com.estudoserpro.model;

public class Usuario {

	private Long idUsuario;
	private String nome;
	private String email;

	public void setIdUsuario(final Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getIdUsuario() {
		return this.idUsuario;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getEmail() {
		return this.email;
	}
}