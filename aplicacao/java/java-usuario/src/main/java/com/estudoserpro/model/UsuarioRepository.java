package com.estudoserpro.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioRepository {

	private static final Logger LOGGER = Logger.getLogger(UsuarioRepository.class.getName());

	public void inserir(Usuario usuario) {
		PreparedStatement statement = null;
		try {
			Connection conexao = AbstractConexaoDatabase.getConexao();
			statement = conexao.prepareStatement("insert into usuario (nome, email) values (?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, usuario.getNome());
			statement.setString(2, usuario.getEmail());
			statement.executeUpdate();
			
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
                    usuario.setIdUsuario(generatedKeys.getLong(1));
                }
			}
			
		} catch (SQLException sqlError) {
			LOGGER.log(Level.SEVERE, "Erro ao inserir o usuário. Erro: {0}", sqlError);
		} finally {
			AbstractConexaoDatabase.fecharConexao(statement);
		}
	}

	public void atualizar(Usuario usuario) {
		PreparedStatement statement = null;
		try {
			Connection conexao = AbstractConexaoDatabase.getConexao();
			statement = conexao.prepareStatement("update usuario set nome = ?, email = ? where id = ?");
			statement.setString(1, usuario.getNome());
			statement.setString(2, usuario.getEmail());
			statement.setLong(3, usuario.getIdUsuario());
			statement.execute();

		} catch (SQLException sqlError) {
			LOGGER.log(Level.SEVERE, "Erro ao atualizar o usuário. Erro: {0}", sqlError);
		} finally {
			AbstractConexaoDatabase.fecharConexao(statement);
		}
	}

	public Usuario pesquisaUsuarioEmail(String email) {
		PreparedStatement statement = null;
		Usuario usuario = null;
		try {
			Connection conexao = AbstractConexaoDatabase.getConexao();
			statement = conexao.prepareStatement("select * from usuario where email = ?");
			statement.setString(1, email);
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				return populateEntity(result);
			}

		} catch (SQLException sqlError) {
			LOGGER.log(Level.SEVERE, "Erro ao pesquisar usuário por e-mail. Erro: {0}", sqlError.getMessage());
		} finally {
			AbstractConexaoDatabase.fecharConexao(statement);
		}

		return usuario;
	}

	public Usuario obterUsuarioId(Long idUsuario) {
		PreparedStatement statement = null;
		Usuario usuario = null;
		try {
			Connection conexao = AbstractConexaoDatabase.getConexao();
			statement = conexao.prepareStatement("select * from usuario where id = ?");
			statement.setLong(1, idUsuario);
			ResultSet result = statement.executeQuery();

			while(result.next()) {
				return populateEntity(result);
			}

		} catch (SQLException sqlError) {
			LOGGER.log(Level.SEVERE, "Erro ao pesquisar usuário por ID. Erro: {0}", sqlError.getMessage());
		} finally {
			AbstractConexaoDatabase.fecharConexao(statement);
		}
		return usuario;
	}

	public void excluirUsuario(Long idUsuario) {
		PreparedStatement statement = null;
		try {
			Connection conexao = AbstractConexaoDatabase.getConexao();
			statement = conexao.prepareStatement("delete from usuario where id = ?");
			statement.setLong(1, idUsuario);
			statement.execute();

		} catch (SQLException sqlError) {
			LOGGER.log(Level.SEVERE, "Erro ao excluir usuário por ID. Erro: {0}", sqlError.getMessage());
		} finally {
			AbstractConexaoDatabase.fecharConexao(statement);
		}
	}

	public void excluirUsuario(String email) {
		PreparedStatement statement = null;
		try {
			Connection conexao = AbstractConexaoDatabase.getConexao();
			statement = conexao.prepareStatement("delete from usuario where email = ?");
			statement.setString(1, email);
			statement.execute();


		} catch (SQLException sqlError) {
			LOGGER.log(Level.SEVERE, "Erro ao excluir usuário pelo e-mail. Erro: {0}", sqlError.getMessage());
		} finally {
			AbstractConexaoDatabase.fecharConexao(statement);
		}
	}

	protected Usuario populateEntity(ResultSet result) throws SQLException {
		Usuario usuario = new Usuario();
		usuario.setIdUsuario(result.getLong("id"));
		usuario.setNome(result.getString("nome"));
		usuario.setEmail(result.getString("email"));

		return usuario;
	}
}