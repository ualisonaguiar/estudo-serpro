package com.estudoserpro.model;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AbstractConexaoDatabase {

    private static Connection conexao;

    private static final Logger LOGGER = Logger.getLogger(AbstractConexaoDatabase.class.getName());

    private AbstractConexaoDatabase() {
        // Construtor privado para evitar instâncias externas
    }

    public static synchronized Connection getConexao() throws SQLException {
        conexao = DriverManager.getConnection("jdbc:mysql://10.67.40.244/ten_ualison", "root", "DCT@_bd&244");
        
        return conexao;
    }

    protected static void fecharConexao() {
        if (conexao != null) {
            try {
                conexao.close();
            } catch (SQLException sqlError) {
                LOGGER.log(Level.SEVERE, "Erro ao fechar a conexão. Erro: {0}", sqlError);
            }
        }
    }

    protected static void fecharConexao(PreparedStatement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException sqlError) {
                LOGGER.log(Level.SEVERE, "Erro ao fechar o PreparedStatement. Erro: {0}", sqlError);
            }
        }

        fecharConexao();
    }
}
