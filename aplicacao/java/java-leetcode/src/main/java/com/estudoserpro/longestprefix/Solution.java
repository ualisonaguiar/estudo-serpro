package com.estudoserpro.longestprefix;

import java.util.ArrayList;

public class Solution {

	public String longestCommonPrefix(String[] strs) {
		
		if (strs == null || strs.length == 0) {
			return "";
		}
		
		String prefixoTest = strs[0];
		for (String str: strs) {
			if (str.length() < prefixoTest.length()) {
				prefixoTest = str;
			}
		}
		
		for (int i = 0; i < prefixoTest.length(); i++) {
			Character letra = prefixoTest.charAt(i);
			
			for (String str : strs) {
				if (letra != str.charAt(i)) {
					return prefixoTest.substring(0, i);
				}
			}
		}
		
		return prefixoTest;
	}

}
