package com.estudoserpro.validparentheses;

class Solution {
	
    public boolean isValid(String s) {
    	int tamanhoAtual;
    	do {
    		tamanhoAtual = s.length();
    		
    		s = s.replace("()", "");
    		s = s.replace("[]", "");
    		s = s.replace("{}", "");
    		
    	} while(tamanhoAtual != s.length());
        
    	return s.length() == 0;
    }
}