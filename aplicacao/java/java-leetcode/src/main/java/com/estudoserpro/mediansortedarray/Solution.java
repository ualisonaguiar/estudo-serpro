package com.estudoserpro.mediansortedarray;

import java.util.Arrays;

/**
 * Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
 * 
 * @author ualison
 *
 */

public class Solution {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
    	int tamanho = nums1.length + nums2.length;
    	int[] combinacao = new int[tamanho];
    	
    	System.arraycopy(nums1, 0, combinacao, 0, nums1.length);
    	System.arraycopy(nums2, 0, combinacao, nums1.length, nums2.length);
    	
    	Arrays.sort(combinacao);
    	
    	if (tamanho % 2 == 0) {
    		int mid1 = combinacao[tamanho / 2 - 1];
    		int mid2 = combinacao[tamanho / 2];
    		
    		return (double) (mid1 + mid2) / 2;    		
    	} else {
    		return combinacao[tamanho / 2];
    	}
    }	
	
}
