package com.estudoserpro.roman;

import java.util.HashMap;

public class Solution {

	public int romanToInt(String s) {

		HashMap<Character, Integer> mapRomano = new HashMap<Character, Integer>();
		mapRomano.put('I', 1);
		mapRomano.put('V', 5);
		mapRomano.put('X', 10);
		mapRomano.put('L', 50);
		mapRomano.put('C', 100);
		mapRomano.put('D', 500);
		mapRomano.put('M', 1000);
		
		int resultado = 0;
		int numeroAnterior = 0;
		
		for (int i = s.length() - 1; i >= 0; i--) {
			Character romano = s.charAt(i);
			int numeroRomano = mapRomano.get(romano);
			
			if (numeroRomano < numeroAnterior) {
				resultado -= numeroRomano;
			} else {
				resultado += numeroRomano;
			}
			
			numeroAnterior = numeroRomano;
		}
		
		return resultado;
	}
}
