package com.estudoserpro.sametree;

public class Solution {
	
	public boolean isSameTree(int[] p, int[] q) {
		if (p.length == p.length) {
			boolean isSame = true;
			
			for (int i = 0; i < p.length; i++) {
				if (p[i] != q[0]) {
					isSame = false;
					break;
				}
			}
			
			return isSame;
		}
		
		return false;
	}
}
