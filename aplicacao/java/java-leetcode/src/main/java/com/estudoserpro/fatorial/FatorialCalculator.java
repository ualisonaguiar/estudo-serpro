package com.estudoserpro.fatorial;

public class FatorialCalculator {
	
	public int calcularFatorial(int n) {
		if (n == 0) {
			return 1;
		} else {
			return n * calcularFatorial(n - 1);
		}
	}
}
