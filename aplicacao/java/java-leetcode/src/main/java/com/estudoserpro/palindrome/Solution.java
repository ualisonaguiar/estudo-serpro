package com.estudoserpro.palindrome;

public class Solution {

	public boolean isPalindrome(int x) {
		String numeroString = Integer.toString(x);
		String reverso = new StringBuilder(numeroString).reverse().toString();
		return numeroString.equals(reverso);
	}

	public boolean isPalindrome(String palavra) {
		String reverso = new StringBuilder(palavra).reverse().toString();
		return palavra.equals(reverso);
	}

	public boolean isPalindrome2(int x) {

		String numeroString = Integer.toString(x);
		StringBuilder reverso = new StringBuilder(numeroString.length());

		for (int i = numeroString.length() - 1; i >= 0; i--) {
			Character numero = numeroString.charAt(i);
			reverso.append(numero);
		}

		return numeroString.equals(reverso.toString());
	}

	public static boolean isPalindrome3(int num) {
		int originalNumber = num;
		int reversedNumber = 0;

		while (num > 0) {
			int digit = num % 10;
			reversedNumber = reversedNumber * 10 + digit;
			num /= 10;
		}

		return originalNumber == reversedNumber;
	}
}
