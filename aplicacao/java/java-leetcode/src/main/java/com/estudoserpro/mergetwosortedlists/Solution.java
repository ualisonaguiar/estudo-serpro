package com.estudoserpro.mergetwosortedlists;

import java.util.ArrayList;
import java.util.Collections;

public class Solution {

    public ArrayList<Integer> mergeTwoLists(int[] list1, int[] list2) {
        
    	ArrayList<Integer> listNumber = new ArrayList<>();
    	
    	for (int i : list1) {
    		listNumber.add(i);
    	}
    	
    	for (int i : list2) {
    		listNumber.add(i);
    	}
    	
    	Collections.sort(listNumber);
    	
    	return listNumber;
    }
	
}
