package com.estudoserpro.removeduplicatesfromsortedlist;

import java.util.HashSet;

public class Solution {

	public int[] removeDuplicates(int[] nums) {
		
		HashSet<Integer> numeroNaoDuplicados = new HashSet<>();
		for (int num : nums) {
			numeroNaoDuplicados.add(num);
		}
		
		int[] numsSemDuplicado = new int[numeroNaoDuplicados.size()];
		int index = 0;
		for (int num : numeroNaoDuplicados) {
			numsSemDuplicado[index] = num;
			index++;
		}
				
		return numsSemDuplicado;
	}

}
