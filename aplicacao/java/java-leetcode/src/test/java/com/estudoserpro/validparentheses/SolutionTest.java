package com.estudoserpro.validparentheses;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class SolutionTest extends TestCase {

	@Test
	public void testExempla01() {
		String s = "()";
		assertTrue(new Solution().isValid(s));
	}

	@Test
	public void testExempla02() {
		String s = "[]";
		assertTrue(new Solution().isValid(s));
	}

	@Test
	public void testExempla03() {
		String s = "{}";
		assertTrue(new Solution().isValid(s));
	}
	
	@Test
	public void testExempla04() {
		String s = "()[]{}";
		assertTrue(new Solution().isValid(s));
	}	
	
	@Test
	public void testExempla05() {
		String s = "(]";
		assertFalse(new Solution().isValid(s));
	}
	
	@Test
	public void testExempla06() {
		String s = "{[()]}";
		assertTrue(new Solution().isValid(s));
	}	
}
