package com.estudoserpro.mergetwosortedlists;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class SolutionTest extends TestCase {

	@Test
	public void testeExample01() {
		
		Integer esperado[] = new Integer[] {1,1,2,3,4,4};
		int[] list1 = new int[] {1,2,4};
		int[] list2 = new int[] {1,3,4};
		
		assertEquals(Arrays.asList(esperado),new Solution().mergeTwoLists(list1, list2));
	}
	
	@Test
	public void testeExample02() {
		
		Integer esperado[] = new Integer[] {};
		int[] list1 = new int[] {};
		int[] list2 = new int[] {};
		
		assertEquals(Arrays.asList(esperado),new Solution().mergeTwoLists(list1, list2));
	}
	
	@Test
	public void testeExample03() {
		
		Integer esperado[] = new Integer[] {0};
		int[] list1 = new int[] {};
		int[] list2 = new int[] {0};
		
		assertEquals(Arrays.asList(esperado),new Solution().mergeTwoLists(list1, list2));
	}		
}
