package com.estudoserpro.roman;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class SolutionTest extends TestCase {

//	@Test
//	public void testExemplo01() {
//		assertEquals(3, new Solution().romanToInt("III"));
//	}
//
//	@Test
//	public void testExemplo02() {
//		assertEquals(58, new Solution().romanToInt("LVIII"));
//	}

	@Test
	public void testExemplo03() {
		assertEquals(1994, new Solution().romanToInt("MCMXCIV"));
	}

//	@Test
//	public void testExemplo04() {
//		assertEquals(4, new Solution().romanToInt("IV"));
//	}
}
