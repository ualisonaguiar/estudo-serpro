package com.estudoserpro.longestprefix;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class SolutionTest extends TestCase {

	@Test
	public void testExemplo01() {
		String[] palavras = { "flower", "flow", "flight" };
		assertEquals("fl", new Solution().longestCommonPrefix(palavras));
	}	

	@Test
	public void testExemplo02() {
		String[] palavras = { "fundacao", "funil", "fundatec" };
		assertEquals("fun", new Solution().longestCommonPrefix(palavras));
	}

	@Test
	public void testExemplo03() {
		String[] palavras = { "fundacao", "funil", "fundatec", "vagas" };
		assertEquals("", new Solution().longestCommonPrefix(palavras));
	}

	@Test
	public void testExemplo04() {
		String[] palavras = { "dog","racecar","car" };
		assertEquals("", new Solution().longestCommonPrefix(palavras));
	}
	
	@Test
	public void testExemplo05() {
		String[] palavras = { "a" };
		assertEquals("a", new Solution().longestCommonPrefix(palavras));
	}		
}
