package com.estudoserpro.sametree;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class SolutionTest {

	@Test
	public void isSameTreeCase1() {
		
		int[] p = new int[3];
		p[0] = 1;
		p[1] = 2;
		p[2] = 3;
		
		int[] q = new int[3];
		q[0] = 1;
		q[1] = 2;
		q[2] = 3;
		
		Solution solution = new Solution();
		assertTrue(solution.isSameTree(p, q));
		
	}
	
	@Test
	public void isSameTreeCase2() {
		
		int[] p = new int[2];
		p[0] = 1;
		p[1] = 2;
		
		int[] q = new int[3];
		q[0] = 1;
		q[1] = 2;
		q[2] = 3;
		
		Solution solution = new Solution();
		assertFalse(solution.isSameTree(p, q));
		
	}	
}
