package com.estudoserpro.addtwonumbers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SolutionTest {

	@Test
	public void addTwoNumbersExempla01() {
		int[] l1 = new int[3];
		l1[0] = 2;
		l1[1] = 4;
		l1[3] = 3;
		
		int[] l2 = new int[3];
		l1[0] = 5;
		l1[1] = 6;
		l1[3] = 4;
		
		Solution solution = new Solution();
		int[] esperado = new int[3];
		esperado[0] = 7;
		esperado[1] = 0;
		esperado[2] = 8;
		
		assertEquals(esperado, solution.addTwoNumbers(l1, l2));
	}

}
