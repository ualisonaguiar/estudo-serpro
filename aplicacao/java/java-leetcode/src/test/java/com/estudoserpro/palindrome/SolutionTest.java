package com.estudoserpro.palindrome;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

public class SolutionTest extends TestCase {

	@Test
	public void testExemplo01() {
		Solution solution = new Solution();
		assertTrue(solution.isPalindrome(121));
	}

	@Test
	public void testExemplo02() {
		Solution solution = new Solution();
		assertTrue(solution.isPalindrome("ANA"));
	}

	@Test
	public void testExemplo03() {
		Solution solution = new Solution();
		assertFalse(solution.isPalindrome("UALISON"));
	}
	
	@Test
	public void testExemplo04() {
		Solution solution = new Solution();
		assertFalse(solution.isPalindrome("UALISON"));
	}
	
	@Test
	public void testExemplo05() {
		Solution solution = new Solution();
		assertFalse(solution.isPalindrome2(1654321));
	}

	@Test
	public void testExemplo06() {
		Solution solution = new Solution();
		assertTrue(solution.isPalindrome2(121));
	}
	
	@Test
	public void testExemplo07() {
		Solution solution = new Solution();
		assertTrue(solution.isPalindrome3(121));
	}		
}
