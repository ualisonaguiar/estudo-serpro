package com.estudoserpro.removeduplicatesfromsortedlist;

import org.junit.jupiter.api.Test;

import com.estudoserpro.removeduplicatesfromsortedlist.Solution;

import junit.framework.TestCase;

public class SolutionTest extends TestCase {
	
	@Test
	public void testExempla01() {
		
		int[] nums = new int[3];
		nums[0] = 1;
		nums[1] = 1;
		nums[2] = 2;
		
		Solution solution = new Solution();
		int[]nums2 = solution.removeDuplicates(nums);
		assertEquals(2, nums2.length);
	}
	
	@Test
	public void testExempla02() {
		
		int[] nums = new int[5];
		nums[0] = 1;
		nums[1] = 1;
		nums[2] = 2;
		nums[3] = 3;
		nums[4] = 3;
		
		Solution solution = new Solution();
		int[]nums2 = solution.removeDuplicates(nums);
		assertEquals(3, nums2.length);
	}	
	
}
