package com.estudoserpro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.estudoserpro.service.CalculaDisntancia;

public class CalculaDisntanciaTest {
	
	private CalculaDisntancia distanciaService;
	
	@BeforeEach
	public void setUp() {
		distanciaService = new CalculaDisntancia();
	}
	
	@Test
	public void testCalculaDistanciaExemplo01() {
		
		distanciaService.setLatitudePontoA("500359º N", "0054253º W");
		distanciaService.setLatitudePontoB("58 38 38º N", "003 04 12ºW");
		
		assertEquals(949926.5845073111d, distanciaService.calculaDistancia());
	}
	
	@Test
	public void testCalculaDistanciaExemplo02() {
		
		distanciaService.setLatitudePontoA("40.7128° N", "-74.0060° W");
		distanciaService.setLatitudePontoB("-33.8688° S", "151.2093° E");
		assertEquals(1.404547433527868E7d, distanciaService.calculaDistancia());
	}	
}
