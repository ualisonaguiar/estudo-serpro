package com.estudoserpro;

public class TipoPonto {
	
	private CoordenadaEnum coordenada;
	private Double ponto;
	private String posicao;
	
	public CoordenadaEnum getCoorenda() {
		return coordenada;
	}
	
	public void setCoorenda(CoordenadaEnum coordenada) {
		this.coordenada = coordenada;
	}
	
	public Double getPonto() {
		return ponto;
	}
	
	public Double getPontoRadians() {
		return Math.toRadians(ponto);
	}	
	
	public void setPonto(Double ponto) {
		this.ponto = ponto;
	}
	
	public String getPosicao() {
		return posicao;
	}
	
	public void setPosicao(String posicao) {
		this.posicao = posicao;
	}
}
