package com.estudoserpro.dto;

import java.util.ArrayList;
import java.util.List;

import com.estudoserpro.TipoPonto;

public class CoordenadaGeograficaDTO {
	
	private List<TipoPonto> pontoA = new ArrayList<>();
	private List<TipoPonto> pontoB = new ArrayList<>();
	
	public List<TipoPonto> getPontoA() {
		return pontoA;
	}
	
	public void setPontoA(TipoPonto pontoA) {
		this.pontoA.add(pontoA);
	}
	
	public List<TipoPonto> getPontoB() {
		return pontoB;
	}
	
	public void setPontoB(TipoPonto pontoB) {
		this.pontoB.add(pontoB);
	}	
	
}
