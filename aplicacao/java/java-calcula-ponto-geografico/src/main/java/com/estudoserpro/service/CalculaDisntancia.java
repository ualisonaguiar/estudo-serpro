package com.estudoserpro.service;

import com.estudoserpro.CoordenadaEnum;
import com.estudoserpro.TipoPonto;
import com.estudoserpro.dto.CoordenadaGeograficaDTO;

public class CalculaDisntancia {

	private CoordenadaGeograficaDTO coordenadas;
	private static String SEPARA_COORDENADA = "º";
	private static String OUTRA_COORDENADA = "°";
	private static double RAIO_TERRA = 6371000;

	public CalculaDisntancia() {
		this.coordenadas = new CoordenadaGeograficaDTO();
	}

	public void setLatitudePontoA(String latitude, String lontitude) {
		coordenadas.setPontoA(preparePontoLatitudeDTO(latitude, CoordenadaEnum.LATITUDE));
		coordenadas.setPontoA(preparePontoLatitudeDTO(lontitude, CoordenadaEnum.LONGITUDE));
	}

	public void setLatitudePontoB(String latitude, String lontitude) {
		coordenadas.setPontoB(preparePontoLatitudeDTO(latitude, CoordenadaEnum.LATITUDE));
		coordenadas.setPontoB(preparePontoLatitudeDTO(lontitude, CoordenadaEnum.LONGITUDE));
	}

	public double calculaDistancia() {
		return calculaFormulaHaversine();
	}

	protected double calculaFormulaHaversine() {
		Double tangente = calcularSenCoordenada(coordenadas.getPontoA().get(0).getPontoRadians(),
				coordenadas.getPontoB().get(0).getPontoRadians());
		tangente += Math.cos(coordenadas.getPontoA().get(0).getPontoRadians());
		tangente *= Math.cos(coordenadas.getPontoB().get(0).getPontoRadians());
		tangente *= calcularSenCoordenada(coordenadas.getPontoA().get(1).getPontoRadians(),
				coordenadas.getPontoB().get(1).getPontoRadians());

		double arcoTangente = 2 * Math.atan2(Math.sqrt(tangente), Math.sqrt(1 - tangente));
		return RAIO_TERRA * arcoTangente;
	}

	protected Double calcularSenCoordenada(Double coordenadaARadiano, Double coordenadaBRadiano) {
		return Math.pow(Math.sin((coordenadaBRadiano - coordenadaARadiano) / 2), 2);
	}

	protected TipoPonto preparePontoLatitudeDTO(String coordenada, CoordenadaEnum tipoCoordenada) {
		TipoPonto pontoLatitude = new TipoPonto();
		pontoLatitude.setCoorenda(tipoCoordenada);
		pontoLatitude.setPonto(trataValorRetornaPonto(coordenada));
		pontoLatitude.setPosicao(trataValorRetornaPosicao(coordenada));
		return pontoLatitude;
	}

	protected Double trataValorRetornaPonto(String ponto) {
		String[] detalhePonto = ponto.replace(SEPARA_COORDENADA, OUTRA_COORDENADA)
				.split(OUTRA_COORDENADA);
		return Double.parseDouble(detalhePonto[0].replace(" ", ""));
	}

	protected String trataValorRetornaPosicao(String ponto) {
		String[] detalhePonto = ponto.replace(SEPARA_COORDENADA, OUTRA_COORDENADA)
				.split(OUTRA_COORDENADA);
		return detalhePonto[1].toLowerCase().trim();
	}
}
