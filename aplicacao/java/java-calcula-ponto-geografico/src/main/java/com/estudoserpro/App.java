package com.estudoserpro;

import java.util.Scanner;

import com.estudoserpro.service.CalculaDisntancia;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		displayMessage("Cálculo de distância usando dois pontos geográficos");
		displayMessage("1º Coordenada:");
		displayMessage("1º Coordenada - Informe a Latitude do primeiro ponto: ");
		String pontoLatitudeA = scanner.next();
		
		displayMessage("1º Coordenada - Informe a Lontitude do primeiro ponto: ");
		String pontoLongitudeA = scanner.next();

		displayMessage("2º Coordenada - Informe a Latitude do primeiro ponto: ");
		String pontoLatitudeB = scanner.next();
		
		displayMessage("2º Coordenada - Informe a Lontitude do primeiro ponto: ");
		String pontoLongitudeB = scanner.next();

		CalculaDisntancia calculaDistancia = new CalculaDisntancia();
		calculaDistancia.setLatitudePontoA(pontoLatitudeA, pontoLongitudeA);
		calculaDistancia.setLatitudePontoB(pontoLatitudeB, pontoLongitudeB);
		Double distancia = calculaDistancia.calculaDistancia();

		System.out.println(String.format("Distância é: %s", distancia));
	}

	private static void displayMessage(String message) {
		System.out.println(message);
	}
}
