package com.estudoserpro;

public enum CoordenadaEnum {
	
	LONGITUDE(1), LATITUDE(2);

	private Integer coordenada;

	CoordenadaEnum(Integer coordenada) {
		this.coordenada = coordenada;
	}
}
