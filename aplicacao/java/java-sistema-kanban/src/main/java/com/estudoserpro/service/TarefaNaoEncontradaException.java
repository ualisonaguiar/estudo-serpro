package com.estudoserpro.service;

public class TarefaNaoEncontradaException extends Exception {
    
    TarefaNaoEncontradaException(String messagem) {
        super(messagem);
    }
}
