package com.estudoserpro.model;

public enum StatusTarefaEnum {
    PARA_FAZER(1L),
    TRABALHANDO(2L),
    ESPERANDO(3L),
    FEITO(4L);

    private Long statusTarefa;

    StatusTarefaEnum(final Long statusTarefa) {
        this.statusTarefa = statusTarefa;
    }

    public Long getStatusTarefa() {
        return statusTarefa;
    }

    public static StatusTarefaEnum getByValue(final Long statusTarefa) {
        for (StatusTarefaEnum status : StatusTarefaEnum.values()) {
            if (status.getStatusTarefa().equals(statusTarefa)) {
                return status;
            }
        }
        return null;
    }
}
