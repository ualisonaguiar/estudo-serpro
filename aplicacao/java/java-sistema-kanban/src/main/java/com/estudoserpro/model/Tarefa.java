package com.estudoserpro.model;

public class Tarefa {
    
    private Long id;
    private String nome;
    private StatusTarefaEnum statusTarefa;

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setStatusTarefa(final StatusTarefaEnum statusTarefa) {
        this.statusTarefa = statusTarefa;
    }

    public StatusTarefaEnum getStatusTarefa() {
        return this.statusTarefa;
    }    
}
