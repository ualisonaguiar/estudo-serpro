package com.estudoserpro.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class TarefaDAO extends AbstractConexaoDatabase {

    private static final Logger LOGGER = Logger.getLogger(TarefaDAO.class.getName());

    public void inserir(Tarefa tarefa) {
        PreparedStatement statement = null;
        try {
            Connection conexao = getConexao();
            statement = conexao.prepareStatement("insert into tarefa (nome, status_tarefa) values (?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, tarefa.getNome());
            statement.setLong(2, tarefa.getStatusTarefa().getStatusTarefa());
            statement.executeUpdate();

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    tarefa.setId(generatedKeys.getLong(1));
                }
            }
        } catch (SQLException sqlError) {
            LOGGER.severe("Falha ao inserir uma tarefa: E".concat(sqlError.getMessage()));
        } finally {
            fecharConexao(statement);
        }
    }

    public void atualizar(Tarefa tarefa) {
        PreparedStatement statement = null;
        try {
            Connection conexao = getConexao();
            statement = conexao.prepareStatement("update tarefa set nome = ?, status_tarefa = ? where id = ?");
            statement.setString(1, tarefa.getNome());
            statement.setLong(2, tarefa.getStatusTarefa().getStatusTarefa());
            statement.setLong(3, tarefa.getId());
            statement.executeUpdate();
        } catch (SQLException sqlError) {
            LOGGER.severe("Falha ao atualizar uma tarefa: Erro".concat(sqlError.getMessage()));
        } finally {
            fecharConexao(statement);
        }
    }

    public void excluir(final Long id) {
        PreparedStatement statement = null;
        try {
            Connection conexao = getConexao();
            statement = conexao.prepareStatement("delete from tarefa where id = ?");
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException sqlError) {
            LOGGER.severe("Falha ao excluir uma tarefa: Erro".concat(sqlError.getMessage()));
        } finally {
            fecharConexao(statement);
        }
    }

    public List<Tarefa> listagem() {
        PreparedStatement statement = null;
        List<Tarefa> tarefas = new ArrayList<>();
        try {
            Connection conexao = getConexao();
            statement = conexao.prepareStatement("select * from tarefa order by id asc");
            statement.execute();
            ResultSet results = statement.getResultSet();

            while (results.next()) {
                tarefas.add(populateEntityResult(results));
            }

        } catch (SQLException sqlError) {
            LOGGER.severe("Falha ao listar as tarefas: Erro".concat(sqlError.getMessage()));
        } finally {
            fecharConexao(statement);
        }
        return tarefas;
    }

    public List<Tarefa> listagemPorStatus(StatusTarefaEnum status) {
        PreparedStatement statement = null;
        List<Tarefa> tarefas = new ArrayList<>();
        try {
            Connection conexao = getConexao();
            statement = conexao.prepareStatement("select * from tarefa where status_tarefa = ? order by id asc");
            statement.setLong(1, status.getStatusTarefa());
            statement.execute();
            ResultSet results = statement.getResultSet();

            while (results.next()) {
                tarefas.add(populateEntityResult(results));
            }

        } catch (SQLException sqlError) {
            LOGGER.severe("Falha ao listar por status as tarefas: Erro".concat(sqlError.getMessage()));
        } finally {
            fecharConexao(statement);
        }
        return tarefas;
    }

    public Tarefa obterDetalhes(final Long id) {
        PreparedStatement statement = null;
        try {
            Connection conexao = getConexao();
            statement = conexao.prepareStatement("select * from tarefa where id = ?");
            statement.setLong(1, id);
            statement.execute();
            ResultSet results = statement.getResultSet();
            while (results.next()) {
                return populateEntityResult(results);
            }
        } catch (SQLException sqlError) {
            LOGGER.severe("Falha ao obter detalhes da tarefa: Erro".concat(sqlError.getMessage()));
        } finally {
            fecharConexao(statement);
        }
        return null;
    }

    protected Tarefa populateEntityResult(ResultSet result) throws SQLException {
        Tarefa tarefa = new Tarefa();
        tarefa.setId(result.getLong(1));
        tarefa.setNome(result.getString(2));
        tarefa.setStatusTarefa(StatusTarefaEnum.getByValue(result.getLong(3)));

        return tarefa;
    }

}
