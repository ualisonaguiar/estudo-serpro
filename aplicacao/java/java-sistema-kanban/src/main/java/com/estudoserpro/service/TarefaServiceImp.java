package com.estudoserpro.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.estudoserpro.model.StatusTarefaEnum;
import com.estudoserpro.model.Tarefa;
import com.estudoserpro.model.TarefaDAO;

public class TarefaServiceImp implements TarefaService {

    private TarefaDAO repository;

    private HashMap<StatusTarefaEnum, StatusTarefaEnum> fluxoStatus = new HashMap<>();

    public TarefaServiceImp() {
        repository = new TarefaDAO();
        definirFluxo();
    }

    public void salvar(Tarefa tarefa) {
        if (tarefa.getId() == null) {
            tarefa.setStatusTarefa(StatusTarefaEnum.PARA_FAZER);
            repository.inserir(tarefa);
        } else {
            repository.atualizar(tarefa);
        }
    }

    public Tarefa obterDetalhes(final Long id) throws TarefaNaoEncontradaException {
        Tarefa tarefa = repository.obterDetalhes(id);
        if (tarefa == null) {
            throw new TarefaNaoEncontradaException(String.format("Tarefa de ID: %s não encontrado", id));
        }
        return tarefa;
    }

    public void excluir(Tarefa tarefa) throws TarefaNaoEncontradaException {
        obterDetalhes(tarefa.getId());
        repository.excluir(tarefa.getId());
    }

    public List<Tarefa> listagem() {
        return repository.listagem();
    }

    public List<Tarefa> listarSituacao(StatusTarefaEnum statusTarefa) {
        return repository.listagemPorStatus(statusTarefa);
    }

    public void progedirStatusTarefa(Tarefa tarefa) {
        if (!tarefa.getStatusTarefa().equals(StatusTarefaEnum.FEITO)) {
            tarefa.setStatusTarefa(fluxoStatus.get(tarefa.getStatusTarefa()));
            salvar(tarefa);
        }        
    }

    public void regredirStatusTarefa(Tarefa tarefa) {
        if (!tarefa.getStatusTarefa().equals(StatusTarefaEnum.PARA_FAZER)) {
            for (Map.Entry<StatusTarefaEnum, StatusTarefaEnum> entry : fluxoStatus.entrySet()) {
                if (entry.getValue().equals(tarefa.getStatusTarefa())) {
                    tarefa.setStatusTarefa(entry.getKey());
                    salvar(tarefa);
                }
            }
        }
    }

    protected void definirFluxo() {
        fluxoStatus.put(StatusTarefaEnum.PARA_FAZER, StatusTarefaEnum.TRABALHANDO);
        fluxoStatus.put(StatusTarefaEnum.TRABALHANDO, StatusTarefaEnum.ESPERANDO);
        fluxoStatus.put(StatusTarefaEnum.ESPERANDO, StatusTarefaEnum.FEITO);
    }

}
