package com.estudoserpro.model;

import java.util.logging.Logger;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;

public class AbstractConexaoDatabase {

    private Connection conexao;
    private static final Logger LOGGER = Logger.getLogger(AbstractConexaoDatabase.class.getName());

    protected Connection getConexao() {

        try {
            conexao = DriverManager.getConnection(
                    "jdbc:mysql://10.67.40.244/ten_ualison", "root", "DCT@_bd&244");
        } catch (SQLException e) {
            LOGGER.severe("Falha ao abrir conexão com o banco de dados. Erro: ".concat(e.getMessage()));
        }
        return conexao;
    }

    protected void fecharConexao() {
        if (conexao != null) {
            try {
                conexao.close();
            } catch (SQLException e) {
                LOGGER.severe("Falha ao fechar conexão com o banco de dados. Erro: ".concat(e.getMessage()));
            }
        }
    }

    protected void fecharConexao(PreparedStatement statement) {
        if (statement != null) {
            try {
                statement.close();
                fecharConexao();
            } catch (SQLException e) {
                LOGGER.severe("Falha ao fechar PreparedStatement com o banco de dados. Erro: ".concat(e.getMessage()));
            }
        }
    }
}
