package com.estudoserpro.service;

import java.util.List;

import com.estudoserpro.model.StatusTarefaEnum;
import com.estudoserpro.model.Tarefa;

public interface TarefaService {
    
    public void salvar(Tarefa tarefa);

    public void excluir(Tarefa tarefa) throws TarefaNaoEncontradaException;

    public Tarefa obterDetalhes(final Long id) throws TarefaNaoEncontradaException;

    public List<Tarefa> listagem();

    public List<Tarefa> listarSituacao(StatusTarefaEnum statusTarefa);

    public void progedirStatusTarefa(Tarefa tarefa);

    public void regredirStatusTarefa(Tarefa tarefa);
}
