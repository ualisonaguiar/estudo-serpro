package com.estudoserpro.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.estudoserpro.model.StatusTarefaEnum;
import com.estudoserpro.model.Tarefa;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TarefaServiceTest {

    private TarefaService service;

    @BeforeEach
    public void setUp() throws TarefaNaoEncontradaException {
        service = new TarefaServiceImp();
        apagarTodasTarefas();
    }

    @Test
    public void cadastrarTarefaStatusParaFazer() throws TarefaNaoEncontradaException {
        Tarefa tarefa = new Tarefa();
        tarefa.setNome("Definir identidade visual");
        service.salvar(tarefa);
        assertNotNull(tarefa.getId());

        Tarefa tarefa2 = service.obterDetalhes(tarefa.getId());
        assertEquals(tarefa.getId(), tarefa2.getId());
        assertEquals(tarefa.getNome(), tarefa2.getNome());
        assertEquals(StatusTarefaEnum.PARA_FAZER, tarefa2.getStatusTarefa());
    }

    @Test
    public void listagemTarefaStatus() {
        List<Long> listaIds = new ArrayList<>();

        Tarefa tarefa = new Tarefa();
        tarefa.setNome("Definir Identidade Visual");
        service.salvar(tarefa);
        listaIds.add(tarefa.getId());

        tarefa.setId(null);
        tarefa.setNome("Definir SGDB");
        service.salvar(tarefa);
        listaIds.add(tarefa.getId());

        tarefa.setId(null);
        tarefa.setNome("Implementar login");
        service.salvar(tarefa);
        listaIds.add(tarefa.getId());

        tarefa.setId(null);
        tarefa.setNome("Implementar cadastro");
        service.salvar(tarefa);
        listaIds.add(tarefa.getId());

        List<Tarefa> tarefas = service.listagem();
        for (Long idTarefa : listaIds) {
            Boolean tarefaEncontrada = tarefas.stream().anyMatch(tarefaDb -> tarefaDb.getId().equals(idTarefa));
            assertTrue(tarefaEncontrada);
        }
    }

    @Test
    public void listarTarefaCategoria() throws TarefaNaoEncontradaException {
        Tarefa tarefa = new Tarefa();
        tarefa.setNome("Definir Identidade Visual");
        service.salvar(tarefa);

        tarefa.setId(null);
        tarefa.setNome("Definir SGDB");
        service.salvar(tarefa);

        List<Tarefa> tarefasCategoriaParaFazer = service.listarSituacao(StatusTarefaEnum.PARA_FAZER);
        assertEquals(2, tarefasCategoriaParaFazer.size());
    }

    @Test
    public void progredirTarefa() throws TarefaNaoEncontradaException {
        Tarefa tarefa = new Tarefa();
        tarefa.setNome("Definir identidade visual");
        service.salvar(tarefa);

        List<Tarefa> tarefaCategoria = service.listarSituacao(StatusTarefaEnum.PARA_FAZER);
        assertEquals(1, tarefaCategoria.size());
        service.progedirStatusTarefa(tarefa);

        tarefaCategoria = service.listarSituacao(StatusTarefaEnum.TRABALHANDO);
        assertEquals(1, tarefaCategoria.size());
        assertEquals(tarefa.getId(), tarefaCategoria.get(0).getId());
        service.progedirStatusTarefa(tarefa);

        tarefaCategoria = service.listarSituacao(StatusTarefaEnum.ESPERANDO);
        assertEquals(1, tarefaCategoria.size());
        assertEquals(tarefa.getId(), tarefaCategoria.get(0).getId());
        service.progedirStatusTarefa(tarefa);

        tarefaCategoria = service.listarSituacao(StatusTarefaEnum.FEITO);
        assertEquals(1, tarefaCategoria.size());
        assertEquals(tarefa.getId(), tarefaCategoria.get(0).getId());
        service.progedirStatusTarefa(tarefa);

        assertEquals(0, service.listarSituacao(StatusTarefaEnum.PARA_FAZER).size());
        assertEquals(0, service.listarSituacao(StatusTarefaEnum.TRABALHANDO).size());
        assertEquals(0, service.listarSituacao(StatusTarefaEnum.ESPERANDO).size());
    }

    @Test
    public void regredirTarefa() throws TarefaNaoEncontradaException {
        Tarefa tarefa = new Tarefa();
        tarefa.setNome("Definir identidade visual");
        service.salvar(tarefa);

        service.progedirStatusTarefa(tarefa);
        service.progedirStatusTarefa(tarefa);
        service.progedirStatusTarefa(tarefa);
        assertEquals(1, service.listarSituacao(StatusTarefaEnum.FEITO).size());

        service.regredirStatusTarefa(tarefa);
        assertEquals(1, service.listarSituacao(StatusTarefaEnum.ESPERANDO).size());
    }

    @Test
    public void atualizarTarefa() throws TarefaNaoEncontradaException {
        Tarefa tarefa = new Tarefa();
        tarefa.setNome("Definir SGDB - Prefencia MySQL");
        service.salvar(tarefa);

        tarefa.setNome("Definir SGDB");
        service.salvar(tarefa);

        Tarefa tarefa2 = service.obterDetalhes(tarefa.getId());
        assertEquals(tarefa.getId(), tarefa2.getId());
        assertEquals(tarefa.getNome(), tarefa2.getNome());
        assertEquals(StatusTarefaEnum.PARA_FAZER, tarefa2.getStatusTarefa());
    }

    @Test
    public void excluirTarefa() {
        Long idTarefa = null;
        try {
            Tarefa tarefa = new Tarefa();
            tarefa.setNome("Definir SGDB - Prefencia Oracle");
            service.salvar(tarefa);
            idTarefa = tarefa.getId();
            service.excluir(tarefa);
            service.obterDetalhes(idTarefa);
            fail("Erro, dever gerar um exception");
        } catch (TarefaNaoEncontradaException tarefaException) {
            String mensageError = String.format("Tarefa de ID: %s não encontrado", idTarefa);
            assertEquals(mensageError, tarefaException.getMessage());
        }
    }

    @Test
    public void criandoQuadroIgualEspecificacao() throws TarefaNaoEncontradaException {
        apagarTodasTarefas();
        Tarefa tarefa = new Tarefa();

        tarefa.setNome("Definir identidade visual");
        service.salvar(tarefa);

        tarefa.setId(null);
        tarefa.setNome("Definir SGDB");
        service.salvar(tarefa);

        tarefa.setId(null);
        tarefa.setNome("Implentar login");
        service.salvar(tarefa);
        service.progedirStatusTarefa(tarefa);

        tarefa.setId(null);
        tarefa.setNome("Implentar cadastro");
        service.salvar(tarefa);
        service.progedirStatusTarefa(tarefa);
        service.progedirStatusTarefa(tarefa);

        tarefa.setId(null);
        tarefa.setNome("Feito");
        service.salvar(tarefa);
        service.progedirStatusTarefa(tarefa);
        service.progedirStatusTarefa(tarefa);
        service.progedirStatusTarefa(tarefa);

        assertEquals(2, service.listarSituacao(StatusTarefaEnum.PARA_FAZER).size());
        assertEquals(1, service.listarSituacao(StatusTarefaEnum.TRABALHANDO).size());
        assertEquals(1, service.listarSituacao(StatusTarefaEnum.ESPERANDO).size());
        assertEquals(1, service.listarSituacao(StatusTarefaEnum.FEITO).size());
    }

    protected void apagarTodasTarefas() throws TarefaNaoEncontradaException {
        List<Tarefa> tarefas = service.listagem();
        for (Tarefa tarefa2 : tarefas) {
            service.excluir(tarefa2);
        }
    }
}
