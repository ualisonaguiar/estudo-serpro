package com.estudoserpro.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

public class Sala {
	
	private Long id;
	private Integer numeroSala;
	private static final Logger LOGGER = Logger.getLogger(Sala.class.getName());
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Integer getNumeroSala() {
		return numeroSala;
	}
	
	public void setNumeroSala(Integer numeroSala) {
		this.numeroSala = numeroSala;
	}
	
	public Sala prepareEntityResultBuscaAgendamento(ResultSet result) {
		
		try {
			setId(result.getLong(1));
			setNumeroSala(result.getInt(2));
		} catch (SQLException e) {
			LOGGER.severe(e.toString());
		}
		
		return this;
	}

}
