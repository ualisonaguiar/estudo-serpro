package com.estudoserpro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Logger;

import com.estudoserpro.model.Agenda;

public class AgendaDAO extends AbstractConexao {
	
	private static final Logger LOGGER = Logger.getLogger(AgendaDAO.class.getName());

	public Optional<Agenda> buscarAgendamento(Agenda agenda) {
		Optional<Agenda> agendamentoResult = Optional.empty();
		PreparedStatement prepareStatement = null;
		try {
			Connection conexao = getConexao();
			String query = "SELECT s.id, s.numero_sala, a.data, a.horario, a.duracao FROM agendamentos a inner join salas s on s.id = a.sala_id "
					+ " WHERE s.numero_sala = ? AND a.data = ? AND ((TIME(?) BETWEEN a.horario AND ADDTIME(a.horario, SEC_TO_TIME(?))) "
					+ " OR (ADDTIME(TIME(?), SEC_TO_TIME(?)) > a.horario AND ADDTIME(TIME(?), SEC_TO_TIME(?)) <= ADDTIME(a.horario, SEC_TO_TIME(a.duracao * 60))))";

			prepareStatement = conexao.prepareStatement(query);
			prepareStatement.setInt(1, agenda.getSala().getNumeroSala());
			prepareStatement.setDate(2, agenda.getData());
			prepareStatement.setTime(3, agenda.getHorario());
			prepareStatement.setInt(4, agenda.getDuracao() * 60);
			prepareStatement.setTime(5, agenda.getHorario());
			prepareStatement.setInt(6, agenda.getDuracao() * 60);
			prepareStatement.setTime(7, agenda.getHorario());
			prepareStatement.setInt(8, agenda.getDuracao() * 60);
			prepareStatement.execute();
			ResultSet resultSet = prepareStatement.getResultSet();
			prepareStatement.close();
			
			while (resultSet.next()) {
				Agenda agendamento = new Agenda();
				agendamento.prepareEntityResultBuscaAgendamento(resultSet);
				agendamentoResult = Optional.of(agendamento);
			}

		} catch (SQLException e) {
			LOGGER.info(e.toString());
		} finally {
			fecharPrepareStatement(prepareStatement);
			fecharConexao();
		}

		return agendamentoResult;
	}

	public Agenda agendarSala(Agenda agenda) throws SQLException {
		PreparedStatement statement = null;
		try {
			Connection conexao = getConexao();
			statement = conexao.prepareStatement(
					"insert into agendamentos (sala_id, participantes, equipamentos, data, horario, duracao) values (?, ?, ?, ?, ?, ?)");
			statement.setLong(1, agenda.getSala().getId());
			statement.setString(2, agenda.getParticipantes());
			statement.setString(3, agenda.getEquipamentos());
			statement.setDate(4, agenda.getData());
			statement.setTime(5, agenda.getHorario());
			statement.setInt(6, agenda.getDuracao());
			statement.executeUpdate();
	
			return agenda;
		} finally {
			fecharPrepareStatement(statement);
			fecharConexao();
		}
	}

}
