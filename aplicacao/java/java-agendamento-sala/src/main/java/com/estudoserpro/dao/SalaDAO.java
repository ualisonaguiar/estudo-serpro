package com.estudoserpro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Logger;

import com.estudoserpro.model.Sala;

public class SalaDAO extends AbstractConexao {
	
	private static final Logger LOGGER = Logger.getLogger(SalaDAO.class.getName());
	
	public Optional<Sala> buscarSala(final Long id) {
		PreparedStatement prepareStatement = null;
		try {
			Connection conexao = getConexao();
			prepareStatement = conexao.prepareStatement("select * from salas where id = ?");
			prepareStatement.setLong(1, id);
			prepareStatement.execute();
			ResultSet resultSet = prepareStatement.getResultSet();
			return populateDomain(resultSet);
		} catch (SQLException e) {
			LOGGER.info(e.toString());
		} finally {
			fecharPrepareStatement(prepareStatement);
			fecharConexao();
		}	
		return Optional.empty();
	}
	
	public Optional<Sala> buscarSalaNumero(final Integer numeroSala) {
		PreparedStatement prepareStatement = null;
		try {
			Connection conexao = getConexao();
			prepareStatement = conexao.prepareStatement("select * from salas where numero_sala = ?");
			prepareStatement.setInt(1, numeroSala);
			prepareStatement.execute();
			ResultSet resultSet = prepareStatement.getResultSet();
			return populateDomain(resultSet);
		} catch (SQLException e) {
			LOGGER.info(e.toString());
		} finally {
			fecharPrepareStatement(prepareStatement);
			fecharConexao();
		}	
		return Optional.empty();
	}	
	
	protected Optional<Sala> populateDomain(ResultSet resultSet) throws SQLException {
		Optional<Sala> resultSala = Optional.empty();
		
		while(resultSet.next()) {
			Sala sala = new Sala();
			sala.setId(resultSet.getLong(1));
			sala.setNumeroSala(resultSet.getInt(2));
			resultSala = Optional.of(sala);
		}		
		
		return resultSala;
	}
	
}
