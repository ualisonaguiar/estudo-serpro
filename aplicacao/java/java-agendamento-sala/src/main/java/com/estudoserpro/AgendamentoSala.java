package com.estudoserpro;

import java.sql.Date;
import java.sql.Time;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.estudoserpro.model.Agenda;
import com.estudoserpro.model.Sala;
import com.estudoserpro.service.AgendaService;
import com.estudoserpro.service.AgendaServiceImp;
import com.estudoserpro.service.SalaService;
import com.estudoserpro.service.SalaServiceImp;

public class AgendamentoSala {
	
	private static final Logger LOGGER = Logger.getLogger(AgendamentoSala.class.getName());

    public static void main(String[] args) {    	
    	SalaService salaService = new SalaServiceImp();
    	AgendaService agendaService = new AgendaServiceImp();
        Scanner scanner = new Scanner(System.in);
        Agenda agenda = new Agenda();
        
        LOGGER.info("Número da sala: ");
        Integer numeroSala = scanner.nextInt();
        Optional<Sala> sala = salaService.buscarSala(numeroSala);
        
        if (Boolean.TRUE.equals(sala.isPresent())) {
        	agenda.setSala(sala.get());
        	
	        LOGGER.info("Relação de participantes (separados por vírgula): ");
	        agenda.setParticipantes(scanner.next());
	
	        LOGGER.info("Relação de equipamentos (separados por vírgula): ");
	        agenda.setEquipamentos(scanner.next());        
	
	        LOGGER.info("Data (AAAA-MM-DD): ");
	        agenda.setData(Date.valueOf(scanner.next()));
	
	        LOGGER.info("Horário (HH:mm:ss): ");
	        agenda.setHorario(Time.valueOf(scanner.next()));
	
	        LOGGER.info("Duração (em minutos): ");
	        agenda.setDuracao(scanner.nextInt());
	        
	        if (Boolean.TRUE.equals(agendaService.agendarSala(agenda))) {
	        	LOGGER.log(Level.INFO, "Sala {0} reservada com sucesso", agenda.getSala().getNumeroSala());
	        } else {
	        	LOGGER.log(Level.WARNING, "Não foi possível reservar a sala: {0}. Já existe um agendamento para esse horário", numeroSala);
	        }
        } else {
        	LOGGER.log(Level.INFO, "Sala: {0} não cadastrada.", numeroSala);
        }
    }
}

