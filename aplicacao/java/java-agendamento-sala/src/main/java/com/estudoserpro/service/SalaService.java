package com.estudoserpro.service;

import java.util.Optional;

import com.estudoserpro.model.Sala;

public interface SalaService {

	public Optional<Sala> buscarSala(Long id);
	
	public Optional<Sala> buscarSala(Integer numeroSala);

}
