package com.estudoserpro.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

public abstract class AbstractConexao {

	protected Connection conexao;
	private static final Logger LOGGER = Logger.getLogger(AbstractConexao.class.getName());

	protected Connection getConexao() {

		try {
			this.conexao = DriverManager.getConnection("jdbc:mysql://localhost/agenda", "root", "abcd1234");
		} catch (SQLException e) {
			LOGGER.severe(e.toString());
		}

		return conexao;
	}

	protected void fecharPrepareStatement(PreparedStatement preparedStatement) {
		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				LOGGER.severe(e.toString());
			}
		}
	}

	protected void fecharConexao() {
		if (conexao != null) {
			try {
				conexao.close();
			} catch (SQLException e) {
				LOGGER.severe(e.toString());
			}
		}
	}
}
