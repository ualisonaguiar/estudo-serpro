package com.estudoserpro.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.logging.Logger;

public class Agenda {

	private Long id;
	private Sala sala;
	private String participantes;
	private String equipamentos;
	private Date data;
	private Time horario;
	private Integer duracao;
	
	private static final Logger LOGGER = Logger.getLogger(Agenda.class.getName());

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

	public String getParticipantes() {
		return participantes;
	}

	public void setParticipantes(String participantes) {
		this.participantes = participantes;
	}

	public String getEquipamentos() {
		return equipamentos;
	}

	public void setEquipamentos(String equipamentos) {
		this.equipamentos = equipamentos;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Time getHorario() {
		return horario;
	}

	public void setHorario(Time horario) {
		this.horario = horario;
	}

	public Integer getDuracao() {
		return duracao;
	}

	public void setDuracao(Integer duracao) {
		this.duracao = duracao;
	}
	
	public Agenda prepareEntityResultBuscaAgendamento(ResultSet result) {
		try {
			Sala salaEntity = new Sala();
			salaEntity.prepareEntityResultBuscaAgendamento(result);
			setSala(salaEntity);
			
			setData(result.getDate(3));
			setHorario(result.getTime(4));
			setDuracao(result.getInt(5));
		} catch (SQLException e) {
			LOGGER.severe(e.toString());
		}
		
		return this;
	}

}
