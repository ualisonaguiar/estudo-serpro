package com.estudoserpro.service;

import com.estudoserpro.model.Agenda;

public interface AgendaService {

	public Boolean agendarSala(Agenda agenda);
	
	public Boolean verificarConflito(Agenda agenda);

}
