package com.estudoserpro.service;

import java.util.Optional;

import com.estudoserpro.dao.SalaDAO;
import com.estudoserpro.model.Sala;

public class SalaServiceImp implements SalaService {
	
	private SalaDAO repository;
	
	public SalaServiceImp() {
		this.repository = new SalaDAO();
	}

	@Override
	public Optional<Sala> buscarSala(final Long id) {
		return repository.buscarSala(id);
	}

	@Override
	public Optional<Sala> buscarSala(Integer numeroSala) {
		return repository.buscarSalaNumero(numeroSala);
	}

}
