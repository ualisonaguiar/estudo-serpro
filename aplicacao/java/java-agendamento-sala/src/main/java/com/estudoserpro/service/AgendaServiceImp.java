package com.estudoserpro.service;

import java.sql.SQLException;
import java.util.logging.Logger;

import com.estudoserpro.dao.AgendaDAO;
import com.estudoserpro.model.Agenda;

public class AgendaServiceImp implements AgendaService {

	private AgendaDAO repository;
	private static final Logger LOGGER  = Logger.getLogger(AgendaServiceImp.class.getName());

	public AgendaServiceImp() {
		repository = new AgendaDAO();
	}

	@Override
	public Boolean agendarSala(Agenda agenda) {
		if (Boolean.FALSE.equals(verificarConflito(agenda))) {
			try {
				repository.agendarSala(agenda);
				return true;
			} catch (SQLException e) {
				LOGGER.severe(e.toString());
			}
		}
		return false;
	}

	@Override
	public Boolean verificarConflito(Agenda agenda) {
		return repository.buscarAgendamento(agenda).isPresent();
	}

}
