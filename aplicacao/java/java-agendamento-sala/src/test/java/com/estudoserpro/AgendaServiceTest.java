package com.estudoserpro;

import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.sql.Time;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.estudoserpro.model.Agenda;
import com.estudoserpro.model.Sala;
import com.estudoserpro.service.AgendaService;
import com.estudoserpro.service.AgendaServiceImp;
import com.estudoserpro.service.SalaService;
import com.estudoserpro.service.SalaServiceImp;

public class AgendaServiceTest {
	
	private AgendaService service;
	private SalaService salaService;

	@BeforeEach
	public void setUp() {
		service = new AgendaServiceImp();
		salaService = new SalaServiceImp();
	}
	
	@Test
	public void agendarSala() {
		Sala sala = salaService.buscarSala(10).get();		
		Agenda agenda = new Agenda();
		agenda.setSala(sala);
		agenda.setData(Date.valueOf("2023-08-09"));
		agenda.setDuracao(60);
		agenda.setEquipamentos("Notebook;");
		agenda.setHorario(Time.valueOf("08:00:00"));		
		agenda.setParticipantes("Ualison Aguiar;João Paulo");		
		service.agendarSala(agenda);
		
		assertTrue(service.agendarSala(agenda));
		
	}
}
