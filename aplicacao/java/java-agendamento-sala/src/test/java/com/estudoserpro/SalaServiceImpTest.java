package com.estudoserpro;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.estudoserpro.model.Sala;
import com.estudoserpro.service.SalaService;
import com.estudoserpro.service.SalaServiceImp;

public class SalaServiceImpTest {
	
	private SalaService service;

	@BeforeEach
	public void setUp() {
		service= new SalaServiceImp();
	}
	
	@Test
	public void buscarSala() {
		Optional<Sala> sala = service.buscarSala(2L);
		assertTrue(sala.isPresent());
		
		Integer numeroSala = sala.get().getNumeroSala();
		assertTrue(service.buscarSala(numeroSala).isPresent());
	}
	
}
