import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseQueryExample {
    public static void main(String[] args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        
        try {
            // Estabelecer conexão com o banco de dados
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydatabase", "username", "password");
            
            // Criar uma instrução SQL parametrizada
            String sqlQuery = "SELECT * FROM products WHERE category = ?";
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setString(1, "Electronics");
            
            // Executar a consulta SQL
            resultSet = preparedStatement.executeQuery();
            
            // Processar os resultados
            while (resultSet.next()) {
                String productName = resultSet.getString("product_name");
                double price = resultSet.getDouble("price");
                System.out.println("Product: " + productName + ", Price: " + price);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Fechar conexão, instrução e resultado
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}