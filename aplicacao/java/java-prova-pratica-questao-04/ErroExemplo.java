public class ErroExemplo {
    public static void main(String[] args) {
        int numero = 10;
        String mensagem = "Olá, mundo!";
        
        System.out.println(numero);
        System.out.println(mensagem);
        
        numero = 20;
        
        int resultado = somaNumeros(numero, 5);
        System.out.println("Resultado: " + resultado);
    }
    
    public static int somaNumeros(int a, int b) {
        return a + b;
    }
}
3º 
