let numeros = [5, 1, 8, 7, 3, 4, 10, 9];
let tamanho = numeros.length

for (let idx = 0; idx < tamanho -1; idx++) {
	let minIdex = idx;

	for (let jdx = idx + 1; jdx < tamanho; jdx++) {
		if (numeros[jdx] < numeros[minIdex]) {
			minIdex = jdx;
		}
	}

	if (minIdex != idx) {
		let numeroTemporario = numeros[idx];
		numeros[idx] = numeros[minIdex];
		numeros[minIdex] = numeroTemporario;
	}
}

for (let numero of numeros) {
	console.log('Número: ' + numero);
}