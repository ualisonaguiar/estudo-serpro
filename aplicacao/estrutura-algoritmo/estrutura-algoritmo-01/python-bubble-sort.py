numeros:[] = [5, 1, 8, 7, 3, 4, 10, 9]
current_state = []

while True :
	current_state[:] = numeros[:]

	for idx in range(len(numeros) - 1):
		if numeros[idx] > numeros[idx + 1]:
			[numeros[idx], numeros[idx + 1]] = [numeros[idx + 1], numeros[idx]]

	if current_state == numeros:
		break

for numero in numeros:
	print(f'Número: {numero}')
