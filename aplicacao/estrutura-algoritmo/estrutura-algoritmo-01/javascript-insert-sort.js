let numeros = [5,1, 8, 7, 3, 4, 10, 9];
let tamanho = numeros.length;

for (let idx = 0; idx < tamanho; idx++) {
	let numeroTemporario = numeros[idx];
	let jdx = idx - 1;

	while(jdx >= 0 && numeros[jdx] > numeroTemporario) {
		numeros[jdx + 1] = numeros[jdx]
		jdx -= 1;
	}

	numeros[jdx + 1] = numeroTemporario;
}

for (let numero of numeros) {
	console.log('Número: ' + numero);
}