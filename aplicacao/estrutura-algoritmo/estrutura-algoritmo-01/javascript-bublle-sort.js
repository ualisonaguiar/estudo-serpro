const bublleSort = function(numeros) {
	let tamanho = numeros.length;
	let swapped = false;

	for (let idx = 0; idx < tamanho; idx++) {
		swapped = false;
		for (let jdx = 0; jdx < tamanho -idx -1; jdx++) {
			if (numeros[jdx] > numeros[jdx + 1]) {
				let numero_temporario = numeros[jdx];
				numeros[jdx] = numeros[jdx + 1];
				numeros[jdx + 1] = numero_temporario;
				swapped = true;
			}
		}

		if (!swapped) {
			break;
		}
	}


	return numeros;
}

numeros = bublleSort([5, 1, 8, 7, 3, 4, 10, 9]);
for (let numero of numeros) {
	console.log('Número: ' + numero);
}