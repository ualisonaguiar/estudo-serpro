import java.util.logging.Logger;
import java.util.logging.Level;

class InsertSort {

	private final static Logger LOGGER = Logger.getLogger(InsertSort.class.getName());

	protected static int[] insertSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 1; idx < tamanho; idx++) {
			int numeroTemporario = numeros[idx];
			int jdx = idx - 1;

			while(jdx >= 0 && numeros[jdx] > numeroTemporario) {
				numeros[jdx + 1] = numeros[jdx];
				jdx -= 1;
			}
			numeros[jdx + 1] = numeroTemporario;
		}

		return numeros;
	}

	public static void main(String args[]) {

		int[] numeros = {5,1, 8, 7, 3, 4, 10, 9};
		for (int numero: insertSort(numeros)) {
			LOGGER.log(Level.INFO, "Número: {0}", numero);
		}

	}
}