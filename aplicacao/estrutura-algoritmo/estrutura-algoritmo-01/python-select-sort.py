numeros: [int] = [5, 1, 8, 7, 3, 4, 10, 9];
tamanho = len(numeros)

for idx in range(tamanho):
	minIdx: int = idx

	for jdx in range(idx, tamanho):
		if numeros[jdx] < numeros[minIdx]:
			minIdx = jdx

	if minIdx != idx:
		numero_temporario: int = numeros[idx]
		numeros[idx] = numeros[minIdx]
		numeros[minIdx] = numero_temporario


for numero in numeros:
	print(f'Número: {numero}')