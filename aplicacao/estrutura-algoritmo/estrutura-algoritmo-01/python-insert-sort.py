numeros: [] = [5,1, 8, 7, 3, 4, 10, 9]

for idx in range(1, len(numeros)):
	numero_temporario = numeros[idx]
	jdx: int = idx -1

	while jdx >= 0 and numeros[jdx] > numero_temporario:
		numeros[jdx + 1] = numeros[jdx]
		jdx -= 1

	numeros[jdx + 1] = numero_temporario


for numero in numeros:
	print(f'Número: {numero}')