import java.util.logging.Logger;
import java.util.logging.Level;

class SelectSort {

	private static final Logger LOGGER = Logger.getLogger(SelectSort.class.getName());

	protected static void selectSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho - 1; idx++) {
			int minIndex = idx;

			for (int jdx = idx + 1; jdx < tamanho; jdx++) {
				if (numeros[jdx] < numeros[minIndex]) {
					minIndex = jdx;
				}
			}

			if (minIndex != idx) {
				int numeroTemporario = numeros[idx];
				numeros[idx] = numeros[minIndex];
				numeros[minIndex] = numeroTemporario;
			}
		}
	}


	public static void main(String args[]) {
		int[] numeros = {5, 1, 8, 7, 3, 4, 10, 9};
		selectSort(numeros);

		for (int numero: numeros) {
			LOGGER.log(Level.INFO, "Número: {0}", numero);
		}
	}
}