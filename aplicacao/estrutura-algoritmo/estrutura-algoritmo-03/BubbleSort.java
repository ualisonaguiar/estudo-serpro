import java.util.logging.Logger;
import java.util.logging.Level;

class BubbleSort {

	private final static Logger LOGGER = Logger.getLogger(BubbleSort.class.getName());

	protected static void bubbleSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho; idx ++) {
			for (int jdx = 0; jdx < tamanho - idx - 1; jdx++) {
				if (numeros[jdx] > numeros[jdx + 1]) {
					int numeroTemporario = numeros[jdx];
					numeros[jdx] = numeros[jdx + 1];
					numeros[jdx + 1] = numeroTemporario;
				}
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 1, 8, 7, 3, 4, 10, 9};
		bubbleSort(numeros);
		for (int numero : numeros) {
			LOGGER.log(Level.INFO, "Número: {0}", numero);
		}
	}
}