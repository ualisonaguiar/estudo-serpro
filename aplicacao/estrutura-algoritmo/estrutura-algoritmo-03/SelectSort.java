import java.util.logging.Logger;
import java.util.logging.Level;

class SelectSort {

	private static final Logger LOGGER = Logger.getLogger(SelectSort.class.getName());

	protected static void selectSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho; idx++) {
			int minxIdx = idx;

			for (int jdx = 1 + idx; jdx < tamanho; jdx++) {
				if (numeros[jdx] < numeros[minxIdx]) {
					minxIdx = jdx;
				}
			}

			if (minxIdx != idx) {
				int numeroTemporario = numeros[idx];
				numeros[idx] = numeros[minxIdx];
				numeros[minxIdx] = numeroTemporario;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 1, 8, 7, 3, 4, 10, 9};
		selectSort(numeros);
		for (int numero : numeros) {
			LOGGER.log(Level.INFO, "Número: {0}", numero);
		}
	}
}
