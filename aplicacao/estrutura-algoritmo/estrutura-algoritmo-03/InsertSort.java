import java.util.logging.Logger;
import java.util.logging.Level;

class InsertSort {

	private static final Logger LOGGER = Logger.getLogger(InsertSort.class.getName());

	protected static void insertSort(int[] numeros) {
		int tamanho = numeros.length;

		for(int idx = 1; idx < tamanho; idx++) {
			int numeroTemporario = numeros[idx];
			int jdx = idx - 1;
			while(jdx >= 0 && numeros[jdx] > numeroTemporario) {
				numeros[jdx + 1] = numeros[jdx];
				jdx -= 1;
			}

			numeros[jdx + 1] = numeroTemporario;
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 1, 8, 7, 3, 4, 10, 9};
		insertSort(numeros);
		for (int numero : numeros) {
			LOGGER.log(Level.INFO, "Número: {0}", numero);
		}
	}

}