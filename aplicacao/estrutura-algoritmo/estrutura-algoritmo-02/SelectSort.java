import java.util.logging.Logger;
import java.util.logging.Level;

class SelectSort {

	private final static Logger LOGGER = Logger.getLogger(SelectSort.class.getName());

	protected static void selectSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho; idx++) {
			int minIdx = idx;

			for (int jdx = idx + 1; jdx < tamanho; jdx++) {
				if (numeros[jdx] < numeros[minIdx]) {
					minIdx = jdx;
				}
			}

			if (minIdx != idx) {
				int numeroTemporario = numeros[idx];
				numeros[idx] = numeros[minIdx];
				numeros[minIdx] = numeroTemporario;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {7, 5, 4, 2};
		selectSort(numeros);

		for (int numero: numeros) {
			LOGGER.log(Level.INFO, "Número: {0}", numero);
		}
	}
}