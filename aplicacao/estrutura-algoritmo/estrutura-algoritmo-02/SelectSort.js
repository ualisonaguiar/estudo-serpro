let numeros = [7, 5, 4, 2]
let tamanho = numeros.length

for (let idx = 0; idx < tamanho; idx++) {
	let minIdx = idx;

	for (let jdx = idx + 1; jdx < tamanho; jdx++) {
		if (numeros[jdx] < numeros[minIdx]) {
			minIdx = jdx;
		}
	}

	if (minIdx != idx) {
		let numeroTemporario = numeros[idx];
		numeros[idx] = numeros[minIdx];
		numeros[minIdx] = numeroTemporario;
	}
}

for (let numero of numeros) {
	console.log('Número: ' + numero)
}