numeros: [int] = [9, 7, 6, 15, 17, 5, 10, 11]
tamanho = len(numeros)

for idx in range(1, tamanho):
	numero_temporario: int = numeros[idx]
	jdx = idx - 1

	while jdx >= 0 and numeros[jdx] > numero_temporario:
		numeros[jdx + 1] = numeros[jdx]
		jdx -= 1

	numeros[jdx + 1] = numero_temporario


for numero in numeros:
	print(f'Número: {numero}')