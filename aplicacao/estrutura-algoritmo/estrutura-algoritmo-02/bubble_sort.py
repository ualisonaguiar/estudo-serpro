numeros:[int] = [5, 3, 8, 4, 6]
tamanho: int = len(numeros)
swaped: bool = False

for idx in range(tamanho) :
	swaped = False

	for jdx in range(idx, tamanho - 1):
		if numeros[jdx] > numeros[jdx + 1]:
			numero_temporario: int = numeros[jdx]
			numeros[jdx] = numeros[jdx + 1]
			numeros[jdx + 1] = numero_temporario

for numero in numeros:
	print(f'Número: {numero}')
