import java.util.logging.Logger;
import java.util.logging.Level;

class BubbleSort {

	private static final Logger LOGGER = Logger.getLogger(BubbleSort.class.getName());

	protected static void bublleSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho; idx++) {
			for (int jdx = 0; jdx < tamanho - 1 - idx; jdx++) {
				if (numeros[jdx] > numeros[jdx + 1]) {
					int numeroTemporario = numeros[jdx];
					numeros[jdx] = numeros[jdx + 1];
					numeros[jdx + 1] = numeroTemporario;
				}
			}
		}

	}

	public static void main(String args[]) {
		int[] numeros = {5, 3, 8, 4, 6};
		bublleSort(numeros);
		for(int numero : numeros) {
			LOGGER.log(Level.INFO, "Número: {0}", numero);
		}
	}
}