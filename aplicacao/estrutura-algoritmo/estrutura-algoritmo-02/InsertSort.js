let numeros = [9, 7, 6, 15, 17, 5, 10, 11]
let tamanho = numeros.length

for (let idx = 0; idx < tamanho; idx++) {
	let numeroTemporario = numeros[idx];
	let jdx = idx - 1;

	while (jdx >= 0 && numeros[jdx] > numeroTemporario) {
		numeros[jdx + 1] = numeros[jdx];
		jdx -= 1;
	}

	numeros[jdx + 1] = numeroTemporario;
}

for (let numero of numeros) {
	console.log('Número: ' + numero);
}