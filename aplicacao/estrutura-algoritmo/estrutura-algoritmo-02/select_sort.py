numeros: [int] = [7, 5, 4, 2]
tamanho: int = len(numeros)

for idx in range(tamanho):
	minIdx: int = idx

	for jdx in range(minIdx, tamanho):

		if numeros[jdx] < numeros[minIdx]:
			minIdx = jdx

	if minIdx != idx:
		numero_temporario: int = numeros[idx]
		numeros[idx] = numeros[minIdx]
		numeros[minIdx] = numero_temporario

for numero in numeros:
	print(f'Número: {numero}')