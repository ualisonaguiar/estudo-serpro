public class BubbleSort {

	public static void bubbleSort(int[] numeros) {
		int tamanho = numeros.length;
		
		while(true) {
			int contador = 0;
			for(int idx = 0; idx < tamanho - 1; idx++) {
				if (numeros[idx] > numeros[idx + 1]) {
					int numeroTemporario = numeros[idx];
					numeros[idx] = numeros[idx + 1];
					numeros[idx + 1] = numeroTemporario;
					contador += 1;
				}
			}

			if (contador == 0) {
				break;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 22, 3, 8, 30, 4, 6, 10, 1, 2, 21};
		bubbleSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}
	}
}