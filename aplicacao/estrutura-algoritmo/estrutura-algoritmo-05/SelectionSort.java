public class SelectionSort {

	public static void selectionSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho; idx++) {
			int minIdx = idx;

			for (int jdx = idx + 1; jdx < tamanho; jdx++) {
				if (numeros[jdx] < numeros[minIdx]) {
					minIdx = jdx;
				}
			}

			if (minIdx != idx) {
				int numeroTemporario = numeros[idx];
				numeros[idx] = numeros[minIdx];
				numeros[minIdx] = numeroTemporario;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 22, 3, 8, 30, 4, 6, 10, 1, 2, 21};
		selectionSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}		
	}
}