numeros = [5, 22, 3, 8, 30, 4, 6, 10, 1, 2, 21]
tamanho = len(numeros)

print(numeros)
for idx in range(tamanho):
	minIdx = idx

	for jdx in range(idx + 1, tamanho):
		if numeros[jdx] < numeros[minIdx]:
			minIdx = jdx

	if minIdx != idx:
		numeros[idx], numeros[minIdx] = numeros[minIdx], numeros[idx]

print(numeros)