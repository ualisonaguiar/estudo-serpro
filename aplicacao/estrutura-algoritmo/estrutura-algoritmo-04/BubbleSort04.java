public class BubbleSort04 {

	public static void bubbleSort(int[] numeros) {
		int tamanho = numeros.length;

		while(true) {
			int contador = 0;
			for (int idx = 0; idx < tamanho - 1; idx++) {
				if (numeros[idx] > numeros[idx + 1]) {
					int numeroTemporario = numeros[idx];
					numeros[idx] = numeros[idx + 1];
					numeros[idx + 1] = numeroTemporario;
					contador += 1;
				}
			}

			if (contador == 0) {
				break;
			}

		}
	}

	public static void main(String args[]) {
		int[] numeros = {7, 5, 4, 2, 6, 8};
		bubbleSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}		
	}
}