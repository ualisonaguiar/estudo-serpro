public class InsertSort03 {

	public static void insertSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 1; idx < tamanho; idx++) {
			int jdx = idx;

			while(jdx > 0 && numeros[jdx] < numeros[jdx - 1]) {
				int numeroTemporario = numeros[jdx];
				numeros[jdx] = numeros[jdx - 1];
				numeros[jdx - 1] = numeroTemporario;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 3, 8, 4,6};
		insertSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}
	}
}