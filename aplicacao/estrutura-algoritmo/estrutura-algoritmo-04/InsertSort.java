public class InsertSort {

	public static void insertSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 1; idx < tamanho; idx++) {
			int numeroTemporario = numeros[idx];
			int jdx = idx - 1;

			while (jdx >= 0 && numeros[jdx] > numeroTemporario) {
				numeros[jdx + 1] = numeros[jdx];
				jdx -= 1;
			}

			numeros[jdx + 1] = numeroTemporario;
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 3, 8, 4,6};
		insertSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}		
	}
}