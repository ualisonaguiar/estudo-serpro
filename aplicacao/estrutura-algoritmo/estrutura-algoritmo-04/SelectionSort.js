let numeros = [5, 3, 8, 4,6];
let tamanho = numeros.length;

for (let idx = 0; idx < tamanho - 1; idx++) {
	let minIndex = idx;

	for (let jdx = idx + 1; jdx < tamanho; jdx++) {

		if (numeros[jdx] < numeros[minIndex]) {
			minIndex = jdx;
		}
	}

	if (minIndex != idx) {
		let numeroTemporario = numeros[idx];
		numeros[idx] = numeros[minIndex];
		numeros[minIndex] = numeroTemporario;		
	}
}

console.log(numeros);