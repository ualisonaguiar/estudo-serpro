let numeros = [5, 3, 8, 4,6];
let tamanho = numeros.length;

for (let idx = 1; idx < tamanho; idx++) {
	let numeroTemporario = numeros[idx];
	let jdx = idx - 1;

	while(jdx >= 0 && numeros[jdx] > numeroTemporario) {
		numeros[jdx + 1] = numeros[jdx];
		jdx -= 1;
	}
	numeros[jdx + 1] = numeroTemporario;
}

console.log(numeros);