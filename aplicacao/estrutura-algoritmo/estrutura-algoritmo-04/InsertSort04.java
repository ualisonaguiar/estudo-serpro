public class InsertSort04 {

	public static void insertSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 1; idx < tamanho; idx++) {
			int jdx = idx;

			while(jdx > 0 && numeros[jdx] < numeros[jdx - 1]) {
				int numeroTemporario = numeros[jdx];
				numeros[jdx] = numeros[jdx - 1];
				numeros[jdx - 1] = numeroTemporario;
				jdx -= 1;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {7, 5, 4, 2, 6, 8};
		insertSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}		
	}		
}