public class BubbleSort {

	public static void bubbleSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho - 1; idx++) {
			for (int jdx = 0; jdx < tamanho -idx -1; jdx++) {
				if (numeros[jdx] > numeros[jdx + 1]) {
					int numeroTemporario = numeros[jdx];
					numeros[jdx] = numeros[jdx + 1];
					numeros[jdx + 1] = numeroTemporario;
				}

			}
		}
	}

	public static void main(String args[]) {

		int[] numeros = {5, 3, 8, 4,6};
		bubbleSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}
	}
}