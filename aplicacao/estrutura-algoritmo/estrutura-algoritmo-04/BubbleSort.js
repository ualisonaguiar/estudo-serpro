let numeros = [5, 3, 8, 4,6];
let tamanho = numeros.length;

for (let idx = 0; idx < tamanho - 1; idx++) {
	for (let jdx = 0; jdx < tamanho - 1 - idx; jdx++) {

		if (numeros[jdx] > numeros[jdx + 1]) {
			let numeroTemporario = numeros[jdx];
			numeros[jdx] = numeros[jdx + 1];
			numeros[jdx + 1] = numeroTemporario;
		}
	}
}

console.log(numeros);