public class BubbleSort03 {

	public static void bubbleSort(int[] numeros) {
		int tamanho = numeros.length;
		int[] numeros2 = new int[numeros.length];

		while (true) {
			for (int idx = 0; idx < tamanho - 1; idx++) {
				numeros2[idx] = numeros[idx];

				if (numeros[idx] > numeros[idx + 1]) {
					int numeroTemporario = numeros[idx];
					numeros[idx] = numeros[idx + 1];
					numeros[idx + 1] = numeroTemporario;
				}
			}

			if (numeros2.length == numeros.length) {
				break;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 3, 8, 4,6};
		bubbleSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}
	}
}