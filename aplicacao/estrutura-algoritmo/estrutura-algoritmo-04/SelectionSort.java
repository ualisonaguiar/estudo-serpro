public class SelectionSort {

	public static void selectionSort(int[] numeros) {
		int tamanho = numeros.length;
		for (int idx = 0; idx < tamanho - 1; idx++) {
			int minIdx = idx;
			for (int jdx = idx + 1; jdx < tamanho; jdx++) {
				if (numeros[jdx] < numeros[minIdx]) {
					minIdx = jdx;
				}
			}
			if (minIdx != idx) {
				int numeroTemporario = numeros[idx];
				numeros[idx] = numeros[minIdx];
				numeros[minIdx] = numeroTemporario;
			}
		}
	}

	public static void main(String args[]) {
		int[] numeros = {5, 3, 8, 4,6};
		selectionSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}
	}
}