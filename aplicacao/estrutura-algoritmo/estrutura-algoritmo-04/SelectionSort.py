numeros = [5, 3, 8, 4,6]
tamanho = len(numeros)

for idx in range(tamanho):
	menor_valor = numeros[idx]
	idxMenor = idx

	for jdx in range(idx, tamanho):
		if numeros[jdx] < menor_valor:
			menor_valor = numeros[jdx]
			idxMenor = jdx

	numeros[idx], numeros[idxMenor] = numeros[idxMenor], numeros[idx]

print(numeros)