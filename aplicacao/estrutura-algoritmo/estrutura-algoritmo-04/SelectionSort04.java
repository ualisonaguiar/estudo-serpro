public class SelectionSort04 {

	public static void selectionSort(int[] numeros) {
		int tamanho = numeros.length;

		for (int idx = 0; idx < tamanho; idx++) {
			int minIdx = idx;
			for(int jdx = idx + 1; jdx < tamanho; jdx++) {
				if (numeros[jdx] < numeros[minIdx]) {
					minIdx = jdx;
				}
			}

			int numeroTemporario = numeros[idx];
			numeros[idx] = numeros[minIdx];
			numeros[minIdx] = numeroTemporario;
		}
	}

	public static void main(String args[]) {
		int[] numeros = {7, 5, 4, 2, 6, 8};
		selectionSort(numeros);
		for (int numero: numeros) {
			System.out.println(numero);
		}
	}
}