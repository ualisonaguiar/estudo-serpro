const assert = require('assert');
const ConversaoMoeda = require('../ConversaoMoeda');

describe('Teste da Conversão de Moeda', () => {

    it('Teste de adicionar taxa de câmbio', () => {
        let conversaoMoeda = new ConversaoMoeda();

        conversaoMoeda.adicionarTaxaCambio('USD', 0.200853);
        conversaoMoeda.adicionarTaxaCambio('EUR', 0.184415);
        conversaoMoeda.adicionarTaxaCambio('BRA', 0.200866);
        
        let conversoes = conversaoMoeda.listarTaxaCambio();
        assert.deepEqual(conversoes.length, 3)
    });

    it('Convertendo valor de R$ 1000,00 para USD', () => {
        let conversaoMoeda = new ConversaoMoeda();
        conversaoMoeda.adicionarTaxaCambio('USD', 0.200853);

        assert.deepEqual(conversaoMoeda.converter(1000, 'USD'), 200.85)
    });

    it('Convertendo valor de R$ 1000,00 para EUR', () => {
        let conversaoMoeda = new ConversaoMoeda();
        conversaoMoeda.adicionarTaxaCambio('EUR', 0.184425);

        assert.deepEqual(conversaoMoeda.converter(1000, 'EUR'), 184.43)
    });
    
    it('Convertendo valor de $ 1000,00 para BRA', () => {
        let conversaoMoeda = new ConversaoMoeda();
        conversaoMoeda.adicionarTaxaCambio('BRA', 4.97705);

        assert.deepEqual(conversaoMoeda.converter(1000, 'BRA'), 4977.05)
    });

    it('Convertendo valor de $ 1000,00 para EUR', () => {
        let conversaoMoeda = new ConversaoMoeda();
        conversaoMoeda.adicionarTaxaCambio('EUR', 5.42100);

        assert.deepEqual(conversaoMoeda.converter(1000, 'EUR'), 5421)
    });       

});