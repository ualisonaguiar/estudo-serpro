const ConversaoMoeda = class {
    constructor() {
        this.taxaCambios = [];
    }

    adicionarTaxaCambio = function (moeda, taxaCambio) {
        this.taxaCambios.push({
            'moeda': moeda,
            'taxa': taxaCambio
        });
    };

    listarTaxaCambio = function () {
        return this.taxaCambios;
    };

    converter = function(valor, moeda) {
        let taxaConvertida = 0.00;
        let taxaCambios = this.listarTaxaCambio();

        for (let taxaCambio of Object.values(taxaCambios)) {
            if (taxaCambio.moeda == moeda) {
                taxaConvertida = taxaCambio.taxa;
            }            
        }

        let novoValor = valor * taxaConvertida;

        return novoValor.toFixed(2);
    };

}

module.exports = ConversaoMoeda;