var verificarNumeroIntervalo = function(numero) {
	let valorMinimo = Math.pow(-2, 31);
	let valorMaximo = Math.pow(2, 31) - 1;

	return numero >= valorMinimo && numero <= valorMaximo;
}

/**
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
	if (!verificarNumeroIntervalo(x)) {
		return 0;
	}

    let numero_negativo = x < 0;
    let numero_string = x.toString();
    let numero_reverso = '';

    for (let idx = numero_string.length - 1; idx >= 0; idx--) {
    	numero_reverso += numero_string[idx];
    }
    numero_reverso = parseInt(numero_reverso);
    if (numero_negativo) {
    	numero_reverso *= -1;
    }

	if (!verificarNumeroIntervalo(numero_reverso)) {
		numero_reverso = 0;
	}

    return numero_reverso;
};

module.exports = reverse