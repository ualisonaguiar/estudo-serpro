const assert = require('assert');
const twoSum = require('../twoSum')

describe('Teste a soma de dois números', () => {
	it('Teste passando o array: nums = [2,7,11,15], target = 9', () => {
		assert.deepEqual([0, 1], twoSum([2,7,11,15], 9));
	});

	it('Teste passando o array: nums = [3,2,4], target = 6', () => {	
		assert.deepEqual([1, 2], twoSum([3, 2, 4], 6));
	});

	it('Teste passando o array: nums = [3,3], target = 6', () => {
		assert.deepEqual([0, 1], twoSum([3, 3], 6));
	});

	it('Teste passando o array: nums = [2,5,5,11], target = 10', () => {
		assert.deepEqual([1, 2], twoSum([2,5,5,11], 10));
	});	
});