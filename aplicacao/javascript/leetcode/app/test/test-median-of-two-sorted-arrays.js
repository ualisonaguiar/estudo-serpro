const assert =  require('assert');
const findMedianSortedArrays = require('../median-of-two-sorted-arrays')

describe('Teste - Median of Two Sorted Arrays', () => {

	it('Teste passando o array: nums1 = [1,3], nums2 = [2] valor esperado: 2.0', () => {
		assert.deepEqual(2.0, findMedianSortedArrays([1,3], [2]));
	});

	it('Teste passando o array: nums1 = [1,2], nums2 = [3,4] valor esperado: 2.5', () => {
		assert.deepEqual(2.5, findMedianSortedArrays([1,2], [3, 4]));
	});	

	it('Teste passando o array: nums1 = [1,2], nums2 = [3,4] valor esperado: -1.0', () => {
		assert.deepEqual(-1.0, findMedianSortedArrays([3], [-2, -1]));
	});		
});