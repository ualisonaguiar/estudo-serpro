const assert = require('assert');
const plusOne = require('../plus-one');

describe('Funcao de PlusOne', () => {
    it('Deve retornar um array [1, 2, 4]', () => {
        assert.deepEqual(plusOne([1, 2, 3]), [1, 2, 4]);
    });

    it('Deve retornar um array [4,3,2,2]', () => {
        assert.deepEqual(plusOne([4, 3, 2, 1]), [4, 3, 2, 2]);
    });

    it('Deve retorna um array [1, 0]', () => {
        assert.deepEqual(plusOne([9]), [1, 0]);
    });

    it ('Deve retorna um array [6,1,4,5,3,9,0,1,9,5,1,8,6,7,0,5,5,4,4]', () => {
        assert.deepEqual(plusOne([6, 1, 4, 5, 3, 9, 0, 1, 9, 5, 1, 8, 6, 7, 0, 5, 5, 4, 3]), [6, 1, 4, 5, 3, 9, 0, 1, 9, 5, 1, 8, 6, 7, 0, 5, 5, 4, 4]);
    });

    it ('Deve retorna um array [1,0,0]', () => {
        assert.deepEqual(plusOne([9, 9]), [1, 0, 0]);
    });
});