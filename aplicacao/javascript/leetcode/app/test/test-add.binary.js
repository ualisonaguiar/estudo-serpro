const assert = require('assert');
const addBinary = require('../add-binary');

describe('Teste adicionando binario', () => {

    it('Deve retorna o valor 100', () => {
        assert.deepEqual(addBinary("11", "1"), 100);
    });

    it('Deve retorna o valor 10101', () => {
        assert.deepEqual(addBinary("1010", "1011"), 10101);
    });

});