const assert = require('assert');
const myAtoi = require('../string-to-integer-atoi');

describe('Testando a conversão de string-to-integer-atoi', () => {

	it('Teste passando string "42" esperando 42', () => {
		//assert.deepEqual(42, myAtoi("42"));
	});

	it('Teste passando string "         -42       " esperando -42', () => {
		//assert.deepEqual(-42, myAtoi("         -42       "));
	});		

	it('Teste passando string "4193 with words" valor esperado 4193', () => {
		//assert.deepEqual(4193, myAtoi("4193 with words"));
	});			

	it('Teste passando string "words 123" valor esperado 0', () => {
		//assert.deepEqual(0, myAtoi("words 123"));
	});			

	it('Teste passando string "-91283472332" valor esperado -2147483648', () => {
		//assert.deepEqual(-2147483648, myAtoi("-91283472332"));
	});	

	it('Teste passando string "-" valor esperado 0', () => {
		//assert.deepEqual(0, myAtoi("-"));
	});	

	it('Teste passando string "+1" valor esperado 1', () => {
		assert.deepEqual(1, myAtoi("+1"));
	});		
});