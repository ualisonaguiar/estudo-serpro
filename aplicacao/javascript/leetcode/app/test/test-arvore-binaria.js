const assert = require('assert');
const { Node, BinaryTree } = require('../arvore-binaria');

describe('Teste da árvore binária', () => {

	it('Criando uma árvore:  [2, 3, 4, 5, 8]', () => {
		let tree = new BinaryTree();
		tree.insert(5);
		tree.insert(3);
		tree.insert(8);
		tree.insert(2);
		tree.insert(4);
		assert.deepEqual([2, 3, 4, 5, 8], tree.inorderTraversal());
		assert.deepEqual(true, tree.search(8));
	});


	it('Criando uma árvore: [2, 3, 4, 5, 6, 8, 9]', () => {
		let tree = new BinaryTree();
		tree.insert(5);
		tree.insert(3);
		tree.insert(8);
		tree.insert(6);
		tree.insert(2);
		tree.insert(9);
		tree.insert(4);
		assert.deepEqual([2, 3, 4, 5, 6, 8, 9], tree.inorderTraversal());
		assert.deepEqual([5, 3, 2, 4, 8, 6, 9], tree.preorderTraversal());
		assert.deepEqual([2, 4, 3, 6, 9, 8, 5], tree.posorderTraversal());
	});	
});