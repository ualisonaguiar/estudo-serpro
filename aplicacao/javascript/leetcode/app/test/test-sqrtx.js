const assert = require('assert');
const sqrtx = require('../sqrtx');

describe('Teste o calculo da raiz quadrada', () => {

    it('Deve retornar o valor 2 da raiz 4', () => {
        assert.deepEqual(sqrtx(4), 2);
    });

    it('Deve retornar o valor 2 da raiz 8', () => {
        assert.deepEqual(sqrtx(8), 2);
    })

});