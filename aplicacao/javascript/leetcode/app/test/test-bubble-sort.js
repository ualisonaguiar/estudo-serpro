const assert = require('assert');
const bubbleSort = require('../bubbleSort');

describe('Testando o algoritmo bubble-sort', () => {

    it('Teste com a entrada: [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5]', () => {
        let entrada = [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5];
        let saida = [1, 1, 2, 3, 3, 4, 5, 5, 5, 6, 9];

        assert.deepEqual(bubbleSort(entrada), saida);
    });

    it('Testando com a entrada []', () => {
        assert.deepEqual(bubbleSort([]), []);
    });
});