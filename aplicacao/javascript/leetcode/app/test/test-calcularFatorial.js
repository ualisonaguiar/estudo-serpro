const assert = require('assert');
const calcularFatorial = require('../fatorial-calculator');

describe('Testando o calculo de fatorial', () => {

    it('Esperando o valor 120 com a entrada 5', () => {
        assert.deepEqual(calcularFatorial(5), 120);
    });

    it('Esperando o valor 1 com a entrada 0', () => {
        assert.deepEqual(calcularFatorial(0), 1);
    });

    it('Deve lançar uma exceção para números negativos', function () {
        assert.throws(() => calcularFatorial(-5), /O cálculo do fatorial não é definido para números negativos./);
    });  
});