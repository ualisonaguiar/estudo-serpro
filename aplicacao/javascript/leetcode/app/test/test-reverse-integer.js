const assert = require('assert');
const reverse = require('../reverse-integer')

describe('Teste de número reverso', () => {

	it('Teste passando o número: 123 esperando o valor: 321', () => {
		//assert.deepEqual(321, reverse(123));
	});

	it('Teste passando o número: -123 esperando o valor: -321', () => {
		//assert.deepEqual(-321, reverse(-123));
	});

	it('Teste passando o número: 120 esperando o valor: 21', () => {
		//assert.deepEqual(21, reverse(120));
	});

	it('Teste passando o número: 1534236469 esperando o valor: 0', () => {
		assert.deepEqual(0, reverse(1534236469));
	});	
});