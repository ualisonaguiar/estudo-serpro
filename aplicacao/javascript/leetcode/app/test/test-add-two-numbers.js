const assert = require('assert');
const addTwoNumbers = require('../add-two-numbers');

describe('Teste adicionando dois números', () => {

	it('Teste com l1 = [2,4,3], l2 = [5,6,4] valor esperado: [7,0,8]', () => {
		assert.deepEqual([7,0,8], addTwoNumbers([2,4,3], [5,6,4]));
	});

	it('Teste com l1 = [0], l2 = [0], valor esperado: [0]', () => {
		assert.deepEqual([0], addTwoNumbers([0], [0]));
	});

	it('Teste com l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9], valor esperado: [8,9,9,9,0,0,0,1]', () => {
		assert.deepEqual([8,9,9,9,0,0,0,1], addTwoNumbers([9,9,9,9,9,9,9], [9,9,9,9]));
	});	
})