var adicionaNumerosLista = function(listaNumeros, lista) {
	for (let numero of lista) {
		listaNumeros.push(numero)
	}
}

/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var findMedianSortedArrays = function(nums1, nums2) {
	let listaNumeros = [];
	adicionaNumerosLista(listaNumeros, nums1);
	adicionaNumerosLista(listaNumeros, nums2);
	
	listaNumeros.sort((a,b) => a -b);
	let tamanho = listaNumeros.length;
	let mediana = 0.0;

	if(tamanho % 2 == 0) {
		let numeroCentro1 = listaNumeros[(parseInt(tamanho/2)) - 1];
		let numeroCentro2 = listaNumeros[(parseInt(tamanho/2))];
		mediana = (numeroCentro1 + numeroCentro2) / 2;
	} else {
		mediana = listaNumeros[parseInt(tamanho/2)];
	}

	return parseFloat(mediana);
}

module.exports = findMedianSortedArrays;