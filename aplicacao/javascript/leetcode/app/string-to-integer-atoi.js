var isDigit = function(character) {
    return !isNaN(parseInt(character)) && isFinite(character);
}

var isNumeroLimite = function(numero) {
	let valorMinimo = Math.pow(-2, 31);
	let valorMaximo = Math.pow(2, 31) - 1;

	if (numero < valorMinimo) {
		numero = valorMinimo;
	} else if(numero > valorMaximo) {
		numero = valorMaximo;
	}

	return numero;
}

/**
 * @param {string} s
 * @return {number}
 */
var myAtoi = function(s) {
    s = s.trim();
    if (s.length == 0) {
    	return 0;
    }

    numero = '';
    numero_sinal = 1;
    idx = 0;

    for (caracter of s) {
    	if (caracter == '-' && idx == 0) {
    		numero_sinal = -1;
    	} else if (caracter == '+' && idx == 0) {
    		numero_sinal = 1;
    	} else if(isDigit(caracter) == false && numero.length == 0) {
    		return 0
    	} else if(isDigit(caracter)) {
    		numero += caracter;
    	} else {
    		break;
    	}

    	idx += 1;
    }

    if (numero.length == 0) {
    	console.log(numero);
    	return 0;
    }

    numero = parseInt(numero) * numero_sinal;
    return isNumeroLimite(numero);
};

module.exports = myAtoi;