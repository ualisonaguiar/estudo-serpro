const calcularFatorial = function (n) {
    if (n < 0) {
        throw new Error('O cálculo do fatorial não é definido para números negativos.')
    } else if (n === 0) {
        return 1;
    } else {
        return n * calcularFatorial(n - 1);
    }
}

module.exports = calcularFatorial