var twoSum = function(nums, target) {
    let retorno = [];
    for(let idx = 0; idx < nums.length; idx++) {
    	for(let jdx = 0; jdx < nums.length; jdx++) {
    		if (idx != jdx && (nums[idx] + nums[jdx]) == target) {
    			retorno[0] = idx;
    			retorno[1] = jdx;
    			return retorno;
    		}
    	}
    }
    return [];
};

module.exports = twoSum