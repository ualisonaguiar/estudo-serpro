let plusOne = function (digits) {
    let digitsStr = "";
    let tamanho = digits.length;

    for (let i = 0; i < tamanho; i++) {
        digitsStr += digits[i].toString();
    }

    let numero = BigInt(digitsStr) + 1n;

    digits = String(numero).split("").map((num) => {
        return Number(num)
    });
    return digits;
};
module.exports = plusOne