/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
var converterArrayToNumero = function(lista) {
	let numero_string = '';

	for (numero of lista) {
		numero_string += numero.toString()
	}

	return parseInt(numero_string);
};

var converterNumeroToArray = function(numero) {
	return Array.from(String(numero), Number);
};

/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */
var addTwoNumbers = function(l1, l2) {
    let numeroL1 = converterArrayToNumero(l1.reverse());
    let numeroL2 = converterArrayToNumero(l2.reverse());
    return converterNumeroToArray(numeroL1 + numeroL2).reverse();
};

module.exports = addTwoNumbers;