class Node {
	constructor(value) {
		this.value = value;
		this.left = null;
		this.right = null;
	}	
}

class BinaryTree {
	constructor() {
		this.root = null;
	}

	insert(value) {
		if (!this.root) {
			this.root = new Node(value);
		} else {
			this._insertRecursive(this.root, value);
		}
	}

	search(value) {
		return this._searchRecursive(this.root, value)
	}

	_searchRecursive(node, value) {
		if (!node) {
			return false;
		}

		if (node.value === value) {
			return true;
		} else if(value < node.value) {
			return this._searchRecursive(node.left, value)
		} else {
			return this._searchRecursive(node.right, value)
		}
	}

	_insertRecursive(node, value) {
		if (value < node.value) {
			if (!node.left) {
				node.left = new Node(value);
			} else {
				this._insertRecursive(node.left, value);
			}
		} else {
			if (!node.right) {
				node.right = new Node(value);
			} else {
				this._insertRecursive(node.right, value);
			}			
		}
	}

	inorderTraversal() {
		let result = [];
		this._inorderRecursive(this.root, result);
		return result;
	}

	_inorderRecursive(node, result) {
		if (node) {
			this._inorderRecursive(node.left, result)
			result.push(node.value);
			this._inorderRecursive(node.right, result)
		}
	}

	preorderTraversal() {
		let result = [];
		this._preorderTraversal(this.root, result);
		return result;
	}

	_preorderTraversal(node, result) {
		if (node) {
			result.push(node.value);			
			this._preorderTraversal(node.left, result);
			this._preorderTraversal(node.right, result);
		}
	}

	posorderTraversal() {
		let result = [];
		this._posorderTraversal(this.root, result);
		return result;
	}

	_posorderTraversal(node, result) {
		if (node) {
			this._posorderTraversal(node.left, result);
			this._posorderTraversal(node.right, result);			
			result.push(node.value);
		}
	}		
}

module.exports = { Node, BinaryTree };