const express = require('express');
const fs = require('fs');
const app = express();
const port = 3000;

app.use(express.json());
app.listen(port, () => {
    console.log('servidor rodando na porta ${port}');
});

const dataFilePath = 'data.json';

app.get('/', (req, res) => {
    return res.json(loadData());
});

app.post('/', (req, res) => {
    const registros = loadData();
    const data = req.body;
    data.id = 1;

    if (registros.length !== 0) {
        for (const index in registros) {
            id = registros[index].id;
        }
        data.id = parseInt(id) + 1;
    }

    registros.push(data);
    saveData(registros);
    
    return res.json({
        message: "Registrado criado com sucesso",
        data: data
    })
});

app.put('/:id', (req, res) => {
    const registros = loadData();
    const registro = req.body;
    const id = req.params.id;

    const registrosData = registros.map(item => {
        if (item.id == id) {
            registro.id = item.id;
            return registro;
        }

        return item;
    });

    saveData(registrosData);

    return res.json({
        message: "Registro alterado com sucesso",
        data: data
    });
});

app.delete('/:id', (req, res) => {
    const registros = loadData();
    const id = req.params.id;

    saveData(registros.filter(item => item.id != req.params.id));
    return res.json({
        message: "Registro excluido com sucesso",
    });    
});

function loadData() {
    try {
        const data = fs.readFileSync(dataFilePath, 'utf8');
        return JSON.parse(data);
    } catch (error) {
        return [];
    }
}

function saveData(data) {
    fs.writeFileSync(dataFilePath, JSON.stringify(data, null, 2), 'utf8');
}