import { Pessoa } from "../src/pessoa";
import { PessoaService } from "../src/pessoaService";

describe('Testando a service Pessoa', () => {

    let service;

    it('Testando o cadastro de pessoa', () => {
        let pessoa = new Pessoa();
        pessoa.nome = 'Ualison Aguiar';
        pessoa.idade = 35;
        pessoa.profissao = 'Analista de Sistema';

        service = new PessoaService();
        expect(service.cadastro(pessoa)).toBe(pessoa);
    });

    it('Testando a listagem de pessoa', () => {
        let pessoa = new Pessoa();
        pessoa.nome = 'Ualison Aguiar';
        pessoa.idade = 35;
        pessoa.profissao = 'Analista de Sistema';

        service = new PessoaService();
        service.cadastro(pessoa);

        pessoa.nome = 'João Paulo';
        pessoa.idade = 13;
        pessoa.profissao = 'Aluno';
        service.cadastro(pessoa); 
        
        expect(service.listagem().length).toBe(2);
    });    
});