import { Pessoa } from '../src/pessoa'; // Use a extensão .ts aqui

describe('Teste da classe Pessoa', () => {

    let pessoa !: Pessoa;

    beforeEach(() => {
        pessoa = new Pessoa();
    });

    it('Deve definir o nome', () => {
        pessoa.nome = 'Ualison Aguiar';
        expect(pessoa.nome).toBe('Ualison Aguiar');
    });

    it('Deve definir a idade', () => {
        pessoa.idade = 35;
        expect(pessoa.idade).toBe(35);
    });

    it('Deve definir a idade', () => {
        pessoa.profissao = 'Analista de Sistema';
        expect(pessoa.profissao).toBe('Analista de Sistema');
    });

    it('Validando a apresentação', () => {
        pessoa.nome = 'Ualison Aguiar';
        pessoa.idade = 35;
        pessoa.profissao = 'Analista de Sistema';
        let textoApresentacao = 'Meu nome é: Ualison Aguiar, tenho 35 e trabalho com Analista de Sistema';

        expect(pessoa.apresentacao()).toBe(textoApresentacao);
    });
})