"use strict";
var PessoaService = /** @class */ (function () {
    function PessoaService() {
    }
    PessoaService.prototype.cadastro = function (pessoa) {
        this.pessoa = pessoa;
        return this.pessoa;
    };
    PessoaService.prototype.apresentacao = function () {
        console.log("Meu nome \u00E9: ".concat(this.pessoa.nome, ", tenho ").concat(this.pessoa.idade, " e trabalho com ").concat(this.pessoa.profissao));
    };
    return PessoaService;
}());
