"use strict";
var Pessoa = /** @class */ (function () {
    function Pessoa() {
    }
    Object.defineProperty(Pessoa.prototype, "nome", {
        get: function () {
            return this._nome;
        },
        set: function (novoNome) {
            this._nome = novoNome;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "idade", {
        get: function () {
            return this._idade;
        },
        set: function (novaIdade) {
            this._idade = novaIdade;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pessoa.prototype, "profissao", {
        get: function () {
            return this._profissao;
        },
        set: function (novaProfissao) {
            this._profissao = novaProfissao;
        },
        enumerable: false,
        configurable: true
    });
    return Pessoa;
}());
