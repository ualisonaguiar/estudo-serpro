const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

// Define o diretório de arquivos estáticos
const staticDir = path.join(__dirname, 'dist');

// Define a rota para servir o arquivo index.html
app.get('/', (req, res) => {
  res.sendFile(path.join(staticDir, 'index.html'));
});

// Define a rota para servir os arquivos estáticos (css, js, imagens, etc.)
app.use(express.static(staticDir));

// Inicia o servidor na porta especificada
app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
});
