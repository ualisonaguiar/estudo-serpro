import { Pessoa } from "./pessoa";
import { PessoaInterface } from './pessoaInterface';

export class PessoaService implements PessoaInterface {

    private _pessoas: Pessoa[];

    constructor() {
        this._pessoas = [];
    }

    listagem(): Pessoa[] {
        return this._pessoas;
    }

    cadastro(pessoa: Pessoa): Pessoa {
        this._pessoas.push(pessoa);

        return pessoa;
    }

}