export class Pessoa {
  private _nome!: string;
  private _idade!: number;
  private _profissao!: string;

  get nome(): string {
    return this._nome;
  }

  set nome(novoNome: string) {
    this._nome = novoNome;
  }

  get idade(): number {
    return this._idade;
  }

  set idade(novaIdade: number) {
    this._idade = novaIdade;
  }

  get profissao(): string {
    return this._profissao;
  }

  set profissao(novaProfissao: string) {
    this._profissao = novaProfissao;
  }

  apresentacao(): string {
    return `Meu nome é: ${this.nome}, tenho ${this.idade} e trabalho com ${this.profissao}`;
  }
}