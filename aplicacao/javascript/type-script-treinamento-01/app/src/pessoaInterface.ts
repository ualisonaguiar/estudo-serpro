import { Pessoa } from "./pessoa";

export interface PessoaInterface {

    cadastro(pessoa: Pessoa): Pessoa;

    listagem(): Pessoa[];
}