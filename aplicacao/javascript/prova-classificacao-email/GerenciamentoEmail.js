const GerenciamentoEmail = class {
    
    regrasSeperacaoEmail = [];
    tipoCategoriaDefault = 'CaixaEntrada';
    caixa = {
        'CaixaEntrada': [],
        'Pessoal': [],
        'Trabalho': [],
        'Spam': []
    };

    definirRegra = function(categoria, dominio) {
        this.regrasSeperacaoEmail.push({
            'categoria': categoria,
            'dominio': dominio
        });

        this.reclassificarEmail();
    };

    receberEmail = function(email) {
        let categoriaDefault = this.tipoCategoriaDefault;
        if (this.regrasSeperacaoEmail.length !== 0) {
            categoriaDefault = this.identificaTipoCaixa(email);
        }
        this.caixa[categoriaDefault].push(email);
    };

    reclassificarEmail = function() {
        for (let [tipoCaixa, emails] of Object.entries(this.caixa)) {
            for (let [indice, email] of Object.entries(emails)) {
                let novaCategoria = this.identificaTipoCaixa(email);
                if (novaCategoria != tipoCaixa) {
                    delete this.caixa[tipoCaixa][indice];
                    this.caixa[novaCategoria].push(email);
                }
            }
        }
    };

    listaEmailCaixa = function(categoria = this.tipoCategoriaDefault) {
        return this.caixa[categoria];
    };

    identificaTipoCaixa = function(email) {
        let detalhesEmail = email.split('@');
        let categoriaDefault = this.tipoCategoriaDefault;

        for(let dominio of this.regrasSeperacaoEmail) {
            if (dominio === detalhesEmail[1]) {
                categoriaDefault = dominio;
            }
        }
        return categoriaDefault;
    };
}

module.exports = GerenciamentoEmail;