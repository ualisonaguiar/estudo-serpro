const assert = require('assert');
const GerenciamentoEmail = require('../GerenciamentoEmail')

describe('Teste do Gerenciamento de E-mail', () => {

    it('Teste de recebimento de e-mail', () => {
        let emailTest = 'ualison.aguiar@gmail.com';
        let gerenciamentoEmail = new GerenciamentoEmail();
        let emailEncontrado = false;

        gerenciamentoEmail.receberEmail('uafrota@hotmail.com');
        gerenciamentoEmail.receberEmail('uafrota@yahoo.com');
        gerenciamentoEmail.receberEmail('dtiadj2@dct.eb.mil.br');
        gerenciamentoEmail.receberEmail(emailTest);

        for (let email of gerenciamentoEmail.listaEmailCaixa()) {
            if (email == emailTest) {
                emailEncontrado = true;
                break;
            }
        }
        assert.ok(emailEncontrado);
    });

    it('Teste de aplicação de categoria de e-mail recebido', () => {
        let gerenciamentoEmail = new GerenciamentoEmail();
        gerenciamentoEmail.definirRegra('Pessoal', 'gmail.com');
        gerenciamentoEmail.definirRegra('Pessoal', 'hotmail.com');
        gerenciamentoEmail.definirRegra('Trabalho', 'serpro.gov.br');
        gerenciamentoEmail.definirRegra('Trabalho', 'dct.eb.mil.br');
        gerenciamentoEmail.definirRegra('Spam', 'g1.com.br');
        gerenciamentoEmail.definirRegra('Spam', 'r7.com.br');

        gerenciamentoEmail.receberEmail('uafrota@hotmail.com');
        gerenciamentoEmail.receberEmail('uafrota@yahoo.com');
        gerenciamentoEmail.receberEmail('dtiadj2@dct.eb.mil.br');     
        gerenciamentoEmail.receberEmail('ualison.frota@serpro.gov.br');
        gerenciamentoEmail.receberEmail('dtiadj3@dct.eb.mil.br');
        gerenciamentoEmail.receberEmail('noticias@g1.com.br');
        gerenciamentoEmail.receberEmail('noticias-dias-pais@r7.com.br');

        assert.ok(1, gerenciamentoEmail.listaEmailCaixa());
        assert.ok(3, gerenciamentoEmail.listaEmailCaixa('Trabalho'));
        assert.ok(2, gerenciamentoEmail.listaEmailCaixa('Spam'));
    });

    it('Teste de recebimento de e-mail depois reaplicar regra', () => {
        let gerenciamentoEmail = new GerenciamentoEmail();
        gerenciamentoEmail.receberEmail('uafrota@hotmail.com');
        gerenciamentoEmail.receberEmail('ualison.frota@serpro.gov.br');
        gerenciamentoEmail.receberEmail('ualison.aguiar@gmail.com');
        assert.ok(3, gerenciamentoEmail.listaEmailCaixa());

        gerenciamentoEmail.definirRegra('Trabalho', 'serpro.gov.br');
        gerenciamentoEmail.definirRegra('Trabalho', 'dct.eb.mil.br');

        assert.ok(1, gerenciamentoEmail.listaEmailCaixa());
        assert.ok(2, gerenciamentoEmail.listaEmailCaixa('Trabalho'));

    });    
});
