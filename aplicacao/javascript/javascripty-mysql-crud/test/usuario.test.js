const chai = require('chai');
const assert = chai.assert;
const chaiHttp = require('chai-http');
const { expect } = chai;
chai.use(chaiHttp);

const app = require('../src/app'); // Sua aplicação Express

describe('Usuário CRUD', () => {
  it('teste - criando usuário', async () => {
    let res = await chai.request(app).post('/usuario').send({
      name: 'John', 
      email: 'john@example.com'
    });
    expect(res).to.have.status(201);
    expect(res.body).to.have.property('id');
  });

  it('teste - listando usuário', async () => {
    let res = await chai.request(app).get('/usuario').send();
    expect(res).to.have.status(200);
    let usuarios = res.body;
    assert.isTrue(usuarios.length != 0)
  });
  
  it('teste - buscando usuário específico', async () => {
    let res = await chai.request(app).post('/usuario').send({
      name: 'John',
      email: 'john@example.com'
    });
    let usuario = res.body;

    let res2 = await chai.request(app).get('/usuario/' + usuario.id).send();
    expect(res2).to.have.status(200);
    let usuario2 = res2.body;

    assert.deepEqual(usuario.id, usuario2.id);
    assert.deepEqual(usuario.name, usuario2.name);
    assert.deepEqual(usuario.email, usuario2.email);
  });

  it('teste - atualizar usuário', async() => {
    let res = await chai.request(app).post('/usuario').send({
      name: 'Ualison Aguiar',
      email: 'ualison.aguiar@gmail.com'
    });
    let usuario = res.body;
    usuario.name = ' da Ponte Frota';

    let res2 = await chai.request(app).put('/usuario/' + usuario.id).send(usuario);
    expect(res).to.have.status(201);

    let res3 = await chai.request(app).get('/usuario/' + usuario.id).send();
    let usuario2 = res3.body;

    assert.deepEqual(usuario.nome, usuario2.nome);
  });

  it('teste - excluindo usuário', async() => {
    let res = await chai.request(app).post('/usuario').send({
      name: 'Ualison Aguiar',
      email: 'ualison.aguiar@gmail.com'
    });
    let usuario = res.body;

    await chai.request(app).delete('/usuario/' + usuario.id).send();
    let res1 = await chai.request(app).get('/usuario/' + usuario.id).send();
    expect(res1).to.have.status(404);
  })
});
