const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('./database');

const Usuario = sequelize.define('usuario', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
},{
  timestamps: false, // Desabilita createdAt e updatedAt
});

module.exports = Usuario;
