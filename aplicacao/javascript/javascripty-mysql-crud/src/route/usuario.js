// src/routes/Usuario.js
const express = require('express');
const router = express.Router();
const UsuarioModel = require('../model/Usuario');

// Rota para criar um novo usuário
router.post('/usuario', async (req, res) => {
  try {
    const { name, email } = req.body;
    const usuario = await UsuarioModel.create({ name, email });
    res.status(201).json(usuario);
  } catch (error) {
    res.status(500).json({ error: 'Failed to create Usuario' });
  }
});

// Rota para obter todos os usuários
router.get('/usuario', async (req, res) => {
  try {
    const usuarios = await UsuarioModel.findAll();
    res.json(usuarios);
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve Usuarios' });
  }
});

// Rota para obter um usuário específico
router.get('/usuario/:id', async (req, res) => {
  try {
    const usuario = await UsuarioModel.findByPk(req.params.id);
    if (usuario) {
      res.json(usuario);
    } else {
      res.status(404).json({ error: 'Usuario not found' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve Usuario' });
  }
});

// Rota para atualizar um usuário
router.put('/usuario/:id', async (req, res) => {
  try {
    const { name, email } = req.body;    
    const usuario = await UsuarioModel.findByPk(req.params.id);
    if (usuario) {
      usuario.name = name;
      usuario.email = email;
      await usuario.save()
      res.json(usuario);
    } else {
      res.status(404).json({ error: 'Usuario not found' });
    }
    res.json(usuario);
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve Usuarios' });
  }
});

// Rota para excluir um usuário
router.delete('/usuario/:id', async (req, res) => {
  try {
    const usuario = await UsuarioModel.findByPk(req.params.id);
    if (usuario) {
      await usuario.destroy();
      res.json({ message: 'Usuario deleted' });
    } else {
      res.status(404).json({ error: 'Usuario not found' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Failed to delete Usuario' });
  }
});

module.exports = router;
