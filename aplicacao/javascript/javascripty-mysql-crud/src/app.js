// src/app.js
const express = require('express');
const app = express();
const userRoutes = require('./route/usuario');

app.use(express.json());

// Use as rotas de usuário
app.use(userRoutes);

module.exports = app;
