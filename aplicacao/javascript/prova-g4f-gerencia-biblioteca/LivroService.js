const fs = require('fs');
const dataFilaPath = 'data.json';

const LivroService = class {
    adicionar = function (data) {
        let registros = this.loadData();
        data.id = 1;

        if (registros.length != 0) {
            let lastId = 0;
            for (const index in registros) {
                lastId = registros[index].id;
            }
            data.id = parseInt(lastId) + 1;
        }
        registros.push(data);
        this.saveData(registros);

        return data;
    }

    listagem = function () {
        let registros = this.loadData();
        return registros;
    }

    remover = function (id) {
        let registros = this.loadData();
        registros = registros.filter(registro => registro.id != id);
        this.saveData(registros);
    }

    saveData = function (data) {
        fs.writeFileSync(dataFilaPath, JSON.stringify(data, null, 2), 'utf8');
    }

    loadData = function () {
        try {
            const data = fs.readFileSync(dataFilaPath, 'utf8');
            return JSON.parse(data);
        } catch (error) {
            return [];
        }
    }
}

module.exports = LivroService;