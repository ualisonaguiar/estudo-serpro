const assert = require('assert');
const LivroService = require('../LivroService');

describe('Testeando o arquivo LivroService', () => {

    it('Adicionando livro', () => {
        const service = new LivroService();

        let data = {
            'titulo': 'Programação em PHP',
            'autor': 'Mineiro Cristal',
            'isbn': '12345678-9'
        };

        data =  service.adicionar(data);
        assert.ok(data.id);
    });

    it('Listando livros', () => {
        const service = new LivroService();
        assert.deepEqual(service.listagem().length != 0, true)
    });

    it('Remove livros', () => {
        let data = {
            'titulo': 'Programação em PHP',
            'autor': 'Mineiro Cristal',
            'isbn': '12345678-9'
        };
        const service = new LivroService();
        data = service.adicionar(data);
        service.remover(data.id);
    });
});